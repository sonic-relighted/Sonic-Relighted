extends Spatial

enum State {
	WALK, IDLE
}

onready var animator : AnimationPlayer = $AnimationPlayer
onready var state    : int             = State.IDLE

func is_idle(): return state == State.IDLE
func is_walk(): return state == State.WALK
func is_warn(): return false
func is_attack(): return false

func walk():
	state = State.WALK
#	animator.get_animation("Corriendo").loop = true	
#	animator.play("Corriendo")

func idle():
	state = State.IDLE
#	animator.play("Idle")
