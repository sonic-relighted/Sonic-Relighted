extends Node

#class_name ResetableEnemy

onready var host = get_parent()

onready var default_translation = host.translation
onready var default_collision_layer = host.collision_layer
onready var default_collision_mask = host.collision_mask

func _ready():
	set_process(false)
	set_physics_process(false)

func reset():
	host.collision_layer = default_collision_layer
	host.collision_mask = default_collision_mask
#	host.velocity = Vector3.ZERO
	host.is_destroyed = false
	host.translation = default_translation
#	host.explosion.emitting = false
	host.set_physics_process(true)
	host.anchor.show()
	host.enableable.active = true
	host.particles.emitting = false
