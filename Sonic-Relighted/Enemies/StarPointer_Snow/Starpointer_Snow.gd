extends SimpleNPC

onready var animator    : AnimationPlayer = $AnimationPlayer
onready var enableable                    = $EnableableArea
onready var anchor      : Spatial         = $Anchor
onready var particles   : Particles       = $Particles
onready var proyectiles : Spatial         = $starpointer_proyectile

onready var angle_projectls  : float   = .0
onready var is_destroyed     : bool    = false

func reset():
	.reset()
	$Resetable.reset()

func get_damage_force() -> Vector3:
	return Vector3(0.0, 1.0, .2) * 6.0

func damage() -> bool:
	if not is_destroyed:
		is_destroyed = true
		collision_layer = 0
		collision_mask = 0
		$DeathSfx.play()
		set_physics_process(false)
		anchor.hide()
		enableable.active = false
		particles.emitting = true
		get_tree().create_timer(.25).connect("timeout", self, "_end_particles")
	return is_destroyed

func _end_particles():
	particles.emitting = false

func _set_up_anims():
#	var idle_anim = animator.get_animation("idle")
#	idle_anim.loop = true
#	animator.playback_speed = .25
#	animator.play("idle")
	pass

func _set_up_death_areas():
	var area1 = _create_death_area($starpointer_proyectile/Eje/StarPointer_proyec)
	var area2 = _create_death_area($starpointer_proyectile/Eje/StarPointer_proyec001)
	var area3 = _create_death_area($starpointer_proyectile/Eje/StarPointer_proyec002)
	var area4 = _create_death_area($starpointer_proyectile/Eje/StarPointer_proyec003)
	$DamageArea.queue_free()

func _create_death_area(node : Spatial) -> Area:
	var template = $DamageArea
	var new_instance = template.duplicate()
	node.add_child(new_instance)
	new_instance.translation = Vector3.ZERO
	return new_instance

func _set_up_shadows():
	Builder.build_bottom_shadow($starpointer_proyectile/Eje/StarPointer_proyec)
	Builder.build_bottom_shadow($starpointer_proyectile/Eje/StarPointer_proyec001)
	Builder.build_bottom_shadow($starpointer_proyectile/Eje/StarPointer_proyec002)
	Builder.build_bottom_shadow($starpointer_proyectile/Eje/StarPointer_proyec003)

func _physics_process(delta : float):
	angle_projectls += delta
	proyectiles.rotation.y = angle_projectls - rotation.y

func _ready():
	._ready()
	.reset()
	_set_up_anims()
	_set_up_death_areas()
	_set_up_shadows()
