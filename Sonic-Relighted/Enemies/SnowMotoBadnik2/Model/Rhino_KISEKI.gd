extends Spatial

enum State {
	WALK, IDLE, WARN, ATTACK, POST_ATTACK
}

onready var animator : AnimationPlayer = $AnimationPlayer
onready var state    : int             = State.IDLE

onready var players_near : int = 0
onready var stopped      : bool = false

func is_idle(): return state == State.IDLE
func is_walk(): return state == State.WALK
func is_warn(): return state == State.WARN
func is_attack(): return state == State.ATTACK

func walk():
	state = State.WALK
	animator.get_animation("Corriendo").loop = true
	animator.play("Corriendo")

func idle():
	state = State.IDLE
	animator.play("Idle")

func warn(host, delta : float):
	var plyr = Instances.player.global_transform.origin
	var me   = host.global_transform.origin
	var diff = plyr - me
	var diff2d = Vector2(diff.x, -diff.z)
	var angle = diff2d.angle() - AngleConstants.SEMI_PI
	host.rotation.y = lerp_angle(host.rotation.y, angle, delta * 5.0)
	host.move_and_slide(Vector3.DOWN * host.gravity_influence)

func attack(host, delta : float):
	if stopped:
		host.move_and_slide(Vector3.DOWN * host.gravity_influence)
		return
		
	host.destination = Instances.player.global_transform.origin

	var origin = host.global_transform.origin
	var diff = origin - host.destination

	var new_rot = atan2(diff.x, diff.z)
	host.rotation.y = lerp_angle(host.rotation.y, new_rot, delta * 5.0)
	var v = Vector3.FORWARD.rotated(Vector3(.0,1.0,.0), host.rotation.y) * 2.0
	v.y -= host.gravity_influence
	host.move_and_slide(v)

func _start_warn(host):
	state = State.WARN
	animator.stop()
	animator.play("Warn")

func _end_warn(host):
	pass

func _start_attack(host):
	_end_warn(host)
	state = State.ATTACK
	animator.stop()
	animator.play("Attack")
	stopped = false

func _end_attack(host):
	_start_post_attack()

func _start_post_attack():
	state = State.POST_ATTACK
	$AttackArea/CollisionShape.disabled = true
	$EnableCollisionShapeTimer.start()

func _stop_attack_movement(host):
	animator.stop()
	animator.play("Attack2")
	stopped = true

func _on_AttackArea_body_entered(body):
	players_near += 1

func _on_AttackArea_body_exited(body):
	players_near -= 1

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Warn":
		_start_attack(get_parent().get_parent())
	elif anim_name == "Attack":
		_stop_attack_movement(get_parent().get_parent())
	elif anim_name == "Attack2":
		_end_attack(get_parent().get_parent())

func _on_EnableCollisionShapeTimer_timeout():
	$AttackArea/CollisionShape.disabled = false
	var host = get_parent().get_parent()
	state = State.IDLE
	host._on_Timer_timeout()

func step(host, delta : float):
	if players_near > 0:
		if state == State.IDLE or state == State.WALK:
			_start_warn(host)
