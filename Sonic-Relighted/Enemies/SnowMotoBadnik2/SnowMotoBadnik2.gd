tool
extends SimpleNPC

onready var animator  : AnimationPlayer   = $AnimationPlayer
onready var enableable                    = $EnableableArea
onready var anchor    : Spatial           = $Anchor
onready var particles : Particles         = $Particles

onready var is_destroyed     : bool    = false

func configure_shadow(shadow : Spatial):
	shadow.scale *= 2.5

func reset():
	.reset()
	$Resetable.reset()

func get_damage_force() -> Vector3:
	return Vector3(0.0, 1.0, .2) * 6.0

func damage() -> bool:
	if not is_destroyed:
		is_destroyed = true
		collision_layer = 0
		collision_mask = 0
		$DeathSfx.play()
		set_physics_process(false)
		anchor.hide()
		enableable.active = false
		particles.emitting = true
		get_tree().create_timer(.25).connect("timeout", self, "_end_particles")
	return is_destroyed

func _end_particles():
	particles.emitting = false

func _physics_process(delta :  float):
	model.step(self, delta)

func _ready():
	._ready()
	.reset()
