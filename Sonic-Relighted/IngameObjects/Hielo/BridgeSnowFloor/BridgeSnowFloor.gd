extends StaticBody

export(float) var delay : float = .5
export(float) var firetime : float = .5

onready var obj       = $CSGBox
onready var obj2      = $CSGBox2
onready var particles = $Particles
onready var fire_particles = $Particles2
onready var sfx       = $Sfx

onready var destroyed : bool = false

func destroy():
	if destroyed: return
	get_tree().create_timer(delay).connect("timeout", self, "_stop_fire")
	destroyed = true
	fire_particles.emitting = true

func _stop_fire():
	fire_particles.emitting = false
	set_collision_layer_bit(0, false)
	obj.hide()
	obj2.hide()
	sfx.play()
	particles.emitting = true
	get_tree().create_timer(firetime).connect("timeout", self, "_stop_particles")

func reset():
	destroyed = false
	set_collision_layer_bit(0, true)
	obj.show()
	obj2.show()
	_stop_particles()

func _stop_particles():
	particles.emitting = false

func _ready():
	reset()
