extends StaticBody

export(float) var delay : float = .5

onready var obj       = $PhysicsOverwriteArea
onready var box       = $PhysicsOverwriteArea/CSGBox
onready var particles = $Particles
onready var sfx       = $Sfx
onready var tween     = $VibrationTween

onready var destroyed : bool = false

func set_physics_process(enabled : bool):
	.set_physics_process(enabled)
	if enabled:
		obj.collision_layer = obj.default_collision_layer
		obj.collision_mask = obj.default_collision_mask
	else:
		obj.collision_layer = 0
		obj.collision_mask = 0

func destroy():
	if destroyed: return
	destroyed = true
	var normal = Vector3(1.0, 1.0, 1.0)
	var scaled = Vector3(.9, .9, .9)
	tween.interpolate_property(
			box, "scale",
			scaled, normal, delay,
			Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
	tween.start()

func _on_VibrationTween_tween_all_completed():
	obj.set_collision_layer_bit(19, false)
	set_collision_layer_bit(0, false)
	obj.hide()
	sfx.play()
	_emit_particles(true)
	get_tree().create_timer(.5).connect("timeout", self, "_stop_particles")

func reset():
	destroyed = false
	obj.set_collision_layer_bit(19, true)
	set_collision_layer_bit(0, true)
	obj.show()
	_stop_particles()

func _stop_particles():
	_emit_particles(false)

func _emit_particles(value : bool):
	particles.emitting = value

func _ready():
	reset()
