extends Node

# Configuracion global del proyecto, no son settings ni nada parecido
# si no configuracion que el usuario no puede modificar

onready var can_skip_logos : bool = false

const VERSION = "ALPHA 20200909"

const LOGOS_SCREEN = false # logotipos al inicio del juego
const DEMO = true # no hay slots y esas cosas

const ZONESELECT_PATH      = "res://RelightedEngine/ZoneSelect/"
const SCENES_PATH          = "res://RelightedEngine/Scenes/"
const UIOBJECTS_PATH       = "res://RelightedEngine/UI Objects/"
const INGAME_ELEMENTS_PATH = "res://RelightedEngine/IngameElements/"
const ITEMS_PATH           = "res://Items/"

const OST_LEVELS_PATH   = "res://Audio/ost/Levels/"
const BONUS_STAGES_PATH = "res://Levels/BonusStages/"
