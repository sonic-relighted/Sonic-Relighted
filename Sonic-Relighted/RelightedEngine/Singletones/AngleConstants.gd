extends Node

const QUART_PI : float = PI / 4
const SEMI_PI : float = PI / 2
const TWO_PI : float = PI * 2

# Coordenadas absolutas
const UP = .0
const UP_RIGHT   = QUART_PI
const RIGHT      = UP_RIGHT * 2
const DOWN_RIGHT = UP_RIGHT * 3
const DOWN       = UP_RIGHT * 4
const DOWN_LEFT  = UP_RIGHT * 5
const LEFT       = UP_RIGHT * 6
const UP_LEFT    = UP_RIGHT * 7

# Inicio de cada posicion
const INIT_UP         = UP_LEFT + PI / 8
const INIT_UP_RIGHT   = UP_RIGHT - PI / 8
const INIT_RIGHT      = RIGHT - PI / 8
const INIT_DOWN_RIGHT = DOWN_RIGHT - PI / 8
const INIT_DOWN       = DOWN - PI / 8
const INIT_DOWN_LEFT  = DOWN_LEFT - PI / 8
const INIT_LEFT       = LEFT - PI / 8
const INIT_UP_LEFT    = UP_LEFT - PI / 8

func convert_angles(length : float, hz_angle : float, vt_angle : float) -> Vector3:
	var r = length
	var fi = vt_angle
	var zeta = hz_angle
	var om = r * sin(fi)
	var x = om * cos(zeta)
	var y = om * sin(zeta)
	var z = r * cos(fi)
	return Vector3(y, z, x)
