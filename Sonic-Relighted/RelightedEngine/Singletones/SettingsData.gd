extends Node
const default_settings_data : Dictionary = {
	"sfx_volume": 0,
	"music_volume": 0,
	"voices_volume": 0,
	"shadows": true,
#	"transparencies": true, #Desactivado para cargarlo programaticamente
	"resolution": 0,
	"fullscreen": true,
	"vsync": true,
	"language": null
}

var screen_resolution

var data

###############################################

func load_data():
	var file = File.new()
	if not file.file_exists("user://settings.json"):
		print("No file saved!")
		data = default_settings_data
	if file.open("user://settings.json", File.READ) != 0:
		print("Error opening file")
		data = default_settings_data
	var readed = parse_json(file.get_line())
	if readed == null:
		readed = default_settings_data
	data = readed
	file.close()
	if not data.has('glow'): data.glow = true
	if not data.has('shadows'): data.shadows = true
	if not data.has('transparencies'): data.transparencies = !MySystem.is_mobile()
	if not data.has('language') or data.language == null or data.language == "":
		if OS.get_locale().begins_with("es_"):
			data.language = "es"
		else:
			data.language = "en"
	AudioServerManager.set_sfx_volume(data.sfx_volume)
	AudioServerManager.set_music_volume(data.music_volume)
	AudioServerManager.set_voices_volume(data.voices_volume)
	set_language(data.language)
	set_resolution(data.resolution)
	OS.window_fullscreen = data.fullscreen
	OS.set_use_vsync(data.vsync)

func save_data():
	var file = File.new()
	if file.open("user://settings.json", File.WRITE) != 0:
		print("Error opening file")
		return
	file.store_line(to_json(data))
	file.close()
	AudioServerManager.set_sfx_volume(data.sfx_volume)
	AudioServerManager.set_music_volume(data.music_volume)
	AudioServerManager.set_voices_volume(data.voices_volume)
#	set_resolution(data.resolution)
#	OS.window_fullscreen = data.fullscreen
	OS.set_use_vsync(data.vsync)

func set_language(value : String):
	if value == "es":
		TranslationServer.set_locale("es_ES")
		return
	if value == "en":
		TranslationServer.set_locale("en_US")
		return
	if OS.get_locale().begins_with("es_"):
		TranslationServer.set_locale("es_ES")
		return
	TranslationServer.set_locale("en_US")

func set_resolution(value : int):
	var res = _get_window_size() / (value + 1)
	screen_resolution = Vector2(int(res.x), int(res.y))
	get_tree().set_screen_stretch(2, 1, screen_resolution)

func _get_window_size() -> Vector2:
	if OS.window_fullscreen:
		return OS.get_screen_size()
	return OS.window_size
