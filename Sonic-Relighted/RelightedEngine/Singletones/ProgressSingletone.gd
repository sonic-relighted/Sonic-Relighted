extends Node

const INITIAL_LIFES : int = 3
const RINGS_TO_LIFE : int = 100
const SCORE_TO_LIFE : int = 9999

# ==== Global ==== #

# ==== Funciones UI ==== #
onready var return_to_menu_from_ingame : bool = false

# ==== Progreso global del juego ==== #
onready var game_slot_id : int = 0 # progreso del juego, para almacenar progreso
onready var game_slot_data : Dictionary
onready var current_cinematic : int = 0
onready var playing_level_id : int = 1
onready var playing_bonus_id : int = 1
onready var active_shield : int = -1

# ==== Progreso en cada nivel ==== #
onready var ingame_rings         : int = 0
onready var ingame_rings_to_life : int = RINGS_TO_LIFE # cada 100 rings, vida extra
onready var ingame_seconds       : int = 0
onready var ingame_devices       : int = 0
onready var ingame_devices_total : int = 0
onready var ingame_stars         : Array = [false, false, false, false, false]
