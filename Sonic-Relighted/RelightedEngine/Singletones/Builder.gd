extends Node

onready var AIR_BUBBLE_PRELOAD         = preload("res://Items/AirBubble/AirBubble.res")
onready var BOUNCING_RING_PRELOAD      = preload("res://Items/Ring/BouncingRing.res")
onready var MONITOR_PRELOAD            = preload("res://Items/Monitor/Monitor.res")

const LIGHTSHADOW_COLOR : Color = Color(.3,.3,.3)
#
# Referentes al jugador
#
func build_air_bubble(parent : Spatial, origin : Vector3):
	var obj = AIR_BUBBLE_PRELOAD.instance()
	parent.add_child(obj)
	obj.translation = origin

func build_bouncing_ring(parent : Spatial, origin : Vector3, random_force : bool = true, force_vector : Vector3 = Vector3.ZERO):
	var obj = BOUNCING_RING_PRELOAD.instance()
	parent.add_child(obj)
	obj.translation = origin
	if random_force:
		var x = randf() - .5
		var y = randf() + 1
		var z = randf() - .5
		var dir : Vector3 = Vector3(x, y, z)
		obj.apply_central_impulse(dir * 2)
	obj.apply_central_impulse(force_vector)

func build_monitor(origin : Vector3, type : int) -> Spatial:
	var obj = MONITOR_PRELOAD.instance()
	obj.translation = origin
	obj.type = type
	return obj

func build_bottom_shadow(target : Spatial):
	if SettingsData.data.shadows == false: return Spatial.new()
	var shadow = load(GlobalConfig.INGAME_ELEMENTS_PATH + "IngameObjectShadow/IngameObjectShadow.res").instance()
	shadow.target = target
	target.connect("physics_processs_changed", shadow, "on_target_physics_processs_changed")
	target.get_parent().add_child(shadow)
	if target.has_method("configure_shadow"):
		target.configure_shadow(shadow)
	return shadow

# zone select

func build_omni_light(omni_energy : float = 3, omni_range : float = 6) -> Spatial:
	var luz = OmniLight.new()
	luz.shadow_enabled = false#SettingsData.data.shadows
	luz.light_energy = omni_energy
	luz.omni_range = omni_range
	luz.omni_shadow_mode = OmniLight.SHADOW_CUBE
	luz.shadow_color = LIGHTSHADOW_COLOR
	luz.set_script(load("res://AutoEnabledLight.gd"))
	return luz

func build_area_sphere(radius : float = 3) -> Spatial:
	var area = Area.new()
	var col = CollisionShape.new()
	var shape = SphereShape.new()
	shape.radius = radius
	col.shape = shape
	area.add_child(col)
	return area

func build_number_sprite(number : int):
	var sprite = Sprite3D.new()
	sprite.texture = load("res://Textures/Dkbd/Nums/" + str(number) + ".png")
	sprite.billboard = SpatialMaterial.BILLBOARD_ENABLED
	sprite.pixel_size = .006
	return sprite
