extends Node

func underwater_effect(enabled : bool):
	print("Underwater ", enabled)
	AudioServer.set_bus_effect_enabled(1, 0, enabled)
	AudioServer.set_bus_effect_enabled(2, 0, enabled)
	AudioServer.set_bus_effect_enabled(2, 1, enabled)

func set_sfx_volume(value : float): _set_volume(1, value)
func set_music_volume(value : float): _set_volume(2, value)
func set_voices_volume(value : float): _set_volume(3, value)

func get_sfx_linear_volume(): return _get_linear_volume(1)
func get_music_linear_volume(): return _get_linear_volume(2)
func get_voices_linear_volume(): return _get_linear_volume(3)

func _set_volume(bus : int, value : float):
	if value < -20:
		AudioServer.set_bus_volume_db(bus, -20)
		AudioServer.set_bus_mute(bus, true)
	else:
		AudioServer.set_bus_volume_db(bus, value)
		AudioServer.set_bus_mute(bus, false)

func _get_linear_volume(bus : int):
	if AudioServer.is_bus_mute(bus):
		return -21#0
	return db2linear(AudioServer.get_bus_volume_db(bus))
