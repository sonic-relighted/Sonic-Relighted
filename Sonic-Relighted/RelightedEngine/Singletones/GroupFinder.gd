extends Node

#
# Retorna un array con todos los nodos que tengan un determinado grupo
#
func find_nodes_in_group(parent : Node, group : String) -> Array:
	var r = []
	for node in parent.get_children():
		if node.is_in_group(group): r.append(node)
		for subnode in find_nodes_in_group(node, group):
			r.append(subnode)
	return r

#
# Retorna un array con todos los nodos de un tipo
#
func find_nodes_by_type(parent : Node, type) -> Array:
	var r = []
	for node in parent.get_children():
		if node is type:
			r.append(node)
		for subnode in find_nodes_by_type(node, type):
			r.append(subnode)
	return r

#
# Retorna un array con todos los nodos cuyo nombre comience por...
#
func find_nodes_by_name(parent : Node, name : String) -> Array:
	var r = []
	for node in parent.get_children():
		if node.get_name().begins_with(name): r.append(node)
		for subnode in find_nodes_by_name(node, name):
			r.append(subnode)
	return r

func get_all_nodes(parent : Node) -> Array:
	var r = []
	for node in parent.get_children():
		r.append(node)
		for subnode in get_all_nodes(node):
			r.append(subnode)
	return r
