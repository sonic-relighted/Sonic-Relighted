extends Node

func filled_number(num : int, digits : int) -> String:
	var str_num = str(num)
	while str_num.length() < digits: str_num = "0" + str_num
	return str_num

func time_seconds_to_timestamp(num_seconds : int) -> String:
	if num_seconds < 0: return "--:--"
	if num_seconds >= 6000: return "99:59"
	var seconds = num_seconds % 60
	var str_seconds = str(seconds)
	while str_seconds.length() < 2: str_seconds = "0" + str_seconds
	var minutes = num_seconds / 60
	var str_minutes = str(minutes)
	while str_minutes.length() < 2: str_minutes = "0" + str_minutes
	return str_minutes + ":" + str_seconds

func time_seconds_to_timestamp_with_decimals(num_seconds_ : float) -> String:
	var num_seconds = int(round(num_seconds_))
	var num_decs    = int(round(num_seconds_ * 10.0)) - num_seconds * 10
	if num_seconds < 0: return "--:--.--"
	if num_seconds >= 6000: return "99:59.99"
	var seconds = num_seconds % 60
	var str_seconds = str(seconds)
	while str_seconds.length() < 2: str_seconds = "0" + str_seconds
	var minutes = num_seconds / 60
	var str_minutes = str(minutes)
	while str_minutes.length() < 2: str_minutes = "0" + str_minutes
	var str_decs = str(num_decs)
	return str_minutes + ":" + str_seconds + "." + str_decs
