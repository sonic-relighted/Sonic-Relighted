extends Node

#
# Esto controla los EnableableAreas. De tal forma que no muestren u
# oculten los objetos que controlan al instante. En vez de eso se
# usa una lista de acciones que procesa una accion en cada step.
#

onready var stack : Array = []

enum Action {
	ENABLE, DISABLE
}

func reset():
	stack.clear()

func add(action : int, node : EnableableArea):
#	_erase(node)
	stack.append([action, node])
	set_process(true)

func _process(delta : float):
	if stack.size():
		_use()

func _use():
	var item = stack[0]
	if is_instance_valid(item[1]):
		match item[0]:
			Action.ENABLE:  item[1].enable()
			Action.DISABLE: item[1].disable()
	stack.erase(item)
	if stack.size() < 1: set_process(false)

func _erase(node : EnableableArea):
	for item in stack:
		if item[1] == node:
			stack.erase(item)
	if stack.size() < 1: set_process(false)

func _ready():
	set_process(false)
