using Godot;
using System;

public class MySystem : Node
{
	public bool is_mobile() {
		return OS.HasTouchscreenUiHint();
	}
	public override void _Ready() {
		Input.SetUseAccumulatedInput(false);
	}
}
