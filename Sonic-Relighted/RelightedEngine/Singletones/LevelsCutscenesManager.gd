extends Node

func close_cutscene(cutscene_id : int):
	match cutscene_id: # despues de esta cutscene ejecutamos...
		0: _ingame(1)
		1: _ingame(2)
		2: _zoneselect()
		3: _ingame(3)

func close_level(level_id : int):
	if ProgressSingletone.game_slot_data.completed_levels[level_id]:
		# Ojo, el array empieza en 0, obviamente.
		# Pero el primero nivel es 1, y no 0, por eso este if funciona
		_zoneselect()
		return
	match level_id: # despues de este nivel ejecutamos...
		1: _cutscene(1)
		2: _cutscene(2)
		3: _finish()

func open_level(level_id : int):
	match level_id:
		1: _ingame(1)
		2: _ingame(2)
		3: _cutscene(3)

func open_bonus(bonus_id : int):
	_bonus(bonus_id)

#########################################

func _finish():
	get_tree().change_scene(GlobalConfig.SCENES_PATH + "DemoScreen.tscn")

func _zoneselect():
	get_tree().change_scene(GlobalConfig.SCENES_PATH + "ZoneSelect.tscn")

func _cutscene(cutscene_id : int):
	ProgressSingletone.current_cinematic = cutscene_id
	get_tree().change_scene(GlobalConfig.SCENES_PATH + "Cutscene.tscn")

func _ingame(level_id : int):
	ProgressSingletone.playing_level_id = level_id
	get_tree().change_scene(GlobalConfig.SCENES_PATH + "Ingame.tscn")

func _bonus(bonus_id : int):
	ProgressSingletone.playing_bonus_id = bonus_id
	get_tree().change_scene(GlobalConfig.SCENES_PATH + "BonusStageIngame.tscn")

func build_menu_principal():
	return load(GlobalConfig.SCENES_PATH + "Menu/MenuPrincipal.tscn").instance()

func build_menu_play():
	return load(GlobalConfig.SCENES_PATH + "Menu/Play.tscn").instance()

func build_menu_opciones():
	return load(GlobalConfig.SCENES_PATH + "Menu/Opciones.tscn").instance()

func build_menu_creditos():
	return load(GlobalConfig.SCENES_PATH + "Menu/Creditos.tscn").instance()

func build_alert_reset_required():
	return load(GlobalConfig.SCENES_PATH + "Menu/ResetRequired.tscn").instance()

func build_title_screen():
	return load(GlobalConfig.SCENES_PATH + "TitleScreen.res").instance()

func build_pause_screen():
	return load(GlobalConfig.UIOBJECTS_PATH + "PauseScreen.res").instance()

func build_bonus_pause_screen():
	return load(GlobalConfig.UIOBJECTS_PATH + "BonusPauseScreen.res").instance()

func build_levelcompleted_screen():
	return load(GlobalConfig.UIOBJECTS_PATH + "LevelCompleted.res").instance()

func build_death_screen():
	return load(GlobalConfig.UIOBJECTS_PATH + "DeathAnim.res").instance()

func build_gameover_screen():
	return load(GlobalConfig.UIOBJECTS_PATH + "GameOver.res").instance()

func build_cutscene_end():
	return load(GlobalConfig.UIOBJECTS_PATH + "BonusEnd.res").instance()
