extends Node

export(float) var movement_speed  : float = 16.0

onready var host = get_parent()

var camera : Camera
var default_position : Vector3
var power : Vector3
var decay : float
var timer : float

func hit(new_power : Vector3, new_decay : float):
	set_physics_process(true)
	timer = .0
	decay = new_decay
	power = -new_power

func initialize(new_camera : Camera):
	camera = new_camera
	default_position = camera.translation

func _restore():
	host.camera.translation = default_position
	set_physics_process(false)

func _physics_process(delta : float):
	var t = sin(timer * movement_speed)
	camera.translation.x = (t * (power.x) + default_position.x)
	camera.translation.y = (t * (power.y) + default_position.y)
	camera.translation.z = (t * (power.z) + default_position.z)
	
	power = lerp(power, Vector3.ZERO, delta * decay)
	timer += delta
	if power.length() < .01:
		_restore()
	elif power.length() < .25:
		decay = lerp(decay, .1, delta)

func _ready():
	set_physics_process(false)
