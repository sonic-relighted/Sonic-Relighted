extends Node

onready var host = get_parent()

var default_zoom
var value

func _ready():
	set_process(false)
	set_physics_process(false)

func doom(new_value : float):
	value = new_value
	set_physics_process(true)

func end():
	value = .0

func _physics_process(delta):
	if value == .0:
		host.camera.fov = lerp(host.camera.fov, default_zoom, delta)
		if host.camera.fov == default_zoom:
			set_physics_process(false)
	else:
		var rnd = randf() * value - value / 2
		host.camera.fov = lerp(host.camera.fov, default_zoom + rnd, delta)
