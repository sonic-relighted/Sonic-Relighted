extends Node

const ANGLE : float = -PI/2

export(float) var field_of_view : float = 15.0
export(float) var vertical_angle : float = .5
export(Vector3) var fix_translation : Vector3 = Vector3(0, 4, 10)
export(float)   var fix_fov : float = 20.0

onready var host = get_parent()
onready var applied : bool = false

var previous_angle : float
var previous_vertical_angle : float
var previous_translation : Vector3
var previous_fov : float

func _ready():
	set_physics_process(false)

func set_back():
	if !host.fixed:
		previous_angle = host.rotation_manager.angle
		previous_vertical_angle = host.rotation.x
		previous_translation = host.camera.translation
		previous_fov = host.camera.fov
	fix_fov = 18
	fix_translation=Vector3(0, 5, 7)
	vertical_angle = 1
	host.rotation_manager.angle = ANGLE
	host.fixed = true
	applied = true
	set_physics_process(true)

func restore():
	host.rotation_manager.angle = previous_angle
	set_physics_process(true)
	applied = false

func _physics_process(delta):
	if applied and host.camera.fov < field_of_view:
		host.camera.translation = lerp(host.camera.translation, fix_translation, delta * 2)
		host.camera.fov = lerp(host.camera.fov, fix_fov, delta * 2)
		host.camera.fov = lerp(host.camera.fov, field_of_view, 2.1 * delta)
		host.rotation.x = lerp(host.rotation.x, vertical_angle, 1.5 * delta)
	elif !applied:
		host.camera.translation = lerp(host.camera.translation, previous_translation, delta * 2)
		host.camera.fov = lerp(host.camera.fov, previous_fov, delta * 2)
		if host.camera.fov > host.default_fov:
			host.camera.fov = lerp(host.camera.fov, host.default_fov, 2.1 * delta)
		else:
			host.camera.fov = host.default_fov
		if host.rotation.x > previous_vertical_angle:
			host.rotation.x = lerp_angle(host.rotation.x, previous_vertical_angle, 1.5 * delta)
		else:
			host.rotation.x = previous_vertical_angle
		if abs(host.camera.fov - host.default_fov) < .01 and abs(host.rotation.x - previous_vertical_angle) < .01:
			#Sucede que tarda mucho en suceder esto, entonces
			#si salimos de este modo y justo nos tapa algo la camara, la rotacion
			#no comienza porque host.fixed == true
			host.camera.fov = host.default_fov
			host.rotation.x = previous_vertical_angle
			host.fixed = false
			set_physics_process(false)
