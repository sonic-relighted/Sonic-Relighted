extends Camera

export(String) var output_directory_path : String = "C:/Gameplay/"
export(bool)   var active : bool = false setget set_active,get_active

onready var n : int = 0

func _ready():
	end()

func set_active(new_active):
	active = new_active
	if active:
		follow_player()
	else:
		end()

func get_active():
	return active

func follow_player():
	n = 0
	set_process(true)
	make_current()

func end():
	set_process(false)
	current = false

func _process(delta : float):
	if Instances.player == null: return
	make_current()
	look_at(Instances.player.global_transform.origin, Vector3.UP)
	var img = get_viewport().get_texture().get_data()
	var img_name = output_directory_path + "/frame_" + _get_frame_number() + ".png"
	img.save_png(img_name)
	n += 1

func _get_frame_number() -> String:
	var nn = str(n)
	while nn.length() < 5:
		nn = "0" + nn
	return nn
