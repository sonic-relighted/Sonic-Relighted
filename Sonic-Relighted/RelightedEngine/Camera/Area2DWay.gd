tool

extends Area

export(float)   var displacement : float = .0
export(Vector3) var size : Vector3 = Vector3.ONE setget set_size, get_size

onready var player_entered : bool = false

func set_size(new_size):
	size = new_size
	if Engine.editor_hint: $CollisionShape.scale = size

func get_size():
	return size

func _physics_process(delta : float):
	if Engine.editor_hint: return
	if player_entered:
		_attract_player(delta)

func _attract_player(delta : float):
	var target = Instances.player
	if target == null: return
	target.global_transform.origin.x = lerp(
			target.global_transform.origin.x,
			global_transform.origin.x + displacement,
			delta * target.velocity.length()
			)

func _ready():
	if not Engine.editor_hint: $CollisionShape.scale = size

func _on_Area2DWay_body_entered(body):
	if body.is_in_group("Player"):
		player_entered = true
		body.input_manager.is_25D = true

func _on_Area2DWay_body_exited(body):
	if body.is_in_group("Player"):
		player_entered = false
		body.input_manager.is_25D = false
