extends Spatial

class_name PlayerCamera, "res://RelightedEngine/EditorIcons/Camera.png"

export(Vector3) var follow_speed = Vector3(100, 130, 100)
export(float) var wait_rotating_time : float = 1.5

onready var rotation_manager        = $RotationManager
onready var displacement_manager    = $DisplacementManager
onready var fixed_angles_manager    = $FixedAnglesManager
onready var doom_manager            = $DoomManager
onready var direction_hit_manager   = $DirectionHitManager
onready var level_completed_manager = $LevelCompleteManager
onready var near_camera : Spatial   = $NearCamera
onready var camera      : Camera    = $Camera

onready var default_fov = camera.fov

onready var fixed : bool = false

func disable():
	rotation_manager.set_physics_process(false)
	displacement_manager.set_physics_process(false)

func enable():
	rotation_manager.set_physics_process(true)
	displacement_manager.set_physics_process(true)

func initialize(seek : Spatial, angle : float):
	rotation_manager.initialize(seek, -3.0 * PI / 4.0 - angle)
	displacement_manager.target = seek
	displacement_manager.real_translation = seek.translation
	doom_manager.default_zoom = camera.fov
	direction_hit_manager.initialize(camera)
	camera.current = true
	doom_manager.end()
	level_completed_manager.target = seek
	near_camera.target = seek
	rotation.y = rotation_manager.angle

func set_back():
	fixed_angles_manager.set_back()

func restore():
	fixed_angles_manager.restore()
