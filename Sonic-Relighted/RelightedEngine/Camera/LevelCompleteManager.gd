extends Node

export(float)   var distance      : float = 3.0
export(float)   var height        : float = 1.4
export(int)     var fov           : int = 35
export(Vector3) var lookat_offset : Vector3 = Vector3.UP * .4

onready var host = get_parent()

var target

func activate():
	_stop_processes()
	set_physics_process(true)

func _stop_processes():
	for child in host.get_children():
		child.set_physics_process(false)
		child.set_process(false)

func _physics_process(delta : float):
	var angle = -target.model.rotation.y + AngleConstants.SEMI_PI
	var x = cos(angle) * distance + target.global_transform.origin.x
	var z = sin(angle) * distance + target.global_transform.origin.z
	var y = height + target.global_transform.origin.y
	var dest_pos = Vector3(x, y, z)
	var delta_spd = delta * 2.5
	host.camera.fov = lerp(host.camera.fov, fov, delta_spd)
	host.camera.look_at(
			target.global_transform.origin + lookat_offset,
			Vector3.UP)
	host.camera.global_transform.origin = lerp(
			host.camera.global_transform.origin,
			dest_pos,
			delta_spd)
	host.global_transform.origin = lerp(
			host.global_transform.origin,
			target.global_transform.origin,
			delta_spd)

func _ready():
	set_physics_process(false)
