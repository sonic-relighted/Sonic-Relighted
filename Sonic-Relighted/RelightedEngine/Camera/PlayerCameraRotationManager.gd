extends Node

const MIN_ROTATION = .005
const INCR_ROTATION = .004

onready var host = get_parent()

onready var angle : float = host.rotation.y
onready var is_rotating : bool = false
onready var wait_rotating : float = .0

var target : Spatial

func initialize(seek : Spatial, new_angle : float):
	target = seek
	is_rotating = false
	angle = new_angle
	move_l()
	move_r()

func move_l():
	angle -= AngleConstants.SEMI_PI
	while angle < .0: angle += AngleConstants.TWO_PI

func move_r():
	angle += AngleConstants.SEMI_PI
	while angle > AngleConstants.TWO_PI: angle -= AngleConstants.TWO_PI

func _physics_process(delta : float):
	if target == null: return
	_camera_rotation(delta)

func _camera_rotation(delta : float):
	var diff = _get_rot_diff(host.rotation.y, angle)
	if abs(diff) < MIN_ROTATION:
		host.rotation.y = angle
		angle = host.rotation.y
		while angle > AngleConstants.TWO_PI: angle -= AngleConstants.TWO_PI
		while angle < 0: angle += AngleConstants.TWO_PI
		is_rotating = false
	elif diff > 0:
		host.rotation.y -= (INCR_ROTATION + diff / 50)
	elif diff < 0:
		host.rotation.y += (INCR_ROTATION - diff / 50)

func _get_rot_diff(angle1 : float, angle2 : float) -> float:
	var diff = angle1 - angle2
	if diff < -PI:
		diff += PI * 2
	elif diff > PI:
		diff -= PI * 2
	return diff
