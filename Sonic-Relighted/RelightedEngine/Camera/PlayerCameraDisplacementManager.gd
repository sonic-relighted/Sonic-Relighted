extends Node

onready var host = get_parent()

onready var real_translation : Vector3 = host.translation

var target

func _physics_process(delta : float):
	if target == null: return
	var target_translation = target.translation
	var target_velocity = target.velocity * 130 * delta
	var diff : Vector3 = target_translation - real_translation
	_displacement(diff, target_velocity, delta)

func _displacement(diff : Vector3, velocity : Vector3, delta : float):
	var move_to : Vector3 = diff * delta
	move_to.x += velocity.x / host.follow_speed.x
	move_to.z += velocity.z / host.follow_speed.z
	move_to.y += velocity.y / host.follow_speed.y
	real_translation = real_translation + move_to
	host.translation = real_translation
