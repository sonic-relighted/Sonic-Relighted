tool
extends Area

class_name AreaChangeCameraAngle, "res://RelightedEngine/EditorIcons/AreaCamera.png"

export(Vector3) var size : Vector3 = Vector3.ONE setget set_size, get_size

func set_size(value):
	$Collider.scale.x = value.x
	$Collider.scale.y = value.y
	$Collider.scale.z = value.z
	size = value

func get_size():
	return size

func _on_AreaChangeCameraAngle_body_entered(body):
	if body.is_in_group("Player"):
		Instances.camera.set_back()

func _on_AreaChangeCameraAngle_body_exited(body):
	if body.is_in_group("Player"):
		Instances.camera.restore()
