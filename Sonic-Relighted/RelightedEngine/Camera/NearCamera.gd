extends Spatial

export(float) var fov  : float = 80.0
export(float) var far  : float = 40.0
export(float) var time : float = .5

onready var tween    : Tween = $Tween
onready var host = get_parent()

onready var col_count : int = 0
onready var active    : bool = false


var target

func _activate():
	time = 2.0
	tween.interpolate_property(host.camera, "fov", host.camera.fov, fov, time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.interpolate_property(host.camera, "translation", host.camera.translation, translation, time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()
	active = true
	set_physics_process(true)

func _deactivate():
	time = 2.0
	tween.interpolate_property(host.camera, "fov", host.camera.fov, host.camera.default_fov, time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.interpolate_property(host.camera, "translation", host.camera.translation, host.camera.default_pos, time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.interpolate_property(host.camera, "rotation:x", host.camera.rotation.x, host.camera.default_rot.x, time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()
	active = false
	set_physics_process(false)

func _physics_process(delta : float):
	if not target: return
	host.global_transform.origin.x = target.global_transform.origin.x
	host.global_transform.origin.z = target.global_transform.origin.z
	var deltax : float = delta * 10.0
	if col_count > 0:
		host.camera.translation.x = lerp(host.camera.translation.x, .0, deltax)
		host.camera.translation.z = lerp(host.camera.translation.z, .0, deltax)
		host.camera.rotation.x = lerp(host.camera.rotation.x, host.camera.default_rot.x -.6, delta)
#		host.camera.rotation.z = lerp(host.camera.rotation.z, host.camera.default_rot.z -.1, deltax)
	else:
		host.camera.translation.x = lerp(host.camera.translation.x, translation.x, deltax)
		host.camera.translation.z = lerp(host.camera.translation.z, translation.z, deltax)
		host.camera.rotation.x = lerp(host.camera.rotation.x, host.camera.default_rot.x, delta)
#		host.camera.rotation.z = lerp(host.camera.rotation.z, host.camera.default_rot.z, deltax)

func _on_ActivationArea_area_entered(area : Area):
	_activate()

func _on_ActivationArea_area_exited(area : Area):
	_deactivate()
	
func _on_WallsDetectionArea_body_entered(body):
	if body.is_in_group("Player"): return
	col_count += 1

func _on_WallsDetectionArea_body_exited(body):
	if body.is_in_group("Player"): return
	col_count -= 1

func _ready():
	set_physics_process(false)
