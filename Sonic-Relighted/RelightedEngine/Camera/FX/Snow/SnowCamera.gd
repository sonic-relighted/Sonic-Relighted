extends Spatial

export(Vector3) var displace : Vector3 = Vector3(0, 8, 0)

onready var particles = $Particles

func toggle():
	particles.emitting = !particles.emitting

func _physics_process(delta):
	var target = Instances.camera
	if target == null: return
	displace.y = 6.7
	global_transform.origin = target.global_transform.origin + displace
