tool
extends Area

signal stopped
signal started

export(Vector3)  var size : Vector3 = Vector3.ONE * 5 setget set_size, get_size
export(NodePath) var fx   : NodePath

onready var fx_instance : Spatial = get_node(fx)

func set_size(new_size):
	size = new_size
	if Engine.editor_hint: $CollisionShape.scale = size

func get_size():
	return size

func reset():
	collision_layer = 1

func _on_SnowTrigger_body_entered(body):
	if body.is_in_group("Player"):
		fx_instance.toggle()
		emit_signal("started", self)

func _on_SnowTrigger_body_exited(body):
	if body.is_in_group("Player"):
		fx_instance.toggle()
		emit_signal("stopped", self)

func _ready():
	if not Engine.editor_hint: $CollisionShape.scale = size
