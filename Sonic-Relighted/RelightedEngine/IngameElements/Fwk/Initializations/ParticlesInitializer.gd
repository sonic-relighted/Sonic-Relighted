extends Spatial

signal end

enum Step {
	IDLE,
	INSERT,
	END
}

export(bool) var enabled : bool = true
export(int)  var particle_count : int = 50
export(int)  var wait_frames : int = 2

var skipframes   : int
var current_step : int = Step.IDLE
var current_i    : int
var particles    : Array

func start(collections):
	particles = collections.particles
	if enabled:
		set_physics_process(true)
		current_step = Step.INSERT
		skipframes   = wait_frames
		current_i    = 0
	else:
		emit_signal("end")

func _physics_process(delta : float):
	if skipframes < 1:
		skipframes = wait_frames
	else:
		skipframes -= 1
		return
	match(current_step):
		Step.INSERT: _insert()
		Step.END:    _end()

func _insert():
	var i = 0
	while i < particle_count and current_i < particles.size():
		var particle = particles[current_i].duplicate(0)
#		particle.process_material = particles[current_i].process_material
		add_child(particle)
		particle.show()
		particle.emitting = true
		i += 1
		current_i += 1
		if current_i >= particles.size():
			skipframes = wait_frames
			current_step = Step.END
			i = particle_count + 1

func _end():
	set_physics_process(false)
	emit_signal("end")

func _ready():
	set_physics_process(false)
