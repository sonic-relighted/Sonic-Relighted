extends Camera

signal end

enum Step {
	IDLE,
	PREPARE,
	SHOW,
	FLY,
	RESTORE,
	END
}

export(bool) var enabled : bool = true

onready var current_step : int = Step.IDLE

var objs            : Array
var materials       : Array
var current_i       : int
var visibilizations : Array

const UP = Vector3.UP

func start(collections):
	if enabled:
		set_physics_process(true)
		objs = collections.objs
		materials = collections.materials
		current_i = 0
		current_step = Step.PREPARE
		visibilizations = []

func _physics_process(delta : float):
	match(current_step):
		Step.PREPARE: _prepare()
		Step.SHOW:    _show()
		Step.FLY:     _fly()
		Step.RESTORE: _restore()
		Step.END:     _end()

func _prepare():
	if not SettingsData.data.transparencies:
		for material in materials:
			if material is SpatialMaterial:
				material.flags_transparent = false
	current_step = Step.SHOW

func _show():
	for obj in objs:
		visibilizations.append(obj.visible)
		obj.show()
	current_step = Step.FLY

func _restore():
	var i : int = 0
	for obj in objs:
		if not visibilizations[i]:
			obj.hide()
		i += 1
	current_step = Step.END

func _fly():
	current = true
	global_transform.origin = objs[current_i].global_transform.origin + UP
	current_i += 1
	if current_i >= objs.size():
		current_step = Step.RESTORE

func _end():
	emit_signal("end")
	current = false
	Instances.camera.camera.make_current()
	set_physics_process(false)

func _ready():
	set_physics_process(false)
