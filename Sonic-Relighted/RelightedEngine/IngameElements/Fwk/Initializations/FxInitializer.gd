extends Spatial

signal end

enum Step {
	IDLE,
	WATER_ON,
	WATER_OFF,
	FLASH,
	END
}

export(bool) var enabled : bool = true

var current_step : int = Step.IDLE
var current_i    : int

func start(collections):
	yield(get_tree(), "idle_frame")
	if enabled:
		set_physics_process(true)
		current_step = Step.WATER_ON
		current_i    = 0
	else:
		emit_signal("end")

func _physics_process(delta : float):
	match(current_step):
		Step.WATER_ON:  _water_on()
		Step.WATER_OFF: _water_off()
		Step.FLASH:     _flash()
		Step.END:       _end()

func _water_on():
	Instances.fxshader.underwater(true)
	current_step = Step.WATER_OFF

func _water_off():
	Instances.fxshader.underwater(false)
	current_step = Step.FLASH

func _flash():
	Instances.fxshader.flash()
	current_step = Step.END

func _end():
	set_physics_process(false)
	emit_signal("end")

func _ready():
	set_physics_process(false)
