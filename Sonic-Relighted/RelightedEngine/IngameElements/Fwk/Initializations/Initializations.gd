extends Spatial

signal initialization_end
signal initialization_step

onready var collections = $Collections

onready var host = get_parent()

func _physics_process(delta):
	if Instances.camera and is_instance_valid(Instances.camera):
		global_transform.origin = Instances.camera.global_transform.origin

func initialize():
	yield(get_tree(), "idle_frame")
	emit_signal("initialization_step", "")
	yield(get_tree().create_timer(.1), "timeout")
	collections.init(host)
	for child in get_children():
		if child.has_method("start"):
			emit_signal("initialization_step",
					child.get_name().replace("Initializer", ""))
			child.start(collections)
			yield(child, "end")
	yield(get_tree().create_timer(.25), "timeout")
	emit_signal("initialization_end")
	queue_free()

func _ready():
	initialize()

