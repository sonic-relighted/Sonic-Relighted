extends Node

var particles
var sprites
var animated_sprites
var materials
var objs

func init(host : Node):
	particles = []
	sprites = []
	animated_sprites = []
	materials = []
	objs = []
	_find_all(host)
	print(materials.size())

func _find_all(node : Node):
	for child in node.get_children():
		# ======== Sprites ======== #
		if child is Sprite3D:
			_add_sprite(child)
#			if child.material_override:
#				_add_material(child, child.material_override)
		elif child is AnimatedSprite3D:
			_add_animated_sprite(child)
		# ======== Particles ======== #
		elif child is Particles:
			_add_particle(child)
		# ======== Materials ======== #
		elif child is CSGShape:
			_add_material(child, child.material)
		elif child is MeshInstance:
			for j in range(child.get_surface_material_count()):
				_add_material(child, child.mesh.surface_get_material(j))
#		elif child is ColorRect:
#			_add_material(child, child.material)
		# ======== Recursive ======== #
		if child is Spatial:
			_find_all(child)

# ======== Add ======== #
func _add_sprite(sprite):
	if sprite and not _has_sprite(sprite):
		objs.append(sprite)
		sprites.append(sprite)

func _add_animated_sprite(sprite):
	if sprite and not _has_animated_sprite(sprite):
		objs.append(sprite)
		animated_sprites.append(sprite)

func _add_particle(particle):
	if particle and not _has_particle(particle):
		particles.append(particle)

func _add_material(node : Node, material : Material):
#	print(node.get_name(), "  ", material)
	if material and not materials.has(material):
		objs.append(node)
		materials.append(material)

# ======== Check ======== #
func _has_sprite(sprite : Sprite3D):
	for s in sprites:
		if s.texture == sprite.texture: return true
	return false

func _has_animated_sprite(sprite : AnimatedSprite3D):
	for s in animated_sprites:
		if s.frames == sprite.frames: return true
	return false

func _has_particle(particle : Particles):
	for p in particles:
		if particle.process_material == p.process_material:
			return true
	return false
