extends Spatial

export(float)   var force         : float   = 15.0
export(Vector3) var origin_offset : Vector3 = Vector3.DOWN * .25

onready var animator = $AnimationPlayer
onready var emission_tween = $EmissionTween
onready var sfx = $Sfx

var mat

func _on_Area_body_entered(body):
	if body.is_in_group("Player"):
		_fx()
		_throw(body)

func _fx():
	sfx.play()
	animator.play("Bounce")

func _throw(player : Player):
	var po : Vector3 = player.global_transform.origin
	var so : Vector3 = global_transform.origin + origin_offset
	var direction : Vector3 = (po - so).normalized()
	player.velocity = direction * force
	player.is_bouncing = true
	_start_tween()

func _start_tween():
	emission_tween.interpolate_property(mat, "emission_energy", 1.5, .25, .5, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	emission_tween.start()

func _ready():
	for mesh in GroupFinder.find_nodes_by_type($Bumper, MeshInstance):
		var ins : MeshInstance = mesh
		mat = ins.mesh.surface_get_material(0).duplicate()
		mat.emission_energy = .25
		ins.set_surface_material(0, mat)
