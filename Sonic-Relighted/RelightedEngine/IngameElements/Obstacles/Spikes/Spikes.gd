tool
extends Spatial

class_name Spikes, "res://RelightedEngine/EditorIcons/Spikes.png"

export(Vector3)  var area_size            : Vector3 = Vector3(.18, .1, .18) setget set_area_size, get_area_size
export(DamageArea.ShapeMode) var area_shape_mode : int = DamageArea.ShapeMode.CUBE setget set_area_shape_mode, get_area_shape_mode
export(bool)     var destructible         : bool = false setget set_destructible, get_destructible
export(Vector3)  var area_position        : Vector3 = Vector3.UP * 0.4 setget set_area_position, get_area_position
export(Resource) var model_resource       : Resource setget set_model_resource, get_model_resource
export(bool)     var timed_toggle         : bool    = false
export(float)    var timed_toggle_seconds : float   = 1.0

onready var switch   : SwitchManager     = $SwitchManager
onready var timer    : Timer             = $ToggleTimer
onready var sfx      : AudioStreamPlayer = $Sfx
onready var area_col : CollisionShape    = $DamageArea/CollisionShape

onready var destroyed : bool = false

var spikes   : Spatial
var bodies   : Array
var animator : AnimationPlayer

func set_area_size(new_size : Vector3):
	area_size = new_size
	if $DamageArea:
		$DamageArea.scale = new_size

func get_area_size() -> Vector3: return area_size

func set_area_shape_mode(new_mode : int):
	area_shape_mode = new_mode
	if $DamageArea: $DamageArea.set_shape_mode(area_shape_mode)

func get_area_shape_mode() -> int: return area_shape_mode

func set_area_position(new_position : Vector3):
	area_position = new_position
	if $DamageArea:
		$DamageArea.translation = new_position

func get_area_position() -> Vector3: return area_position

func set_destructible(new_destr : bool):
	destructible = new_destr
	if $DamageArea:
		$DamageArea.destructible = destructible

func get_destructible() -> bool: return destructible

func set_model_resource(new_glb : Resource):
	model_resource = new_glb
	if $Anchor:
		if $Anchor.get_child_count():
			$Anchor.get_child(0).queue_free()
		var glb = model_resource.instance()
		$Anchor.add_child(glb)

func get_model_resource() -> Resource: return model_resource

func enabling(delta : float) -> bool:
	area_col.disabled = false
	area_col.show()
	if spikes.translation.y < .0:
		spikes.translation.y += delta
		return false
	spikes.translation.y = .0
	timer.start()
	return true

func disabling(delta : float) -> bool:
	if spikes.translation.y > -.5:
		if spikes.translation.y < -.3:
			area_col.hide()
			area_col.disabled = true
		spikes.translation.y -= delta
		return false
	spikes.translation.y = -.5
	area_col.disabled = true
	timer.start()
	return true

func reset():
	$Resetable.reset()

func _on_ToggleTimer_timeout():
	switch.toggle()

func _on_DamageArea_damage():
	sfx.play()

func _on_DamageArea_destroyed():
	if destroyed: return
	destroyed = true
	area_col.hide()
	area_col.disabled = true
	for b in bodies:
		b.collision_layer = 0
		b.collision_mask = 0
	animator.play("Destroy")
	yield(animator, "animation_finished")
	hide()

func _ready():
	if Engine.editor_hint:
		set_process(false)
		set_physics_process(false)
	else:
		var anchor = $Anchor
		spikes = anchor.get_child(0).get_child(0)
		bodies = GroupFinder.find_nodes_by_type(anchor, StaticBody)
		if destructible:
			animator = GroupFinder.find_nodes_by_type(anchor, AnimationPlayer)[0]
		if timed_toggle:
			switch.toggle()
			timer.wait_time = timed_toggle_seconds
