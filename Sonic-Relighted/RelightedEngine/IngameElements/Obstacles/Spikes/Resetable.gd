extends Node

onready var host = get_parent()

func reset():
	host.destroyed = false
	host.area_col.show()
	host.area_col.disabled = false
	for b in host.bodies:
		b.collision_layer = 1
		b.collision_mask = 1
	host.show()
