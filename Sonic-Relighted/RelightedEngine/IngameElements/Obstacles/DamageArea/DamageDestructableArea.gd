tool
extends DamageArea

class_name DamageDestructableArea

signal destroyed

enum DestructMode {
	ROLL,
	STOMP
}

export(bool)         var destructible  : bool = true
export(DestructMode) var destruct_mode : int = DestructMode.ROLL
export(Vector3)      var bounce_force  : Vector3 = Vector3(4.0, 0.0, 4.0)

func is_destructible() -> bool: return destructible

func check(player : Player):
	if not destructible: return false
	var sm = player.state_machine
	var cs = sm.current_state
	match destruct_mode:
		DestructMode.ROLL:
			return cs == sm.rolling_state or cs == sm.rollinginf_state
		DestructMode.STOMP:
			return cs == sm.stomp_state or cs == sm.bubble_stomp_state
	return false

func destroy(player : Player):
	emit_signal("destroyed")
	_bounce(player)

func _bounce(player : Player):
	var po : Vector3 = player.global_transform.origin
	var so : Vector3 = global_transform.origin
	var direction : Vector3 = (po - so).normalized()
	player.velocity = direction * bounce_force
#	player.is_bouncing = true
