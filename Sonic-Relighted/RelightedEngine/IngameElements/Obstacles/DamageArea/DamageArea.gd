tool
extends Spatial

class_name DamageArea

enum ShapeMode {
	CUBE,
	SPHERE
}

export(Vector3)   var size         : Vector3 = Vector3.ONE setget set_size,get_size
export(ShapeMode) var shape_mode   : int     = ShapeMode.CUBE setget set_shape_mode, get_shape_mode
export(Vector3)   var damage_force : Vector3 = Vector3.UP * 15.0

func is_destructible() -> bool: return false

func set_size(new_size : Vector3):
	size = new_size
	if $CollisionShape:
		$CollisionShape.scale = size

func get_size() -> Vector3: return size

func set_shape_mode(new_mode : int):
	shape_mode = new_mode
	if $CollisionShape:
		match shape_mode:
			ShapeMode.CUBE: $CollisionShape.shape = BoxShape.new()
			ShapeMode.SPHERE: $CollisionShape.shape = SphereShape.new()

func get_shape_mode() -> int: return shape_mode

func get_damage_force() -> Vector3: return damage_force
