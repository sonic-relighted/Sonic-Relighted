tool

extends Spatial

export(bool) var enabled : bool = true
export(int, "North", "East", "South", "West") var direction : int = 0 setget set_direction,get_direction

func _enter_tree():
	if !Engine.is_editor_hint():
		$CosaParaVerEditando.queue_free()
		$CosaParaVerEditando2.queue_free()

func _ready():
	if not Engine.editor_hint:
		hide()

func set_direction(new_value):
	direction = new_value
	match direction:
		0: rotation.y = 0
		1: rotation.y = 3*PI/2
		2: rotation.y = PI
		3: rotation.y = PI/2

func get_direction():
	return direction
