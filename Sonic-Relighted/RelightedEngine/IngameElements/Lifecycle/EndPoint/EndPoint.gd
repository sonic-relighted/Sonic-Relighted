tool

extends Area

export(float) var size : float = 1.0 setget set_size, get_size

func set_size(new_size : float):
	size = new_size
	if $CollisionShape:
		$CollisionShape.scale = Vector3.ONE * size

func get_size() -> float: return size

func _on_EndPoint_body_entered(body):
	if body.is_in_group("Player"):
		body.emit_signal("complete_level")
