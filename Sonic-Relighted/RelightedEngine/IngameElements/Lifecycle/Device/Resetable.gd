extends Node

onready var host = get_parent()

func _ready():
	set_process(false)
	set_physics_process(false)

func reset():
	if host.persisted: return
	host.animator.play("Idle")
	if is_instance_valid(host.luz):
		host.luz.show()
	if SettingsData.data.transparencies:
		host.light_up.show()
	host.enabled = true
	host.screen.frame = 0
	host.area_col.disabled = false
