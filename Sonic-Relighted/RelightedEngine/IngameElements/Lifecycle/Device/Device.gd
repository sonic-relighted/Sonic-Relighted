extends KinematicBody

class_name Device, "res://RelightedEngine/EditorIcons/Device.png"

onready var audio     : AudioStreamPlayer = $AudioTurnOff
onready var animator  : AnimationPlayer   = $Device/AnimationPlayer
onready var area_col  : CollisionShape    = $Area/CollisionShape
onready var luz       : OmniLight         = $OmniLight
onready var light_up  : Sprite3D          = $LightUp
onready var screen    : Sprite3D          = $Screen
onready var enabled   : bool              = true
onready var persisted : bool              = false

func reset():
	$Resetable.reset()

func persist():
	if enabled == false:
		persisted = true

func _ready():
	animator.get_animation("Idle").loop = true
	animator.play("Idle",  0, 1, false)
	if not SettingsData.data.transparencies:
		light_up.hide()

func _process(delta):
	var frame : int = screen.frame + 1
	if enabled:
		if frame > 39: frame = 0
	else:
		if frame > 54: frame = 45 
	screen.frame = frame

func _on_Area_body_entered(body):
	if body.is_in_group("Player"):
		animator.play("Disabled")
		if is_instance_valid(luz):
			luz.hide()
		light_up.hide()
		enabled = false
		screen.frame = 40
		area_col.disabled = true
		audio.play()
		body.recolect(self)
