extends Spatial

class_name IngameObjectShadow

onready var frame_count  : int = 0

var target : Spatial

func on_target_physics_processs_changed(enabled : bool):
#	set_physics_process(enabled)
	set_process(enabled)
	visible = enabled

#func _physics_process(delta : float):
#	frame_count += 1
#	if (frame_count % 2) > 0: return
#	global_transform.origin = target.global_transform.origin

func _process(delta : float):
	frame_count += 1
	if (frame_count % 2) > 0: return
	global_transform.origin = target.global_transform.origin
