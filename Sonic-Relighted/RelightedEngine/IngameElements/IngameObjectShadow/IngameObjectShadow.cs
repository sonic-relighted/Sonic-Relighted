using Godot;
using System;

public class IngameObjectShadow : Spatial
{
	Spatial target;

	public override void _Ready() {
	}

	public void on_target_physics_processs_changed(bool enabled) {
		SetPhysicsProcess(enabled);
		Visible = enabled;
	}

	public override void _PhysicsProcess(float delta) {
		var transform = GlobalTransform;
		transform.origin = target.GlobalTransform.origin;
		GlobalTransform = transform;

	}

}
