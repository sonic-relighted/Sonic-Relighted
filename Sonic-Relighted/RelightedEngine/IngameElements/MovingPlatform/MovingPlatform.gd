extends Spatial

class_name MovingPlatform

signal at_end
signal at_begin

enum AccelerationStatus {
	ACCELERATING, CONSTANT, DECELERATING, END
}
enum PositionStatus {
	AT_BEGIN, AT_END
}

export(Vector3) var movement      : Vector3 = Vector3.FORWARD
export(float)   var speed         : float   = 1.0 # Velocidad de movimiento
export(float)   var acceleration  : float   = 5.0 # Aceleracion
export(float)   var deceleration  : float   = 5.0 # Deceleracion
export(float)   var distance      : float   = 5.0 # Distancia a recorrer
export(float)   var initial_delay : float   = 0.0 # Tiempo de espera inicial
export(float)   var toggle_delay  : float   = 1.0 # Tiempo a esperar despues de llegar a destino
export(bool)    var auto_start    : bool    = true
export(bool)    var back_to_begin : bool    = true
export(bool)    var back_to_end   : bool    = true

onready var switch              : SwitchManager = $SwitchManager
onready var toggle_timer        : Timer         = $ToggleTimer
onready var initial_delay_timer : Timer         = $InitialDelayTimer
onready var resetable                        = $Resetable
onready var initial_position : Vector3       = translation
onready var current_position : Vector3       = Vector3.ZERO
onready var current_distance : float         = distance
onready var current_speed    : float         = .0
onready var current_position_status     : int = PositionStatus.AT_BEGIN
onready var current_acceleration_status : int = AccelerationStatus.ACCELERATING
onready var constant_linear_velocity    : Vector3 = Vector3.ZERO

var bodies : Array

func reset():
	resetable.reset()

func start():
	if initial_delay > .0:
		initial_delay_timer.wait_time = initial_delay
	else:
		initial_delay_timer.wait_time = .01
	initial_delay_timer.start()

func enabling(delta : float):
	_update_acceleration(delta)
	if current_acceleration_status == AccelerationStatus.END:
		_move_end(PositionStatus.AT_END)
		return true
	_move(delta, movement, current_speed)
	return false

func disabling(delta : float):
	_update_acceleration(delta)
	if current_acceleration_status == AccelerationStatus.END:
		_move_end(PositionStatus.AT_BEGIN)
		return true
	_move(delta, movement * (-1), current_speed)
	return false

func _update_acceleration(delta : float):
	if current_distance < .0:
		current_acceleration_status = AccelerationStatus.DECELERATING
	if current_acceleration_status == AccelerationStatus.ACCELERATING:
		if current_speed < speed:
			if acceleration < .0: current_speed = speed
			else: current_speed += delta * acceleration
		else:
			current_speed = speed
			current_acceleration_status = AccelerationStatus.CONSTANT
	elif current_acceleration_status == AccelerationStatus.DECELERATING:
		if current_speed > .0:
			if deceleration < .0: current_speed = .0
			else: current_speed -= delta * deceleration
		else:
			current_speed = .0
			current_acceleration_status = AccelerationStatus.END

func _move(delta : float, movement : Vector3, speed : float):
	var deltaspeed = delta * speed
	current_distance -= deltaspeed
	current_position += movement * deltaspeed
	translation = initial_position + current_position
	_set_constant_linear_velocity(movement * speed)

func _stop_move():
	translation = initial_position + current_position
	_set_constant_linear_velocity(Vector3.ZERO)
	current_distance = distance
	current_speed = .0

func _move_end(pos_status : int):
	_stop_move()
	current_position_status = pos_status
	_set_constant_linear_velocity(Vector3.ZERO)
	if pos_status == PositionStatus.AT_BEGIN:
		if back_to_end: toggle_timer.start()
		emit_signal("at_begin")
	elif pos_status == PositionStatus.AT_END:
		if back_to_begin: toggle_timer.start()
		emit_signal("at_end")

func _set_constant_linear_velocity(m : Vector3):
	constant_linear_velocity = m
	for body in bodies:
		body.constant_linear_velocity = m

func _on_ToggleTimer_timeout():
	switch.toggle()
	current_acceleration_status = AccelerationStatus.ACCELERATING

func _on_InitialDelayTimer_timeout():
	switch.toggle()
	current_acceleration_status = AccelerationStatus.ACCELERATING

func _add_bodies(node : Spatial):
	for body in node.get_children():
		if body is StaticBody:
			bodies.append(body)

func _set_group():
	for body in GroupFinder.find_nodes_by_type(self, StaticBody):
		body.add_to_group("MovingBody")

func _ready():
	bodies = []
	_add_bodies(self)
	for spatial in get_children():
		if spatial is Spatial: _add_bodies(spatial)

	movement = movement.normalized()
	toggle_timer.wait_time = toggle_delay
	switch.set_physics_process(false)
	if auto_start:
		start()
	yield(get_tree(), "idle_frame")
#	yield(get_tree().create_timer(.15), "timeout")
	_set_group()
