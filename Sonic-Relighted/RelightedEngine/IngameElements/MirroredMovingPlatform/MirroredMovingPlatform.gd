extends Spatial

class_name MirroredMovingPlatform

export(NodePath) var moving_platform : NodePath

onready var initial_position : Vector3       = translation
onready var current_position : Vector3       = Vector3.ZERO

onready var moving_platform_instance : MovingPlatform = get_node(moving_platform)
onready var bodies                   : Array = []
onready var constant_linear_velocity : Vector3 = Vector3.ZERO

func _physics_process(delta : float):
	var distance = moving_platform_instance.translation - moving_platform_instance.initial_position
	translation = initial_position - distance
	_set_constant_linear_velocity(
			-moving_platform_instance.constant_linear_velocity)

func _set_constant_linear_velocity(m : Vector3):
	constant_linear_velocity = m
	for body in bodies:
		body.constant_linear_velocity = m

func _add_bodies(node : Spatial):
	for body in node.get_children():
		if body is StaticBody:
			bodies.append(body)

func _set_group():
	for body in GroupFinder.find_nodes_by_type(self, StaticBody):
		body.add_to_group("MovingBody")

func _ready():
	bodies = []
	_add_bodies(self)
	for spatial in get_children():
		if spatial is Spatial: _add_bodies(spatial)
	yield(get_tree(), "idle_frame")
	_set_group()
