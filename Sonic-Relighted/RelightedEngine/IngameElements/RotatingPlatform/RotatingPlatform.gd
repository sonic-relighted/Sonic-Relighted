extends Spatial

class_name RotatingPlatform

signal step_done
signal all_steps_done
signal stopped

enum AxisMovement { X, Y, Z }

enum StepType {
	PAUSE,
	ABS_ROT,
	REL_ROT
}

export(bool)          var auto_start    : bool = true
export(bool)          var auto_advance  : bool = true
export(bool)          var in_loop       : bool = false
export(AxisMovement)  var axis_movement : int = AxisMovement.Y
export(float)         var acceleration  : float = 1.0
export(String,MULTILINE) var program : String = "[]"


onready var change_movement_timer = $ChangeMovementTimer
onready var resetable             = $Resetable

onready var initial_rotation  : Vector3 = rotation
onready var current_step      : int     = 0
onready var current_step_data : Dictionary = parse_json("{}")
onready var current_step_type : int = StepType.PAUSE

onready var current_speed : float = .0

var bodies : Array
var steps

func reset():
	resetable.reset()

func start():
	steps = parse_json(program)
	current_step_data = steps[current_step]
	_parse_current()

func stop():
	set_physics_process(false)
	change_movement_timer.stop()
	_set_constant_angular_velocity(Vector3.ZERO)

func _physics_process(delta : float):
	match current_step_type:
		StepType.PAUSE:   _pause_process(delta)
		StepType.ABS_ROT: _abs_rot_process(delta)
		StepType.REL_ROT: _rel_rot_process(delta)

func _finish():
	_set_constant_angular_velocity(Vector3.ZERO)
	set_physics_process(false)
	emit_signal("stopped")

func _pause_process(delta : float):
	_update_current_speed_pause(delta)
	match axis_movement:
		AxisMovement.X:
			if current_speed == .0:
				_finish()
			else:
				rotation.x = rotation.x + delta * current_speed
				_set_constant_angular_velocity(Vector3.RIGHT * current_speed)
		AxisMovement.Y:
			if current_speed == .0:
				_finish()
			else:
				rotation.y = rotation.y + delta * current_speed
				_set_constant_angular_velocity(Vector3.UP * current_speed)
		AxisMovement.Z:
			if current_speed == .0:
				_finish()
			else:
				rotation.z = rotation.z + delta * current_speed
				_set_constant_angular_velocity(Vector3.FORWARD * current_speed)

func _abs_rot_process(delta : float):
	_update_current_speed_abs_rot(delta)
	match axis_movement:
		AxisMovement.X: _abs_rot_process_x(delta)
		AxisMovement.Y: _abs_rot_process_y(delta)
		AxisMovement.Z: _abs_rot_process_z(delta)

func _rel_rot_process(delta : float):
	_update_current_speed_rel_rot(delta)
	match axis_movement:
		AxisMovement.X: _abs_rel_process_x(delta)
		AxisMovement.Y: _abs_rel_process_y(delta)
		AxisMovement.Z: _abs_rel_process_z(delta)

func _abs_rot_process_x(delta : float):
	var tgt = initial_rotation.x + current_step_data.rad
	if tgt < rotation.x: rotation.x = max(tgt, rotation.x + delta * current_speed)
	if tgt > rotation.x: rotation.x = min(tgt, rotation.x + delta * current_speed)
	_set_constant_angular_velocity(Vector3.RIGHT * current_speed)
	if abs(tgt - rotation.x) < .001: _complete_step()

func _abs_rot_process_y(delta : float):
	var tgt = initial_rotation.y + current_step_data.rad
	if tgt < rotation.y: rotation.y = max(tgt, rotation.y + delta * current_speed)
	if tgt > rotation.y: rotation.y = min(tgt, rotation.y + delta * current_speed)
	_set_constant_angular_velocity(Vector3.UP * current_speed)
	if abs(tgt - rotation.y) < .001: _complete_step()

func _abs_rot_process_z(delta : float):
	var tgt = initial_rotation.z + current_step_data.rad
	if tgt < rotation.z: rotation.z = max(tgt, rotation.z + delta * current_speed)
	if tgt > rotation.z: rotation.z = min(tgt, rotation.z + delta * current_speed)
	_set_constant_angular_velocity(Vector3.FORWARD * current_speed)
	if abs(tgt - rotation.z) < .001: _complete_step()

func _abs_rel_process_x(delta : float):
	rotation.x += delta * current_speed
	_set_constant_angular_velocity(Vector3.RIGHT * current_speed)

func _abs_rel_process_y(delta : float):
	rotation.y += delta * current_speed
	_set_constant_angular_velocity(Vector3.UP * current_speed)

func _abs_rel_process_z(delta : float):
	rotation.z += delta * current_speed
	_set_constant_angular_velocity(Vector3.FORWARD * current_speed)

func _update_current_speed_pause(delta : float):
	if current_speed > .0:
		current_speed = max(.0, current_speed - delta * acceleration)
	else:
		current_speed = min(.0, current_speed + delta * acceleration)

func _update_current_speed_abs_rot(delta : float):
	var tgt = initial_rotation.z + current_step_data.rad
	var current
	match axis_movement:
		AxisMovement.X: current = rotation.x
		AxisMovement.Y: current = rotation.y
		AxisMovement.Z: current = rotation.z
	if tgt < current:
		if current_speed > current_step_data.speed:
			current_speed = min(-current_step_data.speed, current_speed + delta * acceleration)
		else:
			current_speed = max(-current_step_data.speed, current_speed - delta * acceleration)
	else:
		if current_speed > current_step_data.speed:
			current_speed = max(current_step_data.speed, current_speed - delta * acceleration)
		else:
			current_speed = min(current_step_data.speed, current_speed + delta * acceleration)

func _update_current_speed_rel_rot(delta : float):
	if current_speed < current_step_data.speed:
		current_speed = min(current_step_data.speed, current_speed + delta * acceleration)
	else:
		current_speed = max(current_step_data.speed, current_speed - delta * acceleration)

func _set_constant_angular_velocity(v : Vector3):
	for body in bodies:
		body.constant_angular_velocity = v

func _on_ChangeMovementTimer_timeout():
	_complete_step()

func _complete_step():
	emit_signal("step_done", current_step)
	var has_parse_current = true
	if current_step >= steps.size() -1:
		emit_signal("all_steps_done")
		current_step = 0
		has_parse_current = false
	else:
		current_step += 1
	if current_step_data.has("brake"):
		current_speed = .0
	current_step_data = steps[current_step]
	if auto_advance and (has_parse_current or in_loop):
		_parse_current()
	else:
		current_step_type = StepType.PAUSE

func _parse_current():
	if current_step_data.has("pause"):
		change_movement_timer.start(current_step_data.pause)
		current_step_type = StepType.PAUSE
	elif current_step_data.has("rad") and current_step_data.has("speed"):
		current_step_type = StepType.ABS_ROT
	elif current_step_data.has("time") and current_step_data.has("speed"):
		current_step_type = StepType.REL_ROT
		change_movement_timer.start(current_step_data.time)
	set_physics_process(true)

func _ready():
	bodies = []
	for body in get_children():
		if body is StaticBody:
			bodies.append(body)

	if auto_start:
		start()
	else:
		stop()
