extends Area

class_name EnableableArea

export(bool) var manage_process         : bool = true
export(bool) var manage_physics_process : bool = true
export(bool) var manage_collision_layer : bool = false
export(bool) var manage_collision_mask  : bool = false

onready var host : Node = get_parent()

onready var active : bool = true

var is_physics_body : bool
var default_collision_mask  : int
var default_collision_layer : int

func disable():
	if active:
		if manage_physics_process: host.set_physics_process(false)
		if manage_process: host.set_process(false)
		if is_physics_body:
			if manage_collision_layer: host.collision_layer = 0
			if manage_collision_mask: host.collision_mask = 0

func enable():
	if active:
		if manage_physics_process: host.set_physics_process(true)
		if manage_process: host.set_process(true)
		if is_physics_body:
			if manage_collision_layer: host.collision_layer = default_collision_layer
			if manage_collision_mask: host.collision_mask = default_collision_mask

func _ready():
	is_physics_body = host is PhysicsBody
	if is_physics_body:
		default_collision_layer = host.collision_layer
		default_collision_mask = host.collision_mask

	$CollisionShape.disabled = true
	yield(get_tree(), "idle_frame")
	disable()
	yield(get_tree(), "idle_frame")
	$CollisionShape.disabled = false

