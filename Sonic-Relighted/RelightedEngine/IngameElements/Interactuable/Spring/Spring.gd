tool

extends Spatial

class_name Spring, "res://RelightedEngine/EditorIcons/Spring.png"

export(Resource) var object       : Resource setget set_object,get_object
export(Vector3) var area_size     : Vector3 = Vector3.ONE  setget set_area_size,get_area_size
export(Vector3) var area_position : Vector3 = Vector3.ZERO setget set_area_position,get_area_position
export(float)   var force         : float = 60.0
export(bool)    var base_collision_enabled : bool = true

onready var audio    : AudioStreamPlayer = $Audio
onready var animator : AnimationPlayer   = $AnimationPlayer
onready var anchor   : Spatial           = $Anchor

func set_object(new_object):
	object = new_object
	if object and $Anchor:
		for child in $Anchor.get_children():
			child.queue_free()
		var instance = object.instance()
		instance.name = "Object"
		$Anchor.add_child(instance, true)
		var scr = load(GlobalConfig.INGAME_ELEMENTS_PATH + "Interactuable/Spring/StaticBody.gd")
		for child in GroupFinder.find_nodes_by_type($Anchor, StaticBody):
			child.add_to_group("Spring")
			child.add_to_group("Homingable")
			child. set_script(scr)

func get_object():
	return object

func set_area_size(new_size):
	area_size = new_size
	if $Area/CollisionShape:
		$Area/CollisionShape.scale = area_size

func get_area_size():
	return area_size

func set_area_position(new_position):
	area_position = new_position
	if $Area:
		$Area.translation = area_position
	
func get_area_position():
	return area_position

func _ready():
	if base_collision_enabled == false:
		var body = $StaticBody
		body.collision_mask = 0
		body.collision_layer = 0

func _on_Area_body_entered(body):
	if body.is_in_group("Player"):
		audio.play()
		var animator : AnimationPlayer = $Anchor/Object/AnimationPlayer
		if animator.is_playing():
			animator.stop()
		$Anchor/Object/AnimationPlayer.play("Action")
		body.velocity = _get_direction() * force

func _get_direction() -> Vector3:
	return Vector3.UP \
			.rotated(Vector3(1,0,0), rotation.x) \
			.rotated(Vector3(0,1,0), rotation.y) \
			.rotated(Vector3(0,0,1), rotation.z)

