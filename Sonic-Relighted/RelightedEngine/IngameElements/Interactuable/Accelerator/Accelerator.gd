tool
extends Spatial

class_name Accelerator

signal do_begin
signal do_spindash
signal do_exit
signal do_end

export(Resource) var resource : Resource setget set_resource,get_resource

onready var animator        : AnimationPlayer   = $AnimationPlayer
onready var player_position : Spatial           = $PlayerPosition
var model_animator          : AnimationPlayer

func set_resource(new_resource : Resource):
	resource = new_resource
	if $Model:
		for c in $Model.get_children(): c.queue_free()
		var instance = resource.instance()
		instance.set_name("acelerador")
		$Model.add_child(instance, true)
		instance.rotation.y = -PI/2
		for child in instance.get_children():
			if child is AnimationPlayer:
				model_animator = child

func get_resource() -> Resource:
	return resource

func get_player_position() -> Vector3:
	return player_position.global_transform.origin

func _on_ActivationArea_body_entered(body):
	if body.is_in_group("Player"):
		_activate(body)
		connect("do_begin",    body.state_machine.onaccelerator_state, "do_begin")
		connect("do_spindash", body.state_machine.onaccelerator_state, "do_spindash")
		connect("do_exit",     body.state_machine.onaccelerator_state, "do_exit")
		connect("do_end",      body.state_machine.onaccelerator_state, "do_end")

func _on_ActivationArea_body_exited(body):
	if body.is_in_group("Player"):
		if body.accelerator == null:
			disconnect("do_begin",    body.state_machine.onaccelerator_state, "do_begin")
			disconnect("do_spindash", body.state_machine.onaccelerator_state, "do_spindash")
			disconnect("do_exit",     body.state_machine.onaccelerator_state, "do_exit")
			disconnect("do_end",      body.state_machine.onaccelerator_state, "do_end")

func _activate(body):
	body.accelerator = self

func started_acceleration():
	animator.play("Action")
	if not model_animator.is_playing():
		model_animator.play(model_animator.get_animation_list()[0])
