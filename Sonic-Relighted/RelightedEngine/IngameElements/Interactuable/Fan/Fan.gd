tool

extends Spatial

class_name Fan, "res://RelightedEngine/EditorIcons/Fan.png"

export(Resource) var object       : Resource setget set_object,get_object
export(Vector3) var area_size     : Vector3 = Vector3.ONE  setget set_area_size,get_area_size
export(bool)    var enabled       : bool = true setget set_enabled,get_enabled
export(float)   var force         : float = 1.0

onready var audio    : AudioStreamPlayer = $Audio
onready var anchor   : Spatial           = $Anchor

onready var bodies_inside : Array = []

func set_object(new_object):
	object = new_object
	if object and $Anchor:
		for child in $Anchor.get_children():
			child.queue_free()
		var instance = object.instance()
		instance.name = "Object"
		$Anchor.add_child(instance, true)
		for child in GroupFinder.find_nodes_by_type($Anchor, StaticBody):
			child.add_to_group("Fan")
		for child in GroupFinder.find_nodes_by_type($Anchor, AnimationPlayer):
			child.get_animation("Action").loop = true

func get_object():
	return object

func set_area_size(new_size):
	area_size = new_size
	if $Area/CollisionShape:
		$Area/CollisionShape.scale = area_size
		$Area.translation.y = area_size.y / 10 

func get_area_size():
	return area_size

func set_enabled(new_enabled : bool):
	enabled = new_enabled
	if $Anchor and $Anchor/Object/AnimationPlayer:
		var animator : AnimationPlayer = $Anchor/Object/AnimationPlayer
		if animator.is_playing():
			animator.stop()
		if enabled:
			$Anchor/Object/AnimationPlayer.play("Action")
		if not Engine.is_editor_hint():
			set_physics_process(enabled)
	if $Area/CollisionShape:
		$Area/CollisionShape.disabled = !enabled

func get_enabled() -> bool:
	return enabled

func _ready():
#	set_physics_process(enabled)
	set_enabled(enabled)

func _on_Area_body_entered(body):
	if body.is_in_group("Player"):
		bodies_inside.append(body)
#		body.velocity /= 8
#		body.wind_force = Vector3.ZERO
		body.is_on_fan = true

func _on_Area_body_exited(body):
	if body.is_in_group("Player"):
		bodies_inside.erase(body)
#		body.wind_force = Vector3.ZERO
		body.is_on_fan = false

func _physics_process(delta):
	if Engine.is_editor_hint(): return
	for body in bodies_inside:
#		audio.play()
		var distance = body.global_transform.origin - global_transform.origin
		var factor = (area_size.y / 10) / (distance.length() / 2) - 1
		var calculated_force = factor
		calculated_force *= calculated_force
		calculated_force *= .3
		calculated_force *= force
		var vel_x = body.velocity.x * .96
		var vel_z = body.velocity.z * .96
		body.velocity = _get_direction() * calculated_force
		body.velocity.x = vel_x
		body.velocity.z = vel_z

func _get_direction() -> Vector3:
	return Vector3.UP \
			.rotated(Vector3(1,0,0), rotation.x) \
			.rotated(Vector3(0,1,0), rotation.y) \
			.rotated(Vector3(0,0,1), rotation.z)
