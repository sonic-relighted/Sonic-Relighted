tool
extends StaticBody

class_name DestructibleBox, "res://RelightedEngine/EditorIcons/DestructibleBox.png"

export(Resource) var object : Resource setget set_object,get_object
export(Vector3)  var size : Vector3 = Vector3.ONE setget set_size,get_size

onready var is_destroyed : bool = false

onready var audio    : AudioStreamPlayer = $Audio
onready var collider : CollisionShape = $CollisionShape
onready var resetable = $Resetable
onready var animator : AnimationPlayer = $Anchor/Object/AnimationPlayer

func set_object(new_object):
	object = new_object
	if object and $Anchor:
		for body in $Anchor.get_children():
			body.queue_free()
		var instance = object.instance()
		instance.name = "Object"
		$Anchor.add_child(instance, true)

func get_object():
	return object

func set_size(new_size):
	size = new_size
	if $CollisionShape:
		$CollisionShape.scale = size

func get_size():
	return size

func reset():
	resetable.reset()

func destroy():
	if is_destroyed: return
	collider.disabled = true
	is_destroyed = true
	audio.play()
	animator.play("Action")
	return true

func _on_Audio_finished():
	hide()
