extends Node

onready var host = get_parent()

func reset():
	host.is_destroyed = false
	host.collider.disabled = false
	host.animator.seek(.0, true)
	host.show()
