extends Control

onready var locked : bool = true

func _ready():
	yield(get_tree().create_timer(1.0), "timeout")
	locked = false

func _input(event):
	if locked: return
	if event is InputEventScreenTouch \
			or event is InputEventJoypadButton \
			or event is InputEventKey:
		get_tree().change_scene(GlobalConfig.SCENES_PATH + "PreMenu.tscn")
