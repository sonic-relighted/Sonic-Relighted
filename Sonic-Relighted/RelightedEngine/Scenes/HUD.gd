extends CanvasLayer

onready var rings          : Node2D = $SpritesContainer/Rings
onready var timer          : Node2D = $SpritesContainer/Timer
onready var lifes          : Node2D = $SpritesContainer/Lifes
onready var machines       : Node2D = $SpritesContainer/Machines
onready var machines_total : Node2D = $SpritesContainer/MachinesTotal
onready var machines_img   : Sprite = $SpritesContainer/MachinesImg
onready var current_time   : float  = .0
onready var controller     : Node2D = $Controller
onready var container      : Node2D = $SpritesContainer

func _ready():
	update_rings()
	set_process(false)

func _process(delta):
	_timer(delta)

#func _pausa_menu():
#	if controller.is_button_menu_just_pressed() or Input.is_action_just_pressed("menu"):
#		_show_pause_menu()

func _timer(delta):
	var prev_secs : int = ProgressSingletone.ingame_seconds
	current_time += delta
	var current_secs : int = int(current_time)
	if prev_secs != current_secs:
		ProgressSingletone.ingame_seconds = current_secs
		update_time()

func countdown(value : int):
	$SpritesContainer/Countdown/Tween.interpolate_property(
		$SpritesContainer/Countdown,
		"modulate:a", 1.0, .0, 2.5, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$SpritesContainer/Countdown.set_text(str(value))
	$SpritesContainer/Countdown/Tween.start()

func reset_countdown():
	$SpritesContainer/Countdown.set_text("")

func update_all():
	update_rings()
	update_time()
	update_lifes()
	update_devices()

func update_rings():
	rings.text = str(ProgressSingletone.ingame_rings)

func update_time():
	timer.text = Formatter.time_seconds_to_timestamp(ProgressSingletone.ingame_seconds)

func update_lifes():
	lifes.text = str(ProgressSingletone.game_slot_data.lifes)

func update_devices():
	if ProgressSingletone.ingame_devices_total == 0:
		machines.hide()
		machines_total.hide()
		machines_img.hide()
	else:
		machines.show()
		machines_total.show()
		machines_img.show()
		machines.text = str(ProgressSingletone.ingame_devices)
		machines_total.text = "/" + str(ProgressSingletone.ingame_devices_total)

func show_pause_menu():
	add_child(LevelsCutscenesManager.build_pause_screen())
