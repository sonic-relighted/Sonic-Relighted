extends CanvasLayer

export(float) var time : float = .5

onready var current = null


func _on_Player_physics_area_enter(type : int):
	if type == PhysicsOverwriteArea.Type.WATER:
		underwater(true)

func _on_Player_physics_area_exit(type : int):
	if type == PhysicsOverwriteArea.Type.WATER:
		underwater(false)



func end():
	if current:
		$Tween.interpolate_property(current, "modulate:a", current.modulate.a, .0, time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		$Tween.start()
		yield($Tween, "tween_completed")
		current.hide()
	current = null

func underwater(status : bool):
	if status:
		_start($Underwater)
	elif current == $Underwater:
		end()

func _start(node : Node):
	var old = current
	current = node
	current.show()
	$Tween.interpolate_property(current, "modulate:a", current.modulate.a, 1.0, time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	if old: $Tween.interpolate_property(old, "modulate:a", old.modulate.a, .0, time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	$Tween.start()
	if old:
		yield($Tween, "tween_all_completed")
		old.hide()
