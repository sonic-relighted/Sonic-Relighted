extends Spatial

onready var previous_score : int = ProgressSingletone.game_slot_data.score
onready var load_manager = $LoadLevelManager
onready var add_manager = $AddLevelManager

onready var hud = $HUD
onready var fxshader = $FxShader
onready var virtual_controller = $HUD/Controller
onready var ost_player = $OstPlayer
onready var player_camera = $PlayerCamera

var title_screen : TitleScreen

func _ready():
	SettingsData.load_data()
	ost_player.stream = load(GlobalConfig.OST_LEVELS_PATH + load_manager.level_id + ".ogg")
	ost_player.play_with_fade_in()
	_title_screen()

func _title_screen():
	title_screen = LevelsCutscenesManager.build_title_screen()
	add_child(title_screen)
	title_screen.set_text(tr("LEVEL.Name." + load_manager.level_id))
	title_screen.set_level_image(load_manager.level_id)
	yield(title_screen, "finish")
	# para la inicializacion de materiales y particulas
	yield(get_tree().create_timer(.25), "timeout")
	hud.set_process(true)
	Instances.player.input_manager.locked = false

func _on_player_death():
	if ProgressSingletone.game_slot_data.lifes < 1:
		add_child(LevelsCutscenesManager.build_gameover_screen())
	else:
		add_child(LevelsCutscenesManager.build_death_screen())

func _on_player_complete_level():
	ProgressSingletone.active_shield = Instances.player.shield_type if Instances.player.is_shielded else -1
	var score_in_this_level = ProgressSingletone.game_slot_data.score - previous_score
	if score_in_this_level > ProgressSingletone.SCORE_TO_LIFE:
		ProgressSingletone.game_slot_data.lifes += 1
	var level = ProgressSingletone.playing_level_id - 1
	ProgressSingletone.game_slot_data.completed_levels[level] = true
	ProgressSingletone.game_slot_data.time[level] = min(
			ProgressSingletone.game_slot_data.time[level],
			ProgressSingletone.ingame_seconds)
	ProgressSingletone.game_slot_data.stars[level] = ProgressSingletone.ingame_stars

	var scr = LevelsCutscenesManager.build_levelcompleted_screen()
	scr.stage_music = ost_player
	add_child(scr)

func on_player_hi_speed(active : bool):
	ost_player.set_hi_speed(active)

func on_level_loaded(level):
	load_manager.queue_free()
	add_manager.add_level(level)

func on_level_added(level):
	title_screen.level_loaded()
	pause_mode = Node.PAUSE_MODE_INHERIT
