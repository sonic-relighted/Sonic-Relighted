extends Spatial

onready var previous_score : int = ProgressSingletone.game_slot_data.score
onready var load_manager = $LoadBonuslManager
onready var add_manager = $AddBonusManager

onready var hud = $BonusHUD
onready var fxshader = $FxShader
onready var virtual_controller = $BonusHUD/Controller
onready var ost_player = $OstPlayer
onready var player_camera = $PlayerCamera

var title_screen : TitleScreen

func _ready():
	SettingsData.load_data()
	ost_player.play_with_fade_in()
	_title_screen()

func _title_screen():
	title_screen = LevelsCutscenesManager.build_title_screen()
	add_child(title_screen)
	title_screen.set_text(tr("BONUS.Name." + load_manager.bonus_id))
	title_screen.set_bonus_image(load_manager.bonus_id)
	yield(title_screen, "finish")
	hud.set_process(true)
	Instances.player.input_manager.locked = false

func on_bonus_loaded(level):
	load_manager.queue_free()
	add_manager.add_level(level)

func on_bonus_added(level):
	title_screen.level_loaded()
	pause_mode = Node.PAUSE_MODE_INHERIT

func _on_BonusHUD_timeout():
	_end(BonusEnd.Type.TIMEOUT)

func _on_player_death():
	# Para compensar, el player ya se encarga de bajarse
	# una vida, ya que es el comportamiento habitual, pero
	# aqui se la volvemos a dar...
	ProgressSingletone.game_slot_data.lifes += 1
	_end(BonusEnd.Type.DEATH)

func _on_player_exit():
	Instances.player.input_manager.locked = true
	_end(BonusEnd.Type.VOLUNTARILY)

func _end(type : int):
	if GlobalConfig.DEMO:
		_demo()
		return
	var scr = LevelsCutscenesManager.build_cutscene_end()
	scr.stage_music = $OstPlayer
	scr.type = type
	add_child(scr)

func _demo():
	get_tree().change_scene(GlobalConfig.SCENES_PATH + "DemoScreen.tscn")
