extends Node
# Empty Stars
const _E_S : Array =  [false,false,false,false,false]
const default_slot_data : Dictionary = {
	"is_empty":         true,
	"lifes":            ProgressSingletone.INITIAL_LIFES,
	"score":            0,
	"shield":           -1,
	#                    1-A   # 1-B  1-C    2-A    2-B    2-C    3-A    3-B    3-C    4-A    4-B    4-C    5-A    5-B    5-C    6-A    6-B    6-C     7-A    7-B    7-C
	"completed_levels": [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
	"time":             [-1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1],
	"stars":            [_E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S,  _E_S],
	"world_rotation":   [.0,.0,.0],
	"world_pointer_angle": 0
}

var slot_data = []

func _ready():
	var i = 0
	while i < 3:
		var data = load_data(i)
		data.slot_id = i
		slot_data.append(data)
		i += 1

###############################################

func load_data(slot_id : int) -> Dictionary:
	var file_name = "user://slot_" + str(slot_id) + ".json"
	var file = File.new()
	if not file.file_exists(file_name):
		print("No file saved!")
		return parse_json(to_json(default_slot_data))
	if file.open(file_name, File.READ) != 0:
		print("Error opening file")
		return parse_json(to_json(default_slot_data))
	var readed = parse_json(file.get_line())
	if readed == null:
		return parse_json(to_json(default_slot_data))
	file.close()
	if !readed.has("shield"): readed.shield = -1
	if !readed.has('score'): readed.score = 0
	if !readed.has('world_rotation'): readed.world_rotation = [.0,.0,.0]
	if !readed.has('world_pointer_angle'): readed.world_pointer_angle = 0 
	return readed

func save_data(slot_id : int, data : Dictionary):
	if GlobalConfig.DEMO: return
	var file_name = "user://slot_" + str(slot_id) + ".json"
	var file = File.new()
	if file.open(file_name, File.WRITE) != 0:
		print("Error opening file")
		return
	file.store_line(to_json(data))
	file.close()
	slot_data[slot_id] = data
