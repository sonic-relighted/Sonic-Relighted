extends Node

export(bool) var show_start_button : bool = true

signal on_play
signal on_open_settings
signal on_open_credits
signal on_quit

func _on_MenuPlayButton_clicked():
	$TransactionManager.go_to("on_play")

func _on_MenuOptionsButton_clicked():
	$TransactionManager.go_to("on_open_settings")

func _on_MenuCreditsButton_clicked():
	$TransactionManager.go_to("on_open_credits")

func _on_MenuQuitButton_clicked():
	var confirm : Confirm = load(GlobalConfig.UIOBJECTS_PATH + "Menu/Confirm.res").instance()
	confirm.connect("accept", $TransactionManager, "go_to", ["on_quit"])
	confirm.connect("cancel", self, "_cancel_exit")
	add_child(confirm)
	confirm.set_message(Confirm.Message.EXIT)
	$FocusManager.enabled = false
	_active_buttons([
			$MenuPlayButton,
			$MenuOptionsButton,
			$MenuCreditsButton,
			$MenuQuitButton
			], false)

func _cancel_exit():
	$FocusManager.enabled = true
	_active_buttons([
			$MenuPlayButton,
			$MenuOptionsButton,
			$MenuCreditsButton,
			$MenuQuitButton
			], true)
	$FocusManager.current_control = $MenuQuitButton
	$FocusManager.current_control.focused = true

func _on_AnimationPlayer_animation_finished(anim_name : String):
	if anim_name == "show_menu": $PressStartButton.queue_free()

func _on_PressStartButton_clicked():
	yield(get_tree(), "idle_frame")
	$PressStartButton/StartAnimation.start()
	$PressStartButton.enabled = false
	_active_buttons([
			$MenuPlayButton,
			$MenuOptionsButton,
			$MenuCreditsButton,
			$MenuQuitButton
			], true)
	$FocusManager.current_control.focused = true
	$FocusManager.current_control.enabled = true

func _active_buttons(buttons : Array, enabled : bool):
	for button in buttons:
		button.enabled = enabled
		button.focused = false

func _apply_language(lang : String):
	for spr in get_tree().get_nodes_in_group("MultiLang"):
		var path = GlobalConfig.SCENES_PATH + "Menu/MenuPrincipal/Principal-" + lang + ".png"
		spr.texture = load(path)

func _ready():
	if show_start_button:
		$FocusManager.current_control.focused = false
		$FocusManager.current_control.enabled = false
	else:
		$PressStartButton.hide()
		_on_PressStartButton_clicked()
	_apply_language(SettingsData.data.language)
