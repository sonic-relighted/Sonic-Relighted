extends Node2D

signal on_close

func _on_BackMenuButton_clicked():
	if $BackMenuButton.enabled:
		$TransactionManager.go_to("on_close")
