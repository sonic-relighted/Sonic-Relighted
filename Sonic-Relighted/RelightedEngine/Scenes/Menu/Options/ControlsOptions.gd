extends Node2D

func _on_SeeInputs_clicked():
	$Controls.visible = !$Controls.visible

func _on_FocusManager_disabled():
	$Controls.visible = false
