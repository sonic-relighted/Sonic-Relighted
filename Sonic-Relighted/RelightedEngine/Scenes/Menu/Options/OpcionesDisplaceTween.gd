extends Tween

signal start
signal end

export(Array, NodePath) var sections : Array
export(Array, int)      var positions : Array
export(int)             var displace : int = 64

var sections_instances : Array

func displace(id : int):
	emit_signal("start")
	var i = 0
	for sec in sections_instances:
		var position = positions[i]
		if i > id: position += displace
		interpolate_property(
			sec, "position:y",
			sec.position.y, position,
			.5, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		i += 1
	start()
	yield(self, "tween_all_completed")
	emit_signal("end")

func _ready():
	sections_instances = []
	for sec in sections:
		sections_instances.append(get_node(sec))
