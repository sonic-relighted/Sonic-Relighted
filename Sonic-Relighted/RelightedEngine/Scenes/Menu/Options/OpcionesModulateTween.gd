extends Tween

signal start
signal end

export(Array, NodePath) var sections : Array

var sections_instances : Array

func show(id : int):
	emit_signal("start")
	var i = 0
	for sec in sections_instances:
		if i != id: _hide(sec)
		i += 1
	start()
	yield(self, "tween_all_completed")

	i = 0
	for sec in sections_instances:
		if i == id: _show(sec)
		i += 1
	start()
	yield(self, "tween_all_completed")
	emit_signal("end")

func _show(sec : Node2D):
	interpolate_property(
		sec, "modulate:a",
		sec.modulate.a, 1.0,
		.5, Tween.TRANS_SINE, Tween.EASE_IN_OUT)

func _hide(sec : Node2D):
	interpolate_property(
		sec, "modulate:a",
		sec.modulate.a, .0,
		.25, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)

func _ready():
	sections_instances = []
	for sec in sections:
		var inst = get_node(sec)
		sections_instances.append(inst)
		inst.modulate.a = .0
