extends Node2D

onready var host = get_parent()

var language

func load_values():
	language = SettingsData.data.language
#	if language == null or language == "": language = "en"
	_apply_language(language)

func apply():
	SettingsData.data.language = language
	SettingsData.set_language(language)

func _process(delta : float):
	_control()
	_print_english()
	_print_spanish()

func _control():
	var current = $FocusManager.current_control
	if Input.is_action_just_pressed("left"):
		if current == $English: _on_English_clicked()
		if current == $Spanish: _on_Spanish_clicked()
	if Input.is_action_just_pressed("right"):
		if current == $English: _on_English_clicked()
		if current == $Spanish: _on_Spanish_clicked()

func _print_english():
	$EnglishValue.region_rect.position.y = 2 if language == "en" else 11

func _print_spanish():
	$SpanishValue.region_rect.position.y = 2 if language == "es" else 11

func _on_English_clicked():
	if modulate.a < .99: return
	language = "en"
	_apply_language(language)

func _on_Spanish_clicked():
	if modulate.a < .99: return
	language = "es"
	_apply_language(language)

func _apply_language(lang : String):
	for spr in get_tree().get_nodes_in_group("MultiLang"):
		var path = GlobalConfig.SCENES_PATH + "Menu/Options/Options-" + lang + ".png"
		spr.texture = load(path)
