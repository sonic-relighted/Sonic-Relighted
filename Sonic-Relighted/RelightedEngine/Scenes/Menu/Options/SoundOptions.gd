extends Node2D

onready var host = get_parent()

onready var sfx_values    : Array = GroupFinder.find_nodes_by_name(self, "SfxValue")
onready var voices_values : Array = GroupFinder.find_nodes_by_name(self, "VoicesValue")
onready var music_values  : Array = GroupFinder.find_nodes_by_name(self, "MusicValue")

var sfx_volume
var music_volume
var voices_volume

func load_values():
	sfx_volume = AudioServerManager.get_sfx_linear_volume() * 10.0
	music_volume = AudioServerManager.get_music_linear_volume() * 10.0
	voices_volume = AudioServerManager.get_voices_linear_volume() * 10.0

func apply():
	SettingsData.data.sfx_volume = _convert_to_bus_volume(sfx_volume / 10.0)
	SettingsData.data.music_volume = _convert_to_bus_volume(music_volume / 10.0)
	SettingsData.data.voices_volume = _convert_to_bus_volume(voices_volume / 10.0)

func _convert_to_bus_volume(value : float):
	return -21 if value == 0 else linear2db(value)



func _on_Voices_clicked():
	if modulate.a < .99: return
	voices_volume += 1
	if voices_volume > 10: voices_volume = 0

func _on_Sfx_clicked():
	if modulate.a < .99: return
	sfx_volume += 1
	if sfx_volume > 10: sfx_volume = 0

func _on_Music_clicked():
	if modulate.a < .99: return
	music_volume += 1
	if music_volume > 10: music_volume = 0

func _process(delta : float):
	_control()
	_print_sfx()
	_print_music()
	_print_voices()

func _control():
	var current = $FocusManager.current_control
	if Input.is_action_just_pressed("left"):
		if current == $Sfx: sfx_volume = max(.0, sfx_volume - 1)
		elif current == $Voices: voices_volume = max(.0, voices_volume - 1)
		else: music_volume = max(.0, music_volume - 1)
		_apply_volumes()
	if Input.is_action_just_pressed("right"):
		if current == $Sfx: sfx_volume = min(10.0, sfx_volume + 1)
		elif current == $Voices: voices_volume = min(10.0, voices_volume + 1)
		else: music_volume = min(10.0, music_volume + 1)
		_apply_volumes()

func _print_sfx():
	var i = 0
	for item in sfx_values:
		item.region_rect.position.y = 2 if i < sfx_volume else 11
		i += 1

func _print_music():
	var i = 0
	for item in music_values:
		item.region_rect.position.y = 2 if i < music_volume else 11
		i += 1

func _print_voices():
	var i = 0
	for item in voices_values:
		item.region_rect.position.y = 2 if i < voices_volume else 11
		i += 1

func _apply_volumes():
	AudioServerManager.set_music_volume(_convert_to_bus_volume(float(music_volume) / 10.0))
	AudioServerManager.set_sfx_volume(_convert_to_bus_volume(float(sfx_volume) / 10.0))
	AudioServerManager.set_voices_volume(_convert_to_bus_volume(float(voices_volume) / 10.0))
