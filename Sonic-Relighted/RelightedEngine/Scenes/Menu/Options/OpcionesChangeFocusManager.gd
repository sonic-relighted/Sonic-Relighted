extends Node

export(Array, NodePath) var managers : Array

var manager_instances : Array

func change(id : int):
	var i = 0
	for m in manager_instances:
		m.enabled = i == id
		m.current_control.focused = m.enabled
		i += 1

func _ready():
	manager_instances = []
	for m in managers:
		manager_instances.append(get_node(m))
