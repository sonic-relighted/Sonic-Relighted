extends Node2D

export(Color) var resolution_color1_on1  : Color = Color("#b63c35")
export(Color) var resolution_color1_on2  : Color = Color("#ffbc4e")
export(Color) var resolution_color1_on3  : Color = Color("#4097ea")
export(Color) var resolution_color2_on   : Color = Color.yellow
export(Color) var resolution_color1_off  : Color = Color.black
export(Color) var resolution_color2_off  : Color = Color.gray

onready var host = get_parent()

var resolution_before
var fullscreen_before

var resolution     : int
var shadows        : bool
var fullscreen     : bool
var vsync          : bool
var transparencies : bool

func reset_required() -> bool:
	return resolution_before != SettingsData.data.resolution \
			or fullscreen_before != SettingsData.data.fullscreen

func load_values():
	fullscreen = OS.window_fullscreen
	vsync = OS.is_vsync_enabled()
	resolution = SettingsData.data.resolution
	shadows = SettingsData.data.shadows
	transparencies = SettingsData.data.transparencies
	resolution_before = resolution
	fullscreen_before = fullscreen 

func apply():
	SettingsData.data.resolution = resolution
	SettingsData.data.shadows = shadows
	SettingsData.data.fullscreen = fullscreen
	SettingsData.data.vsync = vsync
	SettingsData.data.transparencies = transparencies

func _on_Resolution_clicked():
	if modulate.a < .99: return
	resolution += 1
	if resolution > 2: resolution = 0

func _on_Shadows_clicked():
	if modulate.a < .99: return
	shadows = !shadows

func _on_Fullscreen_clicked():
	if modulate.a < .99: return
	fullscreen = !fullscreen

func _on_VSync_clicked():
	if modulate.a < .99: return
	vsync = !vsync

func _on_Transparencies_clicked():
	if modulate.a < .99: return
	transparencies = !transparencies

func _process(delta : float):
	_control()
	_print_resolution()
	_print_shadows()
	_print_fullscreen()
	_print_vsync()
	_print_transparencies()

func _control():
	var current = $FocusManager.current_control
	if Input.is_action_just_pressed("left"):
		if current == $Resolution: resolution = max(.0, resolution - 1)
		if current == $Shadows: _on_Shadows_clicked()
		if current == $Fullscreen: _on_Fullscreen_clicked()
		if current == $VSync: _on_VSync_clicked()
		if current == $Transparencies: _on_Transparencies_clicked()
	if Input.is_action_just_pressed("right"):
		if current == $Resolution: resolution = min(2.0, resolution + 1)
		if current == $Shadows: _on_Shadows_clicked()
		if current == $Fullscreen: _on_Fullscreen_clicked()
		if current == $VSync: _on_VSync_clicked()
		if current == $Transparencies: _on_Transparencies_clicked()

func _print_resolution():
	$ResolutionValue1.material.set_shader_param("replace_1", resolution_color1_on1 if resolution == 0 else resolution_color1_off)
	$ResolutionValue1.material.set_shader_param("replace_2", resolution_color2_on  if resolution == 0 else resolution_color2_off)
	$ResolutionValue2.material.set_shader_param("replace_1", resolution_color1_on2 if resolution == 1 else resolution_color1_off)
	$ResolutionValue2.material.set_shader_param("replace_2", resolution_color2_on  if resolution == 1 else resolution_color2_off)
	$ResolutionValue3.material.set_shader_param("replace_1", resolution_color1_on3 if resolution == 2 else resolution_color1_off)
	$ResolutionValue3.material.set_shader_param("replace_2", resolution_color2_on  if resolution == 2 else resolution_color2_off)

func _print_shadows():
	$ShadowsValue.region_rect.position.y = 2 if shadows else 11

func _print_fullscreen():
	$FullScreenValue.region_rect.position.y = 2 if fullscreen else 11

func _print_vsync():
	$VSyncValue.region_rect.position.y = 2 if vsync else 11

func _print_transparencies():
	$TransparenciesValue.region_rect.position.y = 2 if transparencies else 11
