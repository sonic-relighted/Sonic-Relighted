extends Node

signal no_has_next
signal disabled

export(bool)           var enabled       : bool = true setget set_enabled, get_enabled
export(NodePath)       var default       : NodePath

onready var current_control : Node2D = get_node(default)

func set_enabled(new_enabled : bool):
	enabled = new_enabled

func get_enabled() -> bool:
	emit_signal("disabled")
	return enabled

func _input(event):
	if current_control == null or not is_instance_valid(current_control): return
	if enabled and not event.is_pressed():
		if event is InputEventKey \
		or event is InputEventJoypadButton:
			current_control.focused = false
			if not current_control.near_up and event.is_action_released("ui_up"):
				emit_signal("no_has_next", "up")
			elif not current_control.near_bottom and event.is_action_released("ui_down"):
				emit_signal("no_has_next", "down")
			elif not current_control.near_left and event.is_action_released("ui_left"):
				emit_signal("no_has_next", "left")
			elif not current_control.near_right and event.is_action_released("ui_right"):
				emit_signal("no_has_next", "left")
			elif current_control.near_up and event.is_action_released("ui_up"):
				current_control =_get_next_up(current_control)
			elif current_control.near_bottom and event.is_action_released("ui_down"):
				current_control = _get_next_bottom(current_control)
			elif current_control.near_left and event.is_action_released("ui_left"):
				current_control = _get_next_left(current_control)
			elif current_control.near_right and event.is_action_released("ui_right"):
				current_control = _get_next_right(current_control)
			current_control.focused = true

func _ready():
	current_control.focused = true

func _get_next_up(obj : Node2D) -> Node:
	if obj.near_up == null: return current_control
	var r = obj.get_node(obj.near_up)
	if r == null: return current_control
	if r.is_visible() == false or r.enabled == false:
		r = _get_next_up(r)
	return r

func _get_next_bottom(obj : Node2D) -> Node:
	if obj.near_bottom == null: return current_control
	var r = obj.get_node(obj.near_bottom)
	if r == null: return current_control
	if r.is_visible() == false or r.enabled == false:
		r = _get_next_bottom(r)
	return r

func _get_next_left(obj : Node2D) -> Node:
	if obj.near_left == null: return current_control
	var r = obj.get_node(obj.near_left)
	if r == null: return current_control
	if r.is_visible() == false or r.enabled == false:
		r = _get_next_left(r)
	return r

func _get_next_right(obj : Node2D) -> Node:
	if obj.near_right == null: return current_control
	var r = obj.get_node(obj.near_right)
	if r == null: return current_control
	if r.is_visible() == false or r.enabled == false:
		r = _get_next_right(r)
	return r
