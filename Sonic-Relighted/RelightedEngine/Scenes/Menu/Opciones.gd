extends Node

signal on_accept
signal on_cancel
signal on_reset_required
signal unlock

onready var principal_focus_manager = $PrincipalFocusManager
onready var back_button             = $BackMenuButton
onready var save_button             = $SaveMenuButton

onready var locked : bool = false

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		if principal_focus_manager.enabled:
			_on_BackMenuButton_clicked()
		else:
			_cancel_suboption()

func _set_values():
	$GraphicsOptions.load_values()
	$LanguageOptions.load_values()
	$SoundOptions.load_values()

func _cancel_suboption():
	$DisplaceTween.displace(10000)
	$ModulateTween.show(10000)
	$ChangeFocusManager.change(10000)
	yield(self, "unlock")
	$ChangeFocusManager.change(0)

func _on_GraphicsFocusManager_no_has_next(direction : String):
	if locked: return
	if direction == "left" or direction == "right": return
	$DisplaceTween.displace(10000)
	$ModulateTween.show(10000)
	$GraphicsOptions/FocusManager.enabled = false
	principal_focus_manager.current_control.focused = false
	if direction == "down": principal_focus_manager.current_control = $Language
	yield(self, "unlock")
	principal_focus_manager.enabled = true
	principal_focus_manager.current_control.focused = true

func _on_LanguageFocusManager_no_has_next(direction : String):
	if locked: return
	if direction == "left" or direction == "right": return
	$DisplaceTween.displace(10000)
	$ModulateTween.show(10000)
	$LanguageOptions/FocusManager.enabled = false
	principal_focus_manager.current_control.focused = false
	if direction == "down": principal_focus_manager.current_control = $Sound
	yield(self, "unlock")
	principal_focus_manager.enabled = true
	principal_focus_manager.current_control.focused = true

func _on_SoundFocusManager_no_has_next(direction : String):
	if locked: return
	if direction == "left" or direction == "right": return
	$DisplaceTween.displace(10000)
	$ModulateTween.show(10000)
	$SoundOptions/FocusManager.enabled = false
	principal_focus_manager.current_control.focused = false
	if direction == "down": principal_focus_manager.current_control = $Controls
	yield(self, "unlock")
	principal_focus_manager.enabled = true
	principal_focus_manager.current_control.focused = true

func _on_ControlsFocusManager_no_has_next(direction : String):
	if locked: return
	if direction == "left" or direction == "right": return
	$DisplaceTween.displace(10000)
	$ModulateTween.show(10000)
	$ControlsOptions/FocusManager.enabled = false
	principal_focus_manager.current_control.focused = false
	if direction == "down": principal_focus_manager.current_control = $SaveMenuButton
	yield(self, "unlock")
	principal_focus_manager.enabled = true
	principal_focus_manager.current_control.focused = true

# SAVE / CANCEL

func _on_BackMenuButton_clicked():
	if $BackMenuButton.enabled:
		$TransactionManager.go_to("on_cancel")

func _on_SaveMenuButton_clicked():
	$GraphicsOptions.apply()
	$LanguageOptions.apply()
	$SoundOptions.apply()
	if $GraphicsOptions.reset_required():
		$TransactionManager.go_to("on_reset_required")
	else:
		$TransactionManager.go_to("on_accept")

func _ready():
	_set_values()

func _on_DisplaceTween_start():
	locked = true

func _on_DisplaceTween_end():
	locked = $DisplaceTween.is_active() or $ModulateTween.is_active()
	if not locked: emit_signal("unlock")

func _on_ModulateTween_start():
	locked = true

func _on_ModulateTween_end():
	locked = $DisplaceTween.is_active() or $ModulateTween.is_active()
	if not locked: emit_signal("unlock")
