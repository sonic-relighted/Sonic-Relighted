extends Node

signal on_reset

func _ready():
	var confirm : Confirm = load(GlobalConfig.UIOBJECTS_PATH + "Menu/Confirm.res").instance()
	confirm.connect("accept", self, "emit_signal", ["on_reset"])
	add_child(confirm)
	confirm.set_message(Confirm.Message.APPLY_AND_RESTART)
	confirm.only_accept_button()
