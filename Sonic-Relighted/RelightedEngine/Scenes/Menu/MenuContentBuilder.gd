extends Node

onready var already_pressed_start_button = false

func build_menu_principal(host : Node):
	var node = LevelsCutscenesManager.build_menu_principal()
	node.show_start_button = !already_pressed_start_button
	node.connect("on_play", host, "_on_open_play")
	node.connect("on_open_settings", host, "_on_open_settings")
	node.connect("on_open_credits", host, "_on_open_credits")
	node.connect("on_quit", host, "_on_quit")
	_put_content(host, node)
	already_pressed_start_button = true

func build_play(host : Node):
	var node = LevelsCutscenesManager.build_menu_play()
	node.connect("on_open", host, "_on_play_open")
	node.connect("on_back", host, "_on_play_back")
	_put_content(host, node)

func build_settings(host : Node):
	var node = LevelsCutscenesManager.build_menu_opciones()
	node.connect("on_accept", host, "_on_settings_accept")
	node.connect("on_cancel", host, "_on_settings_cancel")
	node.connect("on_reset_required", host, "_on_settings_reset_required")
	_put_content(host, node)

func build_credits(host : Node):
	var node = LevelsCutscenesManager.build_menu_creditos()
	node.connect("on_close", host, "_on_credits_close")
	_put_content(host, node)

func build_menu_reset_required(host : Node):
	var node = LevelsCutscenesManager.build_alert_reset_required()
	node.connect("on_reset", host, "_on_reset_required_accept")
	_put_content(host, node)

func _put_content(host : Node, node : Node):
	var old_node_id = host.get_child_count() - 1
	var old_node = host.get_child(old_node_id)
	old_node.queue_free()
	host.add_child(node)
	for child in GroupFinder.find_nodes_by_type(node, Button):
		child.connect("focus_entered", get_parent(), "_on_change_focus", [child])
		child.connect("pressed", get_parent(), "_on_pressed", [child])
		
