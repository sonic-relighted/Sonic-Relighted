extends Node

signal on_open
signal on_back

var confirm_data : int

func _ready():
	if GlobalConfig.DEMO:
		_demo()
		return
	_setup_slot($Slot01, $SlotDataManager.slot_data[0])
	_setup_slot($Slot02, $SlotDataManager.slot_data[1])
	_setup_slot($Slot03, $SlotDataManager.slot_data[2])
	$Sprite.texture = load(GlobalConfig.UIOBJECTS_PATH + "Menu/" + tr("LANGUAGE.id") + "/select_file.png")

func _demo():
	confirm_data = 0
	var shield = $SlotDataManager.slot_data[0].shield
	_on_confirm_accept()
	$SlotDataManager.slot_data[0].shield = shield
	$SlotDataManager.slot_data[0].completed_levels[0] = true
	$SlotDataManager.slot_data[0].completed_levels[1] = true
	$SlotDataManager.slot_data[0].lifes = ProgressSingletone.INITIAL_LIFES
	_on_PlayMenuButton_clicked(0)
	set_process(false)

func _physics_process(delta : float):
	if Input.is_action_just_pressed("ui_cancel"):
		_on_BackMenuButton_clicked()

func _setup_slot(slot : Node, slot_data : Dictionary):
	if slot_data.lifes < 0:
		slot.get_child(0).hide()
		slot.get_child(1).show()
		_set_focus_to(slot.get_child(1))
	elif slot_data.is_empty:
		slot.get_child(0).value = 'new_play_empty'
		slot.get_child(0).show()
		slot.get_child(0)._on_mouse_exited()
		slot.get_child(1).hide()
	else:
		slot.get_child(0).show()
		slot.get_child(1).show()

func _on_confirm_cancel():
	_close_confirm()

func _on_confirm_accept():
	$SlotDataManager.save_data(confirm_data, $SlotDataManager.default_slot_data)
	$SlotDataManager.slot_data[confirm_data].is_empty = true
	_setup_slot($Slot01, $SlotDataManager.slot_data[0])
	_setup_slot($Slot02, $SlotDataManager.slot_data[1])
	_setup_slot($Slot03, $SlotDataManager.slot_data[2])
	_close_confirm()
	match confirm_data:
		0: _set_focus_to($Slot01/PlayMenuButton1)
		1: _set_focus_to($Slot02/PlayMenuButton2)
		2: _set_focus_to($Slot03/PlayMenuButton3)

func _on_BackMenuButton_clicked():
	if $BackMenuButton.enabled:
		$TransactionManager.go_to("on_back")

func _on_PlayMenuButton_clicked(slot_id : int):
	var slot = $SlotDataManager.slot_data[slot_id]
	var current_cinematic
	slot.is_empty = false
	$SlotDataManager.save_data(slot_id, slot)
	ProgressSingletone.game_slot_id = slot_id
	ProgressSingletone.game_slot_data = slot
	$TransactionManager.go_to("on_open")

func _on_ClearMenuButton_clicked(slot_id : int):
	confirm_data = slot_id
	_open_confirm()

func _open_confirm():
	_disable_slot($Slot01)
	_disable_slot($Slot02)
	_disable_slot($Slot03)
	$BackMenuButton.enabled = false
	$PrincipalFocusManager.enabled = false
	var confirm : Confirm = load(GlobalConfig.UIOBJECTS_PATH + "Menu/Confirm.res").instance()
	confirm.connect("accept", self, "_on_confirm_accept")
	confirm.connect("cancel", self, "_on_confirm_cancel")
	add_child(confirm)
	confirm.set_message(Confirm.Message.DELETE)

func _close_confirm():
	_enable_slot($Slot01)
	_enable_slot($Slot02)
	_enable_slot($Slot03)
	$BackMenuButton.enabled = true
	$PrincipalFocusManager.enabled = true

func _enable_slot(slot : Node):
	for button in slot.get_children():
		if button is Area2D:
			button.enabled = true

func _disable_slot(slot : Node):
	for button in slot.get_children():
		if button is Area2D:
			button.enabled = false

func _set_focus_to(new_focus : Node2D):
	for b in GroupFinder.find_nodes_by_type(self, Area2D):
		b.focused = false
	new_focus.focused = true
	$PrincipalFocusManager.current_control = new_focus
