extends Node

export(float)    var time_scale     : float = 1.0

export(NodePath) var background     : NodePath
export(NodePath) var play_button    : NodePath
export(NodePath) var options_button : NodePath
export(NodePath) var credits_button : NodePath
export(NodePath) var quit_button    : NodePath

onready var bg      : Node2D = get_node(background)
onready var play    : Node2D = get_node(play_button)
onready var options : Node2D = get_node(options_button)
onready var credits : Node2D = get_node(credits_button)
onready var quit    : Node2D = get_node(quit_button)

onready var bg_end_scale   = Vector2.ONE
onready var bg_start_scale = bg_end_scale * 3
onready var bg_move = false

onready var play_end_position   = play.position
onready var play_start_position = play_end_position * Vector2(3, .0)
onready var play_move = false

onready var options_end_position   = options.position
onready var options_start_position = options_end_position * Vector2(3, .0)
onready var options_move = false

onready var credits_end_position   = credits.position
onready var credits_start_position = credits_end_position * Vector2(3, .0)
onready var credits_move = false

onready var quit_end_position   = quit.position
onready var quit_start_position = quit_end_position * Vector2(3, .0)
onready var quit_move = false

var time : float

func start():
	set_process(true)
	time = .0
	bg.scale = bg_start_scale
	bg_move = true
	play.position = play_start_position
	options.position = options_start_position
	credits.position = credits_start_position
	quit.position = quit_start_position
	bg.show()
	play.show()
	options.show()
	credits.show()
	quit.show()

func _ready():
	set_process(false)
	bg.hide()
	play.hide()
	options.hide()
	credits.hide()
	quit.hide()

func _process(delta : float):
	if get_parent().modulate.a > 0.0:
		get_parent().modulate.a -= delta * 4 * time_scale
	else:
		get_parent().modulate.a = .0
	
	if bg_move:
		bg.scale = lerp(
				bg.scale,
				bg_end_scale,
				delta * 4 * time_scale)
	if play_move:
		play.position = lerp(
				play.position,
				play_end_position,
				delta * 4 * time_scale)
	if options_move:
		options.position = lerp(
				options.position,
				options_end_position,
				delta * 4 * time_scale)
	if credits_move:
		credits.position = lerp(
				credits.position,
				credits_end_position,
				delta * 4 * time_scale)
	if quit_move:
		quit.position = lerp(
				quit.position,
				quit_end_position,
				delta * 4 * time_scale)

	if time >= .5: play_move = true
	if time >= .7: options_move = true
	if time >= .9: credits_move = true
	if time >= 1.1: quit_move = true
	if time >= 5.2:
		get_parent().queue_free()
	time += delta * time_scale
