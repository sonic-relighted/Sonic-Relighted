extends Control

onready var levels  = $Levels/VBoxContainer.get_children()
onready var bonuses = $Bonuses/VBoxContainer.get_children()

func _physics_process(delta : float):
	if Input.is_action_just_released("camL"):
		$Levels/VBoxContainer/Level1.grab_focus()
	elif Input.is_action_just_pressed("camR"):
		$Bonuses/VBoxContainer/Bonus1.grab_focus()

func _on_level_pressed(btn : Button):
	var level_id : int = btn.get_index() + 1
	LevelsCutscenesManager.open_level(level_id)
	
func _on_bonus_pressed(btn : Button):
	var bonus_id : int = btn.get_index() + 1
	LevelsCutscenesManager.open_bonus(bonus_id)

func _ready():
	for btn in levels:
		btn.text = tr("LEVEL.Name." + Formatter.filled_number(btn.get_index() + 1, 2))
		btn.connect("pressed", self, "_on_level_pressed", [btn])
	for btn in bonuses:
		btn.text = tr("BONUS.Name." + Formatter.filled_number(btn.get_index() + 1, 2))
		btn.connect("pressed", self, "_on_bonus_pressed", [btn])
	$Levels/VBoxContainer/Level1.grab_focus()

func _enter_tree():
	var slot_id = 0
	var slot = $SlotDataManager.load_data(slot_id)
	ProgressSingletone.game_slot_id = slot_id
	ProgressSingletone.game_slot_data = slot
	slot.lifes = ProgressSingletone.INITIAL_LIFES
	SettingsData.load_data()
