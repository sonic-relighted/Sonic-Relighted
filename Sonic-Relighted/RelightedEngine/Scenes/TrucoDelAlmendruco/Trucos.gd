extends Node

onready var trucos_keypressed : Array = []

enum Keys {
	UP, DOWN, LEFT, RIGHT, JUMP, ROLL, CANCEL
}

const TRUCO_LEVEL_SELECT = [ Keys.UP, Keys.UP, Keys.DOWN, Keys.DOWN, Keys.LEFT, Keys.RIGHT, Keys.JUMP ]

func _physics_process(delta):
	_manage_pressed()
	_limit()
	_check()

func _manage_pressed():
	if Input.is_action_just_released("up"):
		trucos_keypressed.append(Keys.UP)
	elif Input.is_action_just_released("down"):
		trucos_keypressed.append(Keys.DOWN)
	elif Input.is_action_just_released("left"):
		trucos_keypressed.append(Keys.LEFT)
	elif Input.is_action_just_released("right"):
		trucos_keypressed.append(Keys.RIGHT)
	elif Input.is_action_just_released("jump"):
		trucos_keypressed.append(Keys.JUMP)
	elif Input.is_action_just_released("roll"):
		trucos_keypressed.append(Keys.ROLL)
	elif Input.is_action_just_released("ui_cancel"):
		trucos_keypressed.append(Keys.CANCEL)

func _limit():
	if trucos_keypressed.size() > TRUCO_LEVEL_SELECT.size():
		trucos_keypressed.remove(0)

func _check():
	if _is_level_select():
		get_tree().change_scene(
				GlobalConfig.SCENES_PATH + "TrucoDelAlmendruco/TrucoDelAlmentruco.tscn")
		return

func _is_level_select() -> bool:
	if trucos_keypressed.size() != TRUCO_LEVEL_SELECT.size():
		return false
	var i = 0
	for k in TRUCO_LEVEL_SELECT:
		if k != trucos_keypressed[i]:
			return false
		i += 1
	return true
