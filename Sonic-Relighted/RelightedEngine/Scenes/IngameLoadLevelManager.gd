extends Node

onready var host = get_parent()

var loader       : ResourceInteractiveLoader
var level_id     : String

func _ready():
	level_id = str(ProgressSingletone.playing_level_id)
	while level_id.length() < 2:
		level_id = "0" + level_id
#	level_id = "_inca_"
	loader = ResourceLoader.load_interactive("res://Levels/" + level_id + "/Level.tscn")
	if loader == null:
		show_error("No existe el nivel " + level_id)
		return

func _process(delta):
	_level_loading(delta)

func _level_loading(delta : float):
	if loader == null:
		set_process(false)
		return
	var err = loader.poll()
	if err == ERR_FILE_EOF: # la carga termino
		var resource = loader.get_resource()
		loader = null
		_load_level_completed(resource)
	elif err == OK:
		_update_loading_progress()
	else:
		show_error("Error durante la carga del nivel")
		loader = null

func show_error(err : String):
	var label : Label = Label.new()
	label.text = "ERROR: " + err
	host.hud.add_child(label)
	print("ERROR: " + err)

func _update_loading_progress():
	var progress = float(loader.get_stage()) / loader.get_stage_count()
	host.title_screen.set_progress(progress)

func _load_level_completed(res):
	var level = res.instance()
	_instance_initializations(level)
	host.on_level_loaded(level)

func _instance_initializations(level):
	var init = load(
			GlobalConfig.INGAME_ELEMENTS_PATH
			+ "Fwk/Initializations/Initializations.res").instance()
	init.connect("initialization_end", host.title_screen, "_on_initializations_end")
	init.connect("initialization_step", host.title_screen, "_on_initialization_step")
	host.add_child(init)
