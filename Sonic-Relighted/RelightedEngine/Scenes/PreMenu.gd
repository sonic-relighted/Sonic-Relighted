extends Node2D

onready var animator : AnimationPlayer = $AnimationPlayer

func _on_VideoPlayer_finished():
	_open_menu()

func _input(event):
	if GlobalConfig.can_skip_logos:
		if event is InputEventScreenTouch or event is InputEventJoypadButton or event is InputEventKey:
			_open_menu()

func _open_menu():
	GlobalConfig.can_skip_logos = true
	get_tree().change_scene(GlobalConfig.SCENES_PATH + "menu.tscn")

func _enter_tree():
	AudioServerManager.underwater_effect(false)
	SettingsData.load_data()
	for logo in $Logos.get_children():
		logo.modulate.a = .0

func _ready():
	if GlobalConfig.LOGOS_SCREEN:
		yield(get_tree().create_timer(.5), "timeout")
		animator.play("Sega")
	else:
		_open_menu()

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Sega":
		animator.play("Relighted")
	elif anim_name == "Relighted":
		animator.play("Dkbd")
	elif anim_name == "Dkbd":
		_open_menu()
