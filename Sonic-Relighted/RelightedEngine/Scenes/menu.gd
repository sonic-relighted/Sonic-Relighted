extends Node

onready var canvas_layer : CanvasLayer = $CanvasLayer

var last_focused_button : Button

const MUSIC_FADE_SPEED : float = .5

func _enter_tree():
	SettingsData.load_data()

func _ready():
	if ProgressSingletone.return_to_menu_from_ingame:
		$MenuContentBuilder.build_menu_principal(self)
	else:
		$MenuContentBuilder.build_menu_principal(self)
	ProgressSingletone.return_to_menu_from_ingame = false

func _on_change_focus(button : Button):
	$FocusSfx.play()
	last_focused_button = button

func _on_pressed(button : Button):
	if last_focused_button != button:
		$FocusSfx.play()

#### Principal ####

func _on_open_play():
#	$ClickSfx.play()
	$MenuContentBuilder.build_play(self)
	$ProceduralMusic.change_to(2, MUSIC_FADE_SPEED)

func _on_open_settings():
#	$ClickSfx.play()
	$MenuContentBuilder.build_settings(self)
	$ProceduralMusic.change_to(1, MUSIC_FADE_SPEED)

func _on_open_credits():
#	$ClickSfx.play()
	$MenuContentBuilder.build_credits(self)
#	$ProceduralMusic.change_to(3, MUSIC_FADE_SPEED)

func _on_quit():
#	$ClickSfx.play()
	get_tree().quit()

#### Settings ####

func _on_settings_accept():
#	$ClickSfx.play()
	SettingsData.save_data()
	$MenuContentBuilder.build_menu_principal(self)
	$ProceduralMusic.change_to(0, MUSIC_FADE_SPEED)

func _on_settings_cancel():
#	$ClickSfx.play()
	SettingsData.load_data()
	$MenuContentBuilder.build_menu_principal(self)
	$ProceduralMusic.change_to(0, MUSIC_FADE_SPEED)

func _on_settings_reset_required():
	SettingsData.save_data()
	$MenuContentBuilder.build_menu_reset_required(self)

#### Reset required ####
func _on_reset_required_accept():
	get_tree().quit()

#### Credits ####
func _on_credits_close():
	$MenuContentBuilder.build_menu_principal(self)
#	$ProceduralMusic.change_to(0, MUSIC_FADE_SPEED)

#### Play ####

func _on_play_open():
	if GlobalConfig.DEMO:
		_demo()
		return
	
	if ProgressSingletone.game_slot_data.completed_levels[0] == false:
		ProgressSingletone.current_cinematic = 0
		get_tree().change_scene(GlobalConfig.SCENES_PATH + "Cutscene.tscn")
	elif ProgressSingletone.game_slot_data.completed_levels[1] == false:
		ProgressSingletone.current_cinematic = 1
		get_tree().change_scene(GlobalConfig.SCENES_PATH + "Cutscene.tscn")
	else:
		get_tree().change_scene(GlobalConfig.SCENES_PATH + "ZoneSelect.tscn")

func _demo():
	ProgressSingletone.current_cinematic = 3
	get_tree().change_scene(GlobalConfig.SCENES_PATH + "Cutscene.tscn")

func _on_play_back():
#	$ClickSfx.play()
	$MenuContentBuilder.build_menu_principal(self)
	$ProceduralMusic.change_to(0, MUSIC_FADE_SPEED)
