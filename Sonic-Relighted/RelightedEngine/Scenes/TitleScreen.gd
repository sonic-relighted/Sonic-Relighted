extends CanvasLayer

signal finish

class_name TitleScreen

onready var container  = $Container
onready var title      = $Container/Title
onready var background = $Container/Background
onready var loading    = $Container/Loading
onready var progress   = $Container/ProgressBar
onready var press_key  = $Container/PressKey

onready var is_level_loaded : bool = false
onready var is_player_ready : bool = false

onready var steps_to_ready : int = 2

const LEVEL_IMAGES_PATH = "res://Levels/{id}/TitleScreen.png"
const BONUS_IMAGES_PATH = "res://Levels/BonusStages/{id}/TitleScreen.png"

func set_text(text):
	title.set_text(text)

func set_level_image(level_id : String):
	background.texture = load(LEVEL_IMAGES_PATH.replace("{id}", level_id))
	
func set_bonus_image(bonusl_id : String):
	background.texture = load(BONUS_IMAGES_PATH.replace("{id}", bonusl_id))

func _fadein():
	$EnterTween.interpolate_property(
			container,
			"modulate",
			Color.black, Color.white, .5, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	$EnterTween.start()

func _fadeout():
	$ExitTween.interpolate_property(
			container,
			"modulate:a",
			1.0, .0, 1.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	$ExitTween.start()

func level_loaded():
	_step_to_ready()

func _on_initialization_step(step : String):
	$Container/Loading/AnimationPlayer.play("initializating")
	$Container/PressKey.set_text("initializing " + step + "...  ")
	$Container/PressKey.show()

func _on_initializations_end():
	_step_to_ready()

func _step_to_ready():
	steps_to_ready -= 1
	if steps_to_ready < 1:
		$Container/PressKey.set_text("press any key")
		$Container/Loading/AnimationPlayer.stop()
		is_level_loaded = true
		_hide_progress()
		$WaitTimer.start()
		get_tree().paused = false
		
func _hide_progress():
	if progress:
		progress.queue_free()
		progress = null
	if loading:
		loading.queue_free()
		loading = null
	press_key.show()

func _process(delta):
	if is_level_loaded and is_player_ready and $WaitTimer.is_stopped():
		_done()

func _done():
	set_process(false)
	if Instances.player.state_machine.current_state == Instances.player.state_machine.preinitialized_state:
		Instances.player.state_machine.current_state = Instances.player.state_machine.grounded_state
	_fadeout()

func set_progress(value : float):
	_animate_loading()
	if progress: progress.value = value

func _animate_loading():
	if loading.frame >= loading.hframes - 1:
		loading.frame = 0
	else:
		loading.frame += 1

func _input(event):
	if event is InputEventScreenTouch or event is InputEventJoypadButton or event is InputEventKey:
		if event.is_pressed():
			is_player_ready = true

func _on_ExitTween_tween_all_completed():
	emit_signal("finish")
	queue_free()

func _ready():
	get_tree().paused = true
	_fadein()
