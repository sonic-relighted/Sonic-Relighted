extends Node

onready var host = get_parent()

var loader       : ResourceInteractiveLoader
var bonus_id     : String

func _ready():
	bonus_id = str(ProgressSingletone.playing_bonus_id)
	while bonus_id.length() < 2:
		bonus_id = "0" + bonus_id
#	bonus_id = "_inca_"
	loader = ResourceLoader.load_interactive(
			GlobalConfig.BONUS_STAGES_PATH + bonus_id + "/Bonus.tscn")
	if loader == null:
		show_error("No existe el bonus " + bonus_id)
		return

func _process(delta):
	_bonus_loading(delta)

func _bonus_loading(delta : float):
	if loader == null:
		set_process(false)
		return
	var err = loader.poll()
	if err == ERR_FILE_EOF: # la carga termino
		var resource = loader.get_resource()
		loader = null
		load_bonus_completed(resource)
	elif err == OK:
		update_loading_progress()
	else:
		show_error("Error durante la carga del bonus")
		loader = null

func show_error(err : String):
	var label : Label = Label.new()
	label.text = "ERROR: " + err
	host.hud.add_child(label)
	print("ERROR: " + err)

func update_loading_progress():
	var progress = float(loader.get_stage()) / loader.get_stage_count()
	host.title_screen.set_progress(progress)

func load_bonus_completed(res):
	var bonus = res.instance()
	_instance_initializations(bonus)
	host.on_bonus_loaded(bonus)

func _instance_initializations(level):
	var init = load(
			GlobalConfig.INGAME_ELEMENTS_PATH
			+ "Fwk/Initializations/Initializations.res").instance()
	init.connect("initialization_end", host.title_screen, "_on_initializations_end")
	init.connect("initialization_step", host.title_screen, "_on_initializations_step")
	host.add_child(init)
