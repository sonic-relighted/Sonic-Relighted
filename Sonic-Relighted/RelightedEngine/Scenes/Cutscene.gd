extends Sprite

signal next

export(float) var lock_time : float = .5

onready var content = $Content

onready var cinematic        : int = ProgressSingletone.current_cinematic
onready var scenes           : Array = _load_scenes()
onready var current_scene_id : int = -1
onready var locker_time      : float = .0
onready var finished         : bool = true

func _next_scene():
	current_scene_id += 1
	if current_scene_id >= scenes.size():
		_finish()
	else:
		_create_scene()

func _create_scene():
	if content.get_child_count() > 0: content.get_child(0).queue_free()
	var scene = scenes[current_scene_id].instance()
	content.add_child(scene)
	scene.connect("finish", self, "_next_scene")
	connect("next", scene, "next")
	emit_signal("next")

func _finish():
	finished = true
	$FadeTween.interpolate_property(content, "modulate", content.modulate, Color.black, 1.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	$FadeTween.start()
	yield($FadeTween, "tween_all_completed")
	LevelsCutscenesManager.close_cutscene(cinematic)

func _input(event):
	if finished: return
	if current_scene_id < 0: return
	if locker_time < .0:
		if event is InputEventScreenTouch \
				or event is InputEventJoypadButton \
				or event is InputEventKey \
				or event is InputEventMouseButton:
			emit_signal("next")
		locker_time = lock_time

func _physics_process(delta : float):
	if finished: return
	locker_time -= delta
	if Input.is_action_just_released("menu"):
		_finish()

func _load_scenes() -> Array:
	var r = []
	match cinematic:
		0: r.append(load("res://Cinematics/03/Cinematic.tscn"))
		3: r.append(load("res://Cinematics/03/Cinematic.tscn"))
	return r

func _ready():
	_next_scene()
	content.modulate = Color.black
	$FadeTween.interpolate_property(content, "modulate", Color.black, Color.white, 1.5, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	$FadeTween.start()
	yield(get_tree().create_timer(.5), "timeout")
	finished = false
