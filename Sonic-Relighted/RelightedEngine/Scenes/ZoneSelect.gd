extends Spatial

var loader : ResourceInteractiveLoader

func _ready():
	SettingsData.load_data()
	loader = ResourceLoader.load_interactive(
			GlobalConfig.ZONESELECT_PATH + "World.tscn")
	if loader == null:
		show_error("No se puede abrir el Mundo (?)")
		return

func _process(delta):
	if loader == null:
		set_process(false)
		return
	var err = loader.poll()
	if err == ERR_FILE_EOF: # la carga termino
		var resource = loader.get_resource()
		loader = null
		load_level_completed(resource)
	elif err == OK:
		update_loading_progress()
	else:
		show_error("Error durante la carga")
		loader = null

func show_error(err : String):
	var label : Label = Label.new()
	label.text = "ERROR: " + err
	$HUD.add_child(label)
	print("ERROR: " + err)

func load_level_completed(res):
	var world = res.instance()
	add_child(world)
	$CanvasLayer.queue_free()
	
func update_loading_progress():
	var progress = float(loader.get_stage()) / loader.get_stage_count()
	$CanvasLayer/Percent.anchor_right = progress

