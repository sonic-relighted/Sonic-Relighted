extends Node2D

# Esta escena solo carga configuracion
# esto es por que si no los logos salen pixelados

func _enter_tree():
	SettingsData.load_data()

func _ready():
	get_tree().change_scene(GlobalConfig.SCENES_PATH + "PreMenu.tscn")
