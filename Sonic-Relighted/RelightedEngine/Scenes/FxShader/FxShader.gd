extends CanvasLayer

export(float) var time : float = .5

onready var underwater_tween = $Underwater/Tween
onready var flash_tween      = $Flash/Tween

onready var underwater = $Underwater
onready var flash      = $Flash

func _on_Player_physics_area_enter(type : int):
	if type == PhysicsOverwriteArea.Type.WATER:
		underwater(true)

func _on_Player_physics_area_exit(type : int):
	if type == PhysicsOverwriteArea.Type.WATER:
		underwater(false)

func _on_Player_lost_shield(type : int):
	flash()

func flash():
	if flash_tween.is_active():
		flash_tween.stop_all()
		yield(get_tree(), "idle_frame")
	flash.show()
	flash.modulate.a = .0
	flash_tween.interpolate_property(flash, "modulate:a", 1.0, .0, .1, Tween.TRANS_SINE, Tween.EASE_OUT)
	flash_tween.start()
	yield(flash_tween, "tween_all_completed")
	flash.hide()

func underwater(status : bool):
	AudioServerManager.underwater_effect(status)
	if status:
		_start()
	else:
		_end()

func _start():
	if underwater_tween.is_active():
		underwater_tween.stop_all()
		yield(get_tree(), "idle_frame")
	underwater.show()
	underwater_tween.interpolate_property(underwater, "modulate:a", underwater.modulate.a, 1.0, time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	underwater_tween.start()
#	yield(underwater_tween, "tween_all_completed")
#	underwater.hide()

func _end():
	if underwater_tween.is_active():
		underwater_tween.stop_all()
		yield(get_tree(), "idle_frame")
	underwater_tween.interpolate_property(underwater, "modulate:a", underwater.modulate.a, .0, time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	underwater_tween.start()
	yield(underwater_tween, "tween_all_completed")
	underwater.hide()
