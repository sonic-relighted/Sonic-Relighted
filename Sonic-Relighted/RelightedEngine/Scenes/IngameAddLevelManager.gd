extends Node

onready var host = get_parent()

func _ready():
	set_process(false)
	set_physics_process(false)

func add_level(level):
	host.add_child(level, true)

	_initialize_instances(level)
	
	host.on_level_added(level)

func _create_player():
	var player : Player = load("res://Player/Player.res").instance()
	player.connect("death", host, "_on_player_death")
	player.connect("complete_level", host, "_on_player_complete_level")
	player.connect("hi_speed", host, "on_player_hi_speed")
	player.connect("physics_area_enter", Instances.fxshader, "_on_Player_physics_area_enter")
	player.connect("physics_area_exit", Instances.fxshader, "_on_Player_physics_area_exit")
	player.connect("lost_shield", Instances.fxshader, "_on_Player_lost_shield")
	host.add_child(player)
	player.input_manager.locked = true
	Instances.player = player
	player.reset()
	Builder.build_bottom_shadow(player)
	_quit_player_transparencies(player)

func _quit_player_transparencies(player : Player):
	if not SettingsData.data.transparencies:
		var mesh : MeshInstance = GroupFinder.find_nodes_by_type(
				player.model, MeshInstance)[0]
		var mat : SpatialMaterial = mesh.mesh.surface_get_material(0)
		mat.next_pass = null
		mat.flags_transparent = false

func _initialize_instances(level):
	Instances.level = level
	Instances.music = host.ost_player
	Instances.hud = host.hud
	Instances.hud.update_all()
	Instances.virtual_controller = host.virtual_controller
	Instances.camera = host.player_camera
	Instances.start_point = _start_point()
	Instances.fxshader = host.fxshader
	_create_player()
	_configure_environment(level)

func _configure_environment(level):
	Instances.camera.camera.environment.glow_enabled = SettingsData.data.glow

func _start_point():
	var nodes = get_tree().get_nodes_in_group("StartPoint")
	for point in nodes:
		if point.enabled: return point
	return nodes[0]
