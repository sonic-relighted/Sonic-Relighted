extends AudioStreamPlayer

class_name OstPlayer

export(float) var speed_in = 5
export(float) var speed_out = 5

onready var starting      : bool = false
onready var stopping      : bool = false
onready var pitchchanging : bool = false
onready var hi_speed      : bool = false

var dbs              : float
var stopped_position : float

func play_with_fade_in():
	_do_play(.0)

func stop_with_fade_out():
	starting = false
	stopping = true
	stopped_position = get_playback_position()
	set_process(true)

func set_hi_speed(active : bool):
	hi_speed = active
	pitchchanging = true
	set_process(true)

func pause():
	stop()
	stopped_position = get_playback_position()

func resume_with_fade_in():
	_do_play(stopped_position)

func _do_play(position : float):
	starting = true
	stopping = false
	dbs = 0
	volume_db = linear2db(dbs)
	play(position)
	set_process(true)

func _enter_tree():
	volume_db = linear2db(0)

func _process(delta : float):
	if stopping:
		dbs -= delta * speed_out
		if dbs < .0:
			dbs = .0
			stopping = false
			stop()
			set_process(false)
			return
		volume_db = linear2db(dbs)
	elif starting:
		if is_playing():
			dbs += delta * speed_in
			if dbs > 1.0:
				dbs = 1.0
				starting = false
				if not pitchchanging: set_process(false)
			volume_db = linear2db(dbs)
	if pitchchanging:
		if hi_speed:
			pitch_scale = lerp(pitch_scale, 1.12, delta)
			if pitch_scale >= 1.12:
				pitchchanging = false
				if not starting and not stopping: set_process(false)
		else:
			pitch_scale = lerp(pitch_scale, 1.0, delta)
			if pitch_scale <= 1.0:
				pitchchanging = false
				if not starting and not stopping: set_process(false)

func _ready():
	set_process(false)
