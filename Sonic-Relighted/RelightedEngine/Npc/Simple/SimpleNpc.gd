tool
extends KinematicBody

signal physics_processs_changed(enabled)

class_name SimpleNPC

export(float) var min_x_range       : float = -2.0
export(float) var max_x_range       : float = 2.0
export(float) var min_z_range       : float = -2.0
export(float) var max_z_range       : float = 2.0
export(float) var max_idle_time     : float = 4.0
export(float) var min_idle_time     : float = 3.0
export(float) var max_walk_time     : float = 4.0
export(float) var min_walk_time     : float = 3.0
export(float) var gravity_influence : float = 1.0
export(Resource) var model_resource : Resource = null setget set_model_resource, get_model_resource

onready var timer = $Timer

var model
var default_origin : Vector3
var destination    : Vector3

func set_model_resource(new_res : Resource):
	model_resource = new_res
	if $Anchor:
		if $Anchor.get_child_count():
			$Anchor.get_child(0).queue_free()
		if model_resource:
			model = model_resource.instance()
			$Anchor.add_child(model)

func get_model_resource() -> Resource: return model_resource

func reset():
	default_origin = global_transform.origin

func next_destination():
	destination = _calc_destination()

func set_physics_process(enabled : bool):
	.set_physics_process(enabled)
	emit_signal("physics_processs_changed", enabled)

func _physics_process(delta : float):
	if model.is_attack():
		model.attack(self, delta)
	elif model.is_warn():
		model.warn(self, delta)
	elif model.is_idle():
		_idle()
	elif model.is_walk():
		_walk(delta)

func _idle():
	move_and_slide(Vector3.DOWN * gravity_influence)

func _walk(delta : float):
	var origin = global_transform.origin
	var diff = origin - destination
	
	if Vector2(diff.x, diff.z).length() < .2:
		_end_walk()
	else:
		var new_rot = atan2(diff.x, diff.z)
		rotation.y = lerp_angle(rotation.y, new_rot, delta * 5.0)
		var v = Vector3.FORWARD.rotated(Vector3(.0,1.0,.0), rotation.y)
		v.y -= gravity_influence
		move_and_slide(v)

func _end_walk():
	timer.stop()
	_on_Timer_timeout()

func _calc_destination() -> Vector3:
	var r = default_origin
	r.x += rand_range(-2.0, 2.0)
	r.z += rand_range(-2.0, 2.0)
	return r

func _on_Timer_timeout():
	if model.is_idle():
		destination = _calc_destination()
		model.walk()
		timer.start(rand_range(min_walk_time, max_walk_time))
	elif model.is_walk():
		model.idle()
		timer.start(rand_range(min_idle_time, max_idle_time))

func _ready():
	if Engine.editor_hint:
		set_physics_process(false)
		timer.stop()
		return
	_on_Timer_timeout()
	next_destination()
	set_physics_process(true)

func _enter_tree():
	set_physics_process(false)
