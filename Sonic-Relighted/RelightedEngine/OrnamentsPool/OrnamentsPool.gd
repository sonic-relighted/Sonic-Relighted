extends Spatial

export(Resource) var resource_to_instance : Resource
export(int,1,10) var instances_count : int = 1
export(Array,NodePath) var destinations : Array

onready var instances : Array

const UNLINKED_POSITION : Vector3 = Vector3(0,-100,0)

func _instance_all():
	for i in range(instances_count):
		_instance_one()

func _remove_all():
	while instances.size():
		instances[0].obj.queue_free()
		instances.remove(0)

func _connect_destinations():
	for i in destinations:
		var destination = get_node(i)
		if destination:
			destination.connect("player_entered", self, "_send_to_destination")
			destination.connect("player_exited", self, "_clear_destination")

func _instance_one() -> Spatial:
	var obj : Spatial = resource_to_instance.instance()
	_add(obj)
	_active(obj, false)
	obj.global_transform.origin = UNLINKED_POSITION
	return obj

func _active(obj : Spatial, new_value : bool):
	obj.set_process(new_value)
	obj.set_physics_process(new_value)
	if new_value:
		obj.show()
		if obj.has_method("reset"): obj.reset()
	else:
		obj.hide()

func _add(obj : Spatial):
	add_child(obj)
	var obj_meta = {
		"obj": obj,
		"destination": null
	}
	instances.append(obj_meta)

func _get_available_obj() -> Spatial:
	for instance in instances:
		if instance.destination == null:
			return instance
	return null

func _ready():
	_remove_all()
	_instance_all()
	_connect_destinations()

func _send_to_destination(slots):
	for slot in slots:
		var obj = _get_available_obj()
		if obj == null: return
		_link_obj(obj, slot)

func _clear_destination(slots):
	for slot in slots:
		for instance in instances:
			if instance.destination == slot:
				_unlink_obj(instance, slot)

func _link_obj(obj_meta : Dictionary, slot : Spatial):
	obj_meta.destination = slot
	obj_meta.obj.global_transform.origin = slot.global_transform.origin
	_active(obj_meta.obj, true)

func _unlink_obj(obj_meta : Dictionary, slot : Spatial):
	obj_meta.destination = null
	obj_meta.obj.global_transform.origin = UNLINKED_POSITION
	_active(obj_meta.obj, false)
