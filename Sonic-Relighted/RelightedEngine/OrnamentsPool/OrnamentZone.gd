#tool

extends Area

signal player_entered
signal player_exited

export(int,1,20) var collider_size : int = 10 setget set_collider_size, get_collider_size

onready var slots = _get_slots()

func set_collider_size(new_size):
	collider_size = new_size
	if Engine.editor_hint: $CollisionShape.scale = Vector3.ONE * collider_size

func get_collider_size():
	return collider_size

func enable():
	emit_signal("player_entered", slots)

func disable():
	emit_signal("player_exited", slots)

func  _get_slots() -> Array:
	var r = []
	for i in range(1, get_child_count()):
		r.append(get_child(i))
	return r

func _ready():
	if get_child_count() == 1:
		print("ERROR, " + get_name() + " no tiene slots. Un slot es un Spatial en el que se colocara cada adorno")
		queue_free()
		return
	if not Engine.editor_hint: $CollisionShape.scale = Vector3.ONE * collider_size
