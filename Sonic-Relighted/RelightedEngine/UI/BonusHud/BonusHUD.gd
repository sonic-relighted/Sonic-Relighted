extends CanvasLayer

signal timeout

export(float) var seconds_gameplay : float = 120.0

onready var rings          : Node2D = $SpritesContainer/Rings
onready var timer          : Node2D = $SpritesContainer/Timer
onready var lifes          : Node2D = $SpritesContainer/Lifes
onready var rings_needed   : Node2D = $SpritesContainer/RingsNeededNotice
onready var rings_needed_anim : AnimationPlayer = $SpritesContainer/RingsNeededNotice/AnimationPlayer

onready var current_time   : float = seconds_gameplay
onready var controller = $Controller

func _ready():
	update_rings()
	set_process(false)

func _process(delta):
#	_pausa_menu()
	_timer(delta)

#func _pausa_menu():
#	if controller.is_button_menu_just_pressed() or Input.is_action_just_pressed("menu"):
#		_show_pause_menu()

func _timer(delta):
	var prev_secs : int = ProgressSingletone.ingame_seconds
	current_time -= delta
	var current_secs : int = int(current_time)
	if prev_secs != current_secs:
		ProgressSingletone.ingame_seconds = current_secs
		update_time()
	if current_secs <= 0:
		emit_signal("timeout")

func update_all():
	update_rings()
	update_time()
	update_lifes()

func update_rings():
	rings.text = str(ProgressSingletone.ingame_rings)

func update_time():
	timer.text = Formatter.time_seconds_to_timestamp(ProgressSingletone.ingame_seconds)

func update_lifes():
	lifes.text = str(ProgressSingletone.game_slot_data.lifes)

func show_rings_needed(count : int):
	var text = tr("BONUS.rings_needed").replace("${RINGS}", str(count))
	rings_needed.set_text(text)
	rings_needed_anim.stop()
	rings_needed_anim.play("show")

func show_pause_menu():
	add_child(LevelsCutscenesManager.build_bonus_pause_screen())
