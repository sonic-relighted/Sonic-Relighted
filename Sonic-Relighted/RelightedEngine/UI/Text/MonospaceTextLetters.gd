tool
extends Node2D

class_name MonospaceTextLetters

enum CENTERED { LEFT,CENTER,RIGHT }

export(Texture)          var texture     : Texture       setget set_texture,get_texture
export(CENTERED)         var centered    : int           setget set_centered,is_centered
export(String,MULTILINE) var text        : String        setget set_text,get_text
export(int, 6, 512)      var char_width  : int     = 256 setget set_char_width,get_char_width
export(int, 6, 512)      var char_height : int     = 256 setget set_char_height,get_char_height

func set_texture(new_texture):
	texture = new_texture
	if $Template == null: return
	$Template.texture = texture
	_remove_text()
	_make_text()
	_center()

func get_texture():
	return texture

func set_centered(new_centered):
	centered = new_centered
	_center()

func is_centered():
	return centered

func set_text(new_text):
	text = new_text.to_lower()
	if $Template == null: return
	_remove_text()
	_make_text()
	_center()

func get_text():
	return text

func set_char_width(new_width):
	char_width = new_width
	if $Template == null: return
	_remove_text()
	_make_text()
	_center()

func get_char_width():
	return char_width

func set_char_height(new_height):
	char_height = new_height
	if $Template == null: return
	_remove_text()
	_make_text()
	_center()

func get_char_height():
	return char_height

func _remove_text():
	for child in $Content.get_children():
		child.queue_free()

func _make_text():
	var x : int = 0
	var y : int = 0
	for l in text.to_ascii():
		if l >= 97 and l <= 122: #a-z
			_put_letter(l - 97, x, y)
		elif l >= 48 and l <= 58: # 0-9
			_put_letter(l - 22, x, y)
		elif l == 46: # .
			_put_letter(37, x, y)
		elif l == 44: # ,
			_put_letter(38, x, y)
		elif l == 63: # ?
			_put_letter(39, x, y)
		if l == 10:
			x = 0
			y += char_height
		else:
			x += char_width

func _put_letter(ascii : int, x : int, y : int):
	var letter = $Template.duplicate()
	letter.show()
	letter.frame = ascii
	letter.name = "Letter_" + char(ascii)
	$Content.add_child(letter)
	letter.position.x = x
	letter.position.y = y

func _center():
	if not $Content: return
	if centered:
		# No funciona con retornos de carro
		var w = text.length()
		var h = 1
		if text.find("\n") > -1:
			var lines = text.split("\n")
			w = 0
			h = lines.size()
			for line in lines:
				w = max(w, line.length())
		w *= centered
		$Content.position.x = -((w - 1) * char_width / 2)
		$Content.position.y = -((h - 1) * char_height / 2)
	else:
		$Content.position.x = .0
		$Content.position.y = .0

func _ready():
	_remove_text()
	_make_text()
	_center()
