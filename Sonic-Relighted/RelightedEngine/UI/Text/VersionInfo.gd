tool
extends MonospaceTextLetters

func _get_version() -> String:
	if Engine.editor_hint:
		return "XXXXXXXXXX"
	return GlobalConfig.VERSION

func _ready():
	set_text("Version " + _get_version())
	._ready()
