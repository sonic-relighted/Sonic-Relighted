tool
extends Area2D

class_name SimpleMenuButton

signal clicked

export(NodePath) var near_up      : NodePath
export(NodePath) var near_left    : NodePath
export(NodePath) var near_right   : NodePath
export(NodePath) var near_bottom  : NodePath

export(bool)    var enabled       : bool  = true
export(bool)    var omnipresent   : bool  = false
export(bool)    var emit_instant  : bool  = true
export(float)   var hilight       : float = 3.0
export(float)   var hilight_speed : float = 2.0
export(Vector2) var collider_size : Vector2 = Vector2(1, 1) setget set_collider_size,get_collider_size

export(Color)   var color_disabled1 : Color = Color(.3, .3, .3)
export(Color)   var color_disabled2 : Color = Color(.2, .2, .2)
export(Color)   var color_normal1   : Color = Color(.9, .9, .9)
export(Color)   var color_normal2   : Color = Color(.8, .8, .8)
export(Color)   var color_focused1  : Color = Color(1.0, 1.0, .3)
export(Color)   var color_focused2  : Color = Color(.9, .9, .2)
export(Color)   var color_mouseover_normal1   : Color = Color(1.0, 1.0, 1.0)
export(Color)   var color_mouseover_normal2   : Color = Color(.9, .9, .9)
export(Color)   var color_mouseover_focused1  : Color = Color(1.0, 1.0, .6)
export(Color)   var color_mouseover_focused2  : Color = Color(1.0, 1.0, .5)

export(bool)    var focused : bool  = false setget set_focused,get_focused

onready         var mousehover : bool = false

var shader_material : ShaderMaterial

func set_collider_size(new_size):
	collider_size = new_size
	if $CollisionShape2D:
		$CollisionShape2D.scale = new_size
	if $TouchScreenButton:
		$TouchScreenButton.scale = new_size

func get_collider_size():
	return collider_size

func set_focused(new_focused):
	focused = new_focused

func get_focused():
	return focused

func _physics_process(delta : float):
	if Engine.editor_hint: return
	if not enabled:
		shader_material.set_shader_param("replace_1", color_disabled1)
		shader_material.set_shader_param("replace_2", color_disabled2)
	elif focused:
		if mousehover:
			shader_material.set_shader_param("replace_1", color_mouseover_focused1)
			shader_material.set_shader_param("replace_2", color_mouseover_focused2)
		else:
			shader_material.set_shader_param("replace_1", color_focused1)
			shader_material.set_shader_param("replace_2", color_focused2)
	else:
		if mousehover:
			shader_material.set_shader_param("replace_1", color_mouseover_normal1)
			shader_material.set_shader_param("replace_2", color_mouseover_normal2)
		else:
			shader_material.set_shader_param("replace_1", color_normal1)
			shader_material.set_shader_param("replace_2", color_normal2)

func _input_event(viewport, event, shape_idx):
	if Engine.editor_hint: return
	if not enabled: return
	if not omnipresent:
		_do_input(event)

func _input(event):
	if Engine.editor_hint: return
	if not enabled: return
	if omnipresent:
		_do_input(event)
	_do_key_accept(event)

func _do_input(event):
	if enabled and not event.is_pressed():
		if event is InputEventJoypadButton \
		or event is InputEventKey \
		or event is InputEventMouseButton:
#		or event is InputEventScreenTouch \
			_click()
#			animator.play("click")
#			set_process(true)

func _do_key_accept(event):
	if enabled and focused:
		if event.is_action_released("ui_accept"):
			_click()

func _click():
	if emit_instant: emit_signal("clicked")
	$ClickTween.interpolate_property(
			self, "modulate",
			Color.white * 1.1, Color.white,
			.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
	)
	$ClickTween.interpolate_property(
			self, "scale",
			Vector2(1.5, 1.2), Vector2.ONE,
			.5, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT
	)
	$ClickTween.start()
	if not emit_instant: emit_signal("clicked")

func _on_SimpleMenuButton_mouse_entered():
	if Engine.editor_hint: return
	if not enabled: return
	mousehover = true

func _on_SimpleMenuButton_mouse_exited():
	if Engine.editor_hint: return
	mousehover = false

func _ready():
	shader_material = material.duplicate()
	if not Engine.editor_hint:
		material = shader_material
	for child in get_children():
		if child is Sprite:
			child.material = shader_material

func _on_TouchScreenButton_pressed():
	if enabled and get_tree().paused:
		_click()
