extends TouchScreenButton

export(bool) var is_digital : bool

onready var ongoing_drag = -1

onready var host = get_parent()

const radius = Vector2(22, 22)
const boundary = 48
const return_speed = 14
const return_pos = Vector2.ZERO - radius
const deadzone = 10

func _physics_process(delta):
	if ongoing_drag == -1:
		position = lerp(position, return_pos, delta * return_speed)

func _input(event):
	if event is InputEventScreenDrag or (event is InputEventScreenTouch and event.is_pressed()):
		var event_dist_from_center = (event.position - host.global_position).length()
		
		if event_dist_from_center <= boundary * global_scale.x or event.get_index() == ongoing_drag:
			set_global_position(event.position - radius * global_scale)
			
			var button_pos = _get_button_pos()
			if button_pos.length() > boundary:
				set_position(button_pos.normalized() * boundary - radius)
			
			ongoing_drag = event.get_index()
	
	if event is InputEventScreenTouch and !event.is_pressed() and event.get_index() == ongoing_drag:
		ongoing_drag = -1

func _get_button_pos() -> Vector2:
	return position + radius

func get_value() -> Vector2:
	if is_digital:
		return _get_digital_value()
	return _get_analog_value()

func _get_digital_value() -> Vector2:
	var analog = _get_analog_value()
	var r = Vector2.ZERO
	if analog.x > .2:
		r.x = .5
	elif analog.x < -.2:
		r.x = -.5

	if analog.y > .2:
		r.y = .5
	elif analog.y < -.2:
		r.y = -.5

	if analog.length() > .6:
		r *= 2

	return r

func _get_analog_value() -> Vector2:
	var button_pos = _get_button_pos()
	if button_pos.length() > deadzone:
		var r = button_pos / boundary
		return r
	return Vector2.ZERO

func reset():
	position = return_pos
