extends TouchScreenButton

onready var just_pressed = false
onready var just_released = false

onready var lock_just_pressed = false
onready var lock_just_released = false

func _physics_process(delta):
	_update_pressed()
	_update_released()

func _update_pressed():
	if just_pressed:
		just_pressed = false

	if is_pressed():
		if !lock_just_pressed:
			just_pressed = true
			lock_just_pressed = true
	else:
		lock_just_pressed = false

func _update_released():
	if just_released:
		just_released = false

	if !is_pressed():
		if !lock_just_released:
			just_released = true
			lock_just_released = true
	else:
		lock_just_released = false

