extends Node2D

export(bool)     var has_menu_button : bool = true
export(bool)     var is_digital      : bool = true

export(Resource) var button1_icon : Resource
export(Resource) var button2_icon : Resource
export(Resource) var button3_icon : Resource
export(Resource) var button4_icon : Resource

onready var joystick    = $Joystick
onready var button_menu = $ButtonMenu
onready var button1     = $Button1
onready var button2     = $Button2
onready var button3     = $Button3
onready var button4     = $Button4

func get_pad_value() -> Vector2:
	return joystick.get_pad_value()

func is_menu_just_pressed() -> bool:
	return button_menu.just_pressed

func is_button1_pressed() -> bool:
	return button1.is_pressed()

func is_button1_just_pressed() -> bool:
	return button1.just_pressed

func is_button1_just_released() -> bool:
	return button1.just_released

func is_button2_pressed() -> bool:
	return button2.is_pressed()

func is_button2_just_pressed() -> bool:
	return button2.just_pressed

func is_button2_just_released() -> bool:
	return button2.just_released


func is_button3_pressed() -> bool:
	return button3.is_pressed()

func is_button3_just_pressed() -> bool:
	return button3.just_pressed

func is_button3_just_released() -> bool:
	return button3.just_released

func is_button4_pressed() -> bool:
	return button4.is_pressed()

func is_button4_just_pressed() -> bool:
	return button4.just_pressed

func is_button4_just_released() -> bool:
	return button4.just_released

func _ready():
	var resolution_id = _get_resolution_id()
	if resolution_id >= 0:
		_positioning(resolution_id)
	
	joystick.pad_button.is_digital = is_digital
	
	$Button1/Icon.texture = button1_icon
	$Button2/Icon.texture = button2_icon
	$Button3/Icon.texture = button3_icon
	$Button4/Icon.texture = button4_icon
	
	if has_menu_button:
		button_menu.show()
	else:
		button_menu.hide()

	Input.connect("joy_connection_changed", self, "_on_joy_connection_changed")
	_on_joy_connection_changed(0, Input.is_joy_known(0))

func _get_resolution_id() -> int:
	if SettingsData.data:
		return SettingsData.data.resolution
	return -1

func _positioning(resolution_id):
	var window = Vector2(640.0, 360.0)
	
	var w_factor = window.x / SettingsData.screen_resolution.x
	var h_factor = window.y / SettingsData.screen_resolution.y
	
	var new_scale = Vector2(1 / w_factor, 1 / h_factor)
	joystick.global_scale = new_scale
	button_menu.global_scale = new_scale
	button1.global_scale = new_scale
	button2.global_scale = new_scale
	button3.global_scale = new_scale
	button4.global_scale = new_scale

	joystick.position =    Vector2(96.0  / w_factor, 280.0 / h_factor)
	button_menu.position = Vector2(300.0 / w_factor, 320.0 / h_factor)
	button1.position =     Vector2(480.0 / w_factor, 280.0 / h_factor)
	button2.position =     Vector2(540.0 / w_factor, 260.0 / h_factor)
	button3.position =     Vector2(480.0 / w_factor, 220.0 / h_factor)
	button4.position =     Vector2(540.0 / w_factor, 200.0 / h_factor)

func _on_joy_connection_changed(device_id, connected):
	if device_id == 0:
		if MySystem.is_mobile():
			if connected:
				hide()
				joystick.reset()
			else:
				show()
		else:
			hide()
