extends Sprite

onready var pad_button = $PadButton

func get_pad_value() -> Vector2:
	return pad_button.get_value()

func reset():
	pad_button.reset()
