extends Button

class_name LevelSelectButton

onready var name_label = $Name
onready var time_label = $Time
onready var stars = [$Star1,$Star2,$Star3]
onready var perfect = $Perfect
onready var level_is_enabled : bool = true

func set_name(text : String):
	name_label.text = text

func set_time(time_secs : int):
	var secs = str(time_secs % 60)
	var minutes = str(time_secs / 60)
	while secs.length() < 2:
		secs = "0" + secs
	while minutes.length() < 2:
		minutes = "0" + minutes
	
	time_label.text = minutes + ":" + secs

func update_stars(n : int, is_perfect : bool):
	var i = 0
	while i < stars.size():
		if i < n:
			stars[i].show()
		else:
			stars[i].hide()
		i = i + 1
	if is_perfect:
		perfect.show()
	else:
		perfect.hide()

func disable_level():
	level_is_enabled = false
	$Preview.queue_free()
	$Name.queue_free()
	$Time.queue_free()
	$Star1.queue_free()
	$Star2.queue_free()
	$Star3.queue_free()
	$Perfect.queue_free()
	disabled = true
	focus_mode = Control.FOCUS_NONE
