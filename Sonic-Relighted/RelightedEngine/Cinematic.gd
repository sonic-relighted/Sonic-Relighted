extends Node2D

signal finish

class_name Cinematic

onready var controller : AnimationPlayer = $Controller

onready var current_animation : int = 0

func next():
	current_animation += 1
	var n = Formatter.filled_number(current_animation, 2)
	controller.stop()
	if controller.has_animation(n):
		controller.play(n)
	else:
		emit_signal("finish")

func _animation_finished(anim_name):
	yield(get_tree(), "idle_frame")
	next()

func _ready():
	controller.connect("animation_finished", self, "_animation_finished")
