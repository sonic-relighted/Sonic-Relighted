extends AbstractEnemyState

export(float, .0, 10.0) var fixed_time : float = .0
export(float, .0, 10.0) var rand_time : float = 2.5

export(NodePath) var death_state_path : NodePath
export(NodePath) var next_state_path : NodePath

onready var death_state : AbstractEnemyState = get_node(death_state_path)
onready var next_state  : AbstractEnemyState = get_node(next_state_path)

var timer : float

func is_attack() -> bool: return false

func enter(host):
	timer = randf() * rand_time + fixed_time

func exit(host):
	pass

func on_animation_finished(anim_name : String):
	pass

func step(host, delta : float):
	if host.is_destroyed: return death_state
	timer -= delta
	if timer < 0:
		return next_state
	_brake(host, delta)
	return self

func _brake(host, delta : float):
	host.velocity = lerp(host.velocity, Vector3.ZERO, delta * 5)

func step_move(host, delta : float):
	pass
