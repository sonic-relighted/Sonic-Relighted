extends Node

class_name RandomPositioning

func _ready():
	set_process(false)
	set_physics_process(false)

#
# Devuelve una posicion aleatoria, sin mas
#
func get_position(offset : Vector3) -> Vector2:
	var x : float = randf() * 2 - 1
	var z : float = randf() * 2 - 1
	if x > 1: x *= 3
	if z > 1: z *= 3
	return Vector2(offset.x + x, offset.z + z)

#
# Devuelve una posicion aleatoria, pero alejada de 0,0
#
func get_position2(offset : Vector3, min_distance : float, range_x : float, range_y : float) -> Vector2:
	var r = Vector2.ZERO
	while r.length() < min_distance:
		var x = (randi() % int(range_x * 2) + 1) - range_x
		var z = (randi() % int(range_y * 2) + 1) - range_y
		r = Vector2(x, z)
	return Vector2(r.x + offset.x, r.y + offset.z)
