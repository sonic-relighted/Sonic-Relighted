extends Node

class_name AbstractEnemyStateMachine

export(NodePath) var default_state : NodePath

var current_state
var previous_state

func _ready():
	set_process(false)
	set_physics_process(false)

func initialize():
	current_state  = get_node(default_state)
	previous_state = current_state
	current_state.enter(get_parent())

func on_animation_finished(anim_name : String):
	current_state.on_animation_finished(anim_name)

func is_attack():
	return current_state.is_attack()

func step(host, delta : float):
	var next_state = current_state.step(host, delta)
	current_state.step_move(host, delta)
	_change_state(host, next_state)

func _change_state(host, next_state):
	if next_state == current_state: return
	current_state.exit(host)
	previous_state = current_state
	current_state = next_state
	current_state.enter(host)
