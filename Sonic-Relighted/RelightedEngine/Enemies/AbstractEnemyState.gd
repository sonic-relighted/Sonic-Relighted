extends Node

class_name AbstractEnemyState

onready var state_machine : AbstractEnemyStateMachine = get_parent()

func is_attack() -> bool:
	return false

func enter(host):
	pass

func exit(host):
	pass

func step(host, delta):
	pass

func step_move(host, delta):
	pass

func on_animation_finished(anim_name : String):
	pass
