extends Area

func enable(body):
	get_parent()._on_AttackArea_body_entered(body)

func disable(body):
	get_parent()._on_AttackArea_body_exited(body)
