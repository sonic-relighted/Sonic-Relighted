extends KinematicBody

class_name AbstractEnemy

func _ready():
	for other in GroupFinder.find_nodes_in_group(get_parent(), "Enemy"):
		add_collision_exception_with(other)

func reset():
	$Resetable.reset()

func damage() -> bool:
	return true
