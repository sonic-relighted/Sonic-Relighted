tool
extends TextureRect

export(bool) var enabled : bool = true setget set_enabled,get_enabled


func set_enabled(value : bool):
	modulate = _get_color(value)
	enabled = value

func get_enabled():
	return enabled

func _get_color(value : bool):
	var r : Color = Color.white
	if !value: r.a = .15
	return r

func _ready():
	var FACTORS = [.5,.5,1,1,2,2,3,3]
	var factor = FACTORS[SettingsData.data.resolution]
	rect_size *= factor
	margin_left += factor * 10
