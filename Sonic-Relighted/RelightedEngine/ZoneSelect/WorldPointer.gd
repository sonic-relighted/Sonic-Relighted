extends Spatial

signal show_level_name(id)
signal show_level_details(id)
signal close_level_name
signal close_level_details
signal show_bonus_name(id)
signal show_bonus_details(id)
signal close_bonus_name
signal close_bonus_details
signal open_level(id)
signal open_bonus(id)

onready var host : Spatial = get_parent()

onready var inp                 : PlayerInputManager = $InputManager
onready var navigation_animator : AnimationPlayer    = $NavigationAnimation
onready var model_animator      : AnimationPlayer    = $Tornado/AnimationPlayer
onready var error_audio         : AudioStreamPlayer  = $ErrorAudio
onready var open_audio          : AudioStreamPlayer  = $OpenAudio
onready var select_audio        : AudioStreamPlayer  = $SelectLevelAudio

onready var lock_right : bool = false
onready var lock_left  : bool = false

onready var planet              : Spatial = $"../World"

onready var hover               : Spatial = null
onready var showing_description : bool    = false

enum ANGLES {
	NORTH, EAST, SOUTH, WEST
}

onready var speed         : float = .0
onready var angle         : int   = ANGLES.NORTH
onready var pointer_angle : float = .0

func _ready():
	navigation_animator.play("Walk", .5)
	model_animator.get_animation("Idle").loop = true
	model_animator.play("Idle")

func _physics_process(delta):
	_control(delta)
	_move(delta)
	_pointer_rotation(delta)

func _control(delta : float):
	if Input.is_action_just_pressed("ui_cancel"):
		if showing_description:
			showing_description = false
			emit_signal("close_level_details")
			emit_signal("close_bonus_details")
		else:
			_back()
	
	var analog = inp.get_analog_joystick_value()#.rotated(-pointer_angle)
	var analog_up = analog.y < -.5
	var analog_down = analog.y > .5
	var analog_left = !lock_left and analog.x < -.9
	var analog_right = !lock_right and analog.x > .9
	lock_left = analog.x < -.9
	lock_right = analog.x > .9

	if inp.is_pressing_up() or analog_up:
		speed += 8 * delta
	elif inp.is_pressing_down() or analog_down:
		speed -= 8 * delta
	if inp.is_just_pressed_right() or analog_right:
		navigation_animator.stop()
		navigation_animator.play("Right", .1)
		angle += 1.0
		pointer_angle += AngleConstants.SEMI_PI
	elif inp.is_just_pressed_left() or analog_left:
		navigation_animator.stop()
		navigation_animator.play("Left", .1)
		angle -= 1.0
		pointer_angle -= AngleConstants.SEMI_PI
	while angle > 3.0: angle -= 4.0
	while angle < 0.0: angle += 4.0
	
	if hover:
		if inp.is_just_pressed_jump():
			if hover.get_name().find("Level") != -1:
				_open_level()
			else:
				_open_bonus()

	if inp.is_just_pressed_roll():
		_back()

	if speed != .0:
		speed = lerp(speed, .0, delta * 8)
		if speed > 2.0: speed = 2.0
		if speed < -2.0: speed = -2.0

func _move(delta : float):
	if abs(speed) < .1: return
	if angle == ANGLES.NORTH:
		planet.rotate_z(speed * delta)
	elif angle == ANGLES.EAST:
		planet.rotate_x(-speed * delta)
	elif angle == ANGLES.SOUTH:
		planet.rotate_z(-speed * delta)
	else:
		planet.rotate_x(speed * delta)

func _pointer_rotation(delta : float):
	if pointer_angle == rotation.y: return
	rotation.y = lerp(
			rotation.y,
			-pointer_angle,
			delta * 10)

func _open_level():
	if hover == null:
		error_audio.play()
		return
	
	var id = host.level_info.get_level_id(hover.get_name())
	if host.level_info.is_locked_level(id):
		error_audio.play()
		return

	if showing_description:
		open_audio.play()
		emit_signal("open_level", id)
		set_process(false)
		set_physics_process(false)
	else:
		emit_signal("show_level_details", id)
		showing_description = true

func _open_bonus():
	if hover == null:
		error_audio.play()
		return
	
	var id = host.bonus_info.get_bonus_id(hover.get_name())
	if host.bonus_info.is_locked_bonus(id):
		error_audio.play()
		return

	if showing_description:
		open_audio.play()
		emit_signal("open_bonus", id)
		set_process(false)
		set_physics_process(false)
	else:
		emit_signal("show_bonus_details", id)
		showing_description = true

func _back():
	get_tree().change_scene(GlobalConfig.SCENES_PATH + "menu.tscn")

func _on_HoverArea_area_entered(area):
	hover = area.get_parent()
	
	var id : int = host.level_info.get_level_id(hover.get_name())
	var type : String = "level" if hover.get_name().find("Level") != -1 else "bonus"
	if type == "bonus" and not host.bonus_info.is_locked_bonus(id):
		emit_signal("show_" + type + "_name", id)
	elif type == "level" and not host.level_info.is_locked_level(id):
		emit_signal("show_" + type + "_name", id)
	select_audio.play()

func _on_HoverArea_area_exited(area):
	if hover == null: return
	var id : int = host.level_info.get_level_id(hover.get_name())
	var type : String = "level" if hover.get_name().find("Level") != -1 else "bonus"
	emit_signal("close_" + type + "_name")
	emit_signal("close_" + type + "_details")
	hover = null
	showing_description = false

func _on_NavigationAnimation_animation_finished(anim_name):
	navigation_animator.play("Walk", .5)
