extends Node

class_name WorldBonusInfo

func get_bonus_id(level_name : String) -> int:
	return int(level_name\
			.substr(level_name.find("0"),
					level_name.length()))

func is_locked_bonus(id : int) -> bool:
	return false # contador de llaves

func get_bonus_data(id : int) -> Dictionary:
	var id_str = str(id + 1)
	var r = {
		"type": "bonus",
		"name": tr("BONUS.Name." + id_str),
		"description": tr("BONUS.Description." + id_str)
	}
	return r
