extends CanvasLayer

class_name WorldHud

onready var sprites_animation         : AnimationPlayer = $SpritesContainer/SpritesAnimation
onready var level_name_background     : TextureRect = $TitleImg
onready var level_name_zone_name      : Label       = $ZoneName
onready var level_name_act            : Label       = $Act
onready var level_details             : Control     = $LevelDetails
onready var level_details_zigzag      : TextureRect = $ZigZag
onready var level_details_level_name  : Label       = $LevelDetails/LevelName
onready var level_details_level_act   : Label       = $LevelDetails/Act
onready var level_details_level_time  : Label       = $LevelDetails/BestTimeValue
onready var level_details_level_time_img : TextureRect = $LevelDetails/BestTimeImg
onready var level_details_level_stars_container : Control = $LevelDetails/Stars
onready var level_details_level_stars : Array       = [
		$LevelDetails/Stars/Star1,
		$LevelDetails/Stars/Star2,
		$LevelDetails/Stars/Star3,
		$LevelDetails/Stars/Star4,
		$LevelDetails/Stars/Star5]
onready var level_details_level_desc  : Label       = $LevelDetails/Description
onready var level_details_level_img   : TextureRect = $LevelDetails/Thumbnail

onready var lifes                     : Node2D      = $SpritesContainer/Lifes
onready var score                     : Node2D      = $SpritesContainer/Score

func hide_level_name():
	if $TitleImg.visible:
		$TitleAnimator.play("hide")
#	level_name_zone_name.hide()
#	level_name_act.hide()
#	level_name_background.hide()

func show_level_name(id : int, data : Dictionary):
	$TitleAnimator.play("show")
#	level_name_zone_name.show()
#	level_name_act.show()
#	level_name_background.show()
	level_name_zone_name.text = data.name
	if data.has("act"):
		level_name_act.text = data.act
	else:
		level_name_act.text = ""

func hide_level_details():
#	level_details.hide()
#	level_locked.hide()
#	level_details_zigzag.hide()
	if $LevelDetails.visible:
		$DescriptionAnimator.play("hide")

func show_level_details(id : int, data : Dictionary):
	$DescriptionAnimator.play("show")
#	level_details.show()
	level_details_level_name.text = data.name
	if data.has("act"):
		level_details_level_act.text = data.act
	else:
		level_details_level_act.text = ""
	if data.has("time"):
		level_details_level_time.text = Formatter.time_seconds_to_timestamp(data.time)
		level_details_level_time_img.show()
	else:
		level_details_level_time.text = ""
		level_details_level_time_img.hide()
	if data.has("stars"):
		_enable_stars(data.stars)
		level_details_level_stars_container.show()
	else:
		_enable_stars([])
		level_details_level_stars_container.hide()
	
	level_details_level_desc.text = data.description
	level_details_level_img.texture = _get_thumbnail(id, data.type)
	level_details_zigzag.show()

func _ready():
	_show_lifes()
	_show_score()

func _show_lifes(): lifes.text = str(ProgressSingletone.game_slot_data.lifes)
func _show_score(): score.text = str(ProgressSingletone.game_slot_data.score)

func _enable_stars(values : Array):
	var i = 0
	while i < values.size():
		level_details_level_stars[i].set_enabled(values[i])
		i += 1

func _get_thumbnail(id : int, type : String):
	var num : String = str(id + 1)
	var r = load(GlobalConfig.ZONESELECT_PATH + "Thumbnail/" + type + "/" + num + ".png")
	if r == null:
		return load("res://Textures/Dkbd/Nums/" + num + ".png")
	return r
