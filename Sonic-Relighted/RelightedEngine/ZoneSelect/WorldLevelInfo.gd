extends Node

class_name WorldLevelInfo

func get_level_id(level_name : String) -> int:
	return int(level_name\
			.substr(level_name.find("0"),
					level_name.length()))

func is_locked_level(id : int) -> bool:
	if id == 0: return false
	var completed_levels = ProgressSingletone.game_slot_data.completed_levels
	return completed_levels[id - 1] == false

func get_enabled_zones() -> Array:
	var completed_levels = ProgressSingletone.game_slot_data.completed_levels
	var enabled = [true, false, false, false, false, false, false]
	enabled[1] = completed_levels[0] and completed_levels[1]
	enabled[2] = completed_levels[2] and completed_levels[3] and completed_levels[4]
	enabled[3] = completed_levels[5] and completed_levels[6] and completed_levels[7]
	enabled[4] = completed_levels[8] and completed_levels[9] and completed_levels[10]
	enabled[5] = completed_levels[11] and completed_levels[12] and completed_levels[13]
	enabled[6] = completed_levels[14] and completed_levels[15] and completed_levels[16]
	return enabled

func get_level_data(id : int) -> Dictionary:
	var id_str = str(id + 1)
	var r = {
		"type": "level",
		"name": tr("LEVEL.Name." + id_str),
		"act": tr("LEVEL.Act." + id_str),
		"description": tr("LEVEL.Description." + id_str),
		"time": ProgressSingletone.game_slot_data.time[id],
		"stars": ProgressSingletone.game_slot_data.stars[id]
	}
	return r
