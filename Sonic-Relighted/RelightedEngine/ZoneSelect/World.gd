extends Spatial

onready var bonus_info : WorldBonusInfo = $BonusInfo
onready var level_info : WorldLevelInfo = $LevelInfo
onready var hud        : WorldHud       = $HUD

export(Resource) var pin_locked         : Resource
export(Resource) var pin_completed      : Resource
export(Resource) var pin_pending        : Resource
export(Resource) var pin_bonus_enabled  : Resource
export(Resource) var pin_bonus_disabled : Resource

func _ready():
	hud.hide_level_name()
	hud.hide_level_details()
	Instances.virtual_controller = $HUD/Controller
	_enable_lamps()
	_enable_levels()
	_enable_bonuses()
	$World.rotation = Vector3.ZERO
	$World.rotate_z(ProgressSingletone.game_slot_data.world_rotation[2])
	$World.rotate_x(ProgressSingletone.game_slot_data.world_rotation[0])
	$World.rotate_y(ProgressSingletone.game_slot_data.world_rotation[1])
	$Pointer.angle = ProgressSingletone.game_slot_data.world_pointer_angle
	$Pointer.pointer_angle = $Pointer.angle * AngleConstants.SEMI_PI
	$Pointer.rotation.y = -$Pointer.pointer_angle

func _enable_lamps():
	for lamp in _get_lamps_of_enabled_levels():
		var id = level_info.get_level_id(lamp.get_name())
		lamp.add_child(Builder.build_omni_light(6, 3))

func _enable_levels():
	for building in _get_all_buildings():
		var chincheta = _create_pin(
				building.global_transform,
				building.name)
		var parent : Spatial = building.get_parent()
#		parent.remove_child(building)
		building.queue_free()
		parent.add_child(chincheta, true)
	
	for building in _get_buildings_of_enabled_levels():
		var id : int = level_info.get_level_id(building.get_name())
		var spr = Builder.build_number_sprite(id + 1)
		spr.translation.y = .5
		building.add_child(spr)

func _enable_bonuses():
	for building in _get_all_bonus_buildings():
		var chincheta = _create_bonus_pin(
				building.global_transform,
				building.name)
				
		var parent : Spatial = building.get_parent()
#		parent.remove_child(building)
		building.queue_free()
		parent.add_child(chincheta, true)

func _create_pin(new_transform : Transform, new_name : String):
	var r
	var level_id = level_info.get_level_id(new_name)
	var is_locked : bool = level_info.is_locked_level(level_id)
	if is_locked:
		r = pin_locked.instance()
	else:
		var is_completed : bool = level_info.get_level_data(level_id).time != -1
		if is_completed:
			r = pin_completed.instance()
		else:
			r = pin_pending.instance()
	r.global_transform = new_transform
	r.name = new_name
	r.add_child(Builder.build_area_sphere(.15))
	return r

func _create_bonus_pin(new_transform : Transform, new_name : String):
	var r
	var bonus_id = bonus_info.get_bonus_id(new_name)
	var is_locked : bool = bonus_info.is_locked_bonus(bonus_id)
	if is_locked:
		r = pin_bonus_disabled.instance()
	else:
		r = pin_bonus_enabled.instance()
	r.global_transform = new_transform
	r.name = new_name
	r.add_child(Builder.build_area_sphere(.15))
	return r

func _on_open(id : int):
	LevelsCutscenesManager.open_level(id)
	ProgressSingletone.game_slot_data.world_rotation[0] = $World.rotation.x
	ProgressSingletone.game_slot_data.world_rotation[1] = $World.rotation.y
	ProgressSingletone.game_slot_data.world_rotation[2] = $World.rotation.z
	ProgressSingletone.game_slot_data.world_pointer_angle = $Pointer.angle

func _on_open_bonus(id : int):
	LevelsCutscenesManager.open_bonus(id)
	ProgressSingletone.game_slot_data.world_rotation[0] = $World.rotation.x
	ProgressSingletone.game_slot_data.world_rotation[1] = $World.rotation.y
	ProgressSingletone.game_slot_data.world_rotation[2] = $World.rotation.z
	ProgressSingletone.game_slot_data.world_pointer_angle = $Pointer.angle

func _on_Pointer_open_level(id : int):
	var fade_out = load(GlobalConfig.UIOBJECTS_PATH + "LoadLevelTransactionScreen.res").instance()
	$HUD.add_child(fade_out)
	$HUD.sprites_animation.play("hide")
	fade_out.start()
	fade_out.connect("callback", self, "_on_open", [id + 1])
	set_process(false)
	set_physics_process(false)

func _on_Pointer_open_bonus(id : int):
	var fade_out = load(GlobalConfig.UIOBJECTS_PATH + "LoadLevelTransactionScreen.res").instance()
	$HUD.add_child(fade_out)
	$HUD.sprites_animation.play("hide")
	fade_out.start()
	fade_out.connect("callback", self, "_on_open_bonus", [id + 1])
	set_process(false)
	set_physics_process(false)

func _get_lamps_of_enabled_levels() -> Array:
	var r = []
	var enabled = level_info.get_enabled_zones()
	for lamp in GroupFinder.find_nodes_by_name(self, "Lamp"):
		var id = level_info.get_level_id(lamp.get_name())
		if enabled[id] == true:
			r.append(lamp)
	return r

func _get_all_buildings() -> Array:
	return GroupFinder.find_nodes_by_name(self, "Level0")

func _get_all_bonus_buildings() -> Array:
	return GroupFinder.find_nodes_by_name(self, "Bonus0")

func _get_buildings_of_enabled_levels() -> Array:
	var r = []
	for building in _get_all_buildings():
		var id : int = level_info.get_level_id(building.get_name())
		if not level_info.is_locked_level(id):
			r.append(building)
	return r

func _on_Pointer_show_level_name(id):
	var data = level_info.get_level_data(id)
	hud.show_level_name(id, data)

func _on_Pointer_show_level_details(id : int):
	var data = level_info.get_level_data(id)
	hud.show_level_details(id, data)

func _on_Pointer_close_level_name():
	hud.hide_level_name()

func _on_Pointer_close_level_details():
	hud.hide_level_details()

func _on_Pointer_show_bonus_name(id):
	var data = bonus_info.get_bonus_data(id)
	hud.show_level_name(id, data)

func _on_Pointer_show_bonus_details(id : int):
	var data = bonus_info.get_bonus_data(id)
	hud.show_level_details(id, data)

func _on_Pointer_close_bonus_details():
	hud.hide_level_name()

func _on_Pointer_close_bonus_name():
	hud.hide_level_details()

