tool

extends Spatial

class_name SkyWorld

onready var timer = $Timer

onready var target_rotation_speed  : Vector3
onready var current_rotation_speed : Vector3

func _update_rotation_speed():
	target_rotation_speed = Vector3(
			randf(),
			randf(),
			randf()
			) * rand_range(.01, .1)

func _randomize_timer():
	timer.wait_time = rand_range(2, 20)

func _ready():
	_update_rotation_speed()
	current_rotation_speed = target_rotation_speed

func _physics_process(delta : float):
	current_rotation_speed = lerp(current_rotation_speed, target_rotation_speed, delta * .5)

	rotation += current_rotation_speed * delta

func _on_Timer_timeout():
	_update_rotation_speed()

