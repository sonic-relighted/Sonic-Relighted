extends Light

func _process(delta):
	var target = Instances.camera
	if target == null or not is_instance_valid(target): return
	if randf() > .75:
		_autoenable_light(target)

func _autoenable_light(target):
	var diff = global_transform.origin - target.global_transform.origin
	var distance = diff.length()
	
	if distance > 15:
		hide()
	else:
		show()
