extends ScrollContainer

class_name ScrollContainerFocusScroll

func _ready():
	for child in get_child(2).get_children():
		child.connect('focus_entered', self, '_on_focus_change')

func _on_focus_change():
	var focused = get_focus_owner()
	var focus_offset = focused.rect_position.y

	var scrolled_top = get_v_scroll()
	var scrolled_bottom = scrolled_top + get_size().y - focused.get_size().y

	if focus_offset < scrolled_top or focus_offset >= scrolled_bottom:
		set_v_scroll(focus_offset)
