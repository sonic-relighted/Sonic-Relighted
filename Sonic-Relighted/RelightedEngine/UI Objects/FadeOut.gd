extends ColorRect

class_name FadeOut

signal fadeout_completed

export(float) var time : float = 1.0
export(bool) var auto_destroy : bool = false
export(Color) var custom_color : Color = Color.black

onready var opacity : float = .0

func _ready():
	color = Color(custom_color.r, custom_color.g, custom_color.b, .0)

func _process(delta):
	_update_opacity(delta)
	if opacity == 1.0:
		color = Color(custom_color.r, custom_color.g, custom_color.b, opacity)
		emit_signal("fadeout_completed")
		if auto_destroy: queue_free()
		set_process(false)
	else:
		color = Color(custom_color.r, custom_color.g, custom_color.b, opacity)

func _update_opacity(delta):
	if opacity < 1.0: opacity += delta / time
	if opacity >= 1.0: opacity = 1.0
