extends Button

class_name TransactionButton

export(NodePath) var host : NodePath = ".."
export(float) var fadeout_time : float = .1

signal fadeout_completed

onready var host_instance : Node2D = get_node(host)

func _on_FadeoutButton_pressed():
#	var res = load(GlobalConfig.UIOBJECTS_PATH + "FadeOut.res")
#	var node = res.instance()
#	node.time = fadeout_time
#	node.connect("fadeout_completed", self, "_fadeout_completed")
#	host_instance.add_child(node)
	var res = load(GlobalConfig.UIOBJECTS_PATH + "TransactionScreen.res")
	var node = res.instance()
#	node.time = fadeout_time
	node.connect("callback",
			self,
			"emit_signal",
			["fadeout_completed"])
	host_instance.get_parent().get_parent().add_child(node)

