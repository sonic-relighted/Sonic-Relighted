extends CanvasLayer

class_name PauseScreen

enum ACTIONS_ACPT_CNCL {
	RESTART,
	QUIT
}

var stage_music : OstPlayer
var music_position : float
var action_acpt_cncl : int

func _ready():
#	music_position = stage_music.get_playback_position()
#	stage_music.stop()
	get_tree().paused = true

func _on_ResumeMenuButton_clicked():
#	stage_music.play(music_position)
	get_tree().paused = false
	queue_free()

func _on_RestartMenuButton_clicked():
	_open_confirm()
	action_acpt_cncl = ACTIONS_ACPT_CNCL.RESTART

func _on_ExitMenuButton_clicked():
	_open_confirm()
	action_acpt_cncl = ACTIONS_ACPT_CNCL.QUIT

func _open_confirm():
	var confirm : Confirm = load(GlobalConfig.UIOBJECTS_PATH + "Menu/Confirm.res").instance()
	confirm.connect("accept", self, "_on_confirm_accept")
	confirm.connect("cancel", self, "_on_confirm_cancel")
	get_child(0).add_child(confirm)
	if action_acpt_cncl == ACTIONS_ACPT_CNCL.QUIT:
		confirm.set_message(Confirm.Message.EXIT_LEVEL)
	else:
		confirm.set_message(Confirm.Message.RETRY_LEVEL)
	_set_buttons_enabled(false)

func _on_confirm_cancel():
	_set_buttons_enabled(true)

func _on_confirm_accept():
	get_tree().paused = false
	if action_acpt_cncl == ACTIONS_ACPT_CNCL.QUIT:
		ProgressSingletone.return_to_menu_from_ingame = true
		get_tree().change_scene(GlobalConfig.SCENES_PATH + "menu.tscn")
	else:
		ProgressSingletone.game_slot_data.shield = ProgressSingletone.active_shield
		get_tree().reload_current_scene()

func _set_buttons_enabled(new_state : bool):
	$Menu/PrincipalFocusManager.enabled = new_state
	$Menu/ResumeMenuButton.enabled = new_state
	$Menu/ResumeMenuButton.focused = false
	if is_instance_valid($Menu/RestartMenuButton):
		$Menu/RestartMenuButton.enabled = new_state
		$Menu/RestartMenuButton.focused = false
	$Menu/ExitMenuButton.enabled = new_state
	$Menu/ExitMenuButton.focused = false
	if new_state:
		$Menu/PrincipalFocusManager.current_control = $Menu/ResumeMenuButton
		$Menu/PrincipalFocusManager.current_control.focused = true
