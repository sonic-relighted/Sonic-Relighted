extends CanvasLayer

class_name BonusEnd

enum Type {
	TIMEOUT,
	DEATH,
	VOLUNTARILY
}

onready var black : ColorRect = $BlackTexture
onready var opacity : float = .0

var stage_music : OstPlayer
var type : int

func _ready():
	Instances.hud.set_process(false)
	stage_music.stop_with_fade_out()
	
	$Message/Timeout.visible = type == Type.TIMEOUT
	$Message/Death.visible  = type == Type.DEATH
	
	$Message/Timeout.set_text(tr("BONUS.timeout"))
	$Message/Death.set_text(tr("BONUS.death"))

	$SlotDataManager.save_data(ProgressSingletone.game_slot_id, ProgressSingletone.game_slot_data)
	Instances.camera.disable()

func _process(delta):
	black.color = Color(0, 0, 0, opacity)
	opacity = opacity + 1 * delta

func _on_AnimationPlayer_animation_finished(anim_name):
	get_tree().change_scene(GlobalConfig.ZONESELECT_PATH + "World.tscn")
