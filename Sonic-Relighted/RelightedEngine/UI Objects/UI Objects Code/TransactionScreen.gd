extends Control

signal callback

export(bool) var auto_destroy : bool = true

func _ready():
	set_process(false)
	set_physics_process(false)

func start():
	$Animator.play("T")

func _emit_callback():
	emit_signal("callback")

func _on_Animator_animation_finished(anim_name : String):
	_emit_callback()
	queue_free()
