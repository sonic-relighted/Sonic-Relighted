extends CanvasLayer

var stage_music

onready var black : ColorRect = $BlackTexture

onready var opacity : float = 0

func _ready():
#	stage_music.stop()
	Instances.hud.set_process(false)
	ProgressSingletone.game_slot_data.lifes = ProgressSingletone.INITIAL_LIFES
	$SlotDataManager.save_data(ProgressSingletone.game_slot_id, ProgressSingletone.game_slot_data)
	Instances.camera.disable()

func _process(delta):
	black.color = Color(0, 0, 0, opacity)
	opacity = opacity + 1 * delta

func _on_DeathAnimTimer_timeout():
	if GlobalConfig.DEMO:
		_demo()
		return
	get_tree().change_scene(GlobalConfig.ZONESELECT_PATH + "World.tscn")

func _demo():
	get_tree().paused = false
	get_tree().change_scene(GlobalConfig.SCENES_PATH + "DemoScreen.tscn")
