extends CanvasLayer

var stage_music

onready var black   : ColorRect = $BlackTexture

onready var opacity : float     = 0
onready var timer   : float     = 0

func _ready():
#	stage_music.stop()
	Instances.hud.set_process(false)
	$SlotDataManager.save_data(ProgressSingletone.game_slot_id, ProgressSingletone.game_slot_data)
	Instances.camera.disable()

func _process(delta):
	black.color = Color(0, 0, 0, opacity)
	if timer > .5: opacity = opacity + 1 * delta
	
#	if lifes.anchor_left < .5: lifes.anchor_left += .1
#	else: lifes.anchor_left = .5
#
#	if lifes.anchor_top > .5: lifes.anchor_top -= .1
#	else: lifes.anchor_top = .5
	
	
	if timer > .5: Instances.hud.lifes.text = str(ProgressSingletone.game_slot_data.lifes)

	timer += delta

func _on_DeathAnimTimer_timeout():
	get_tree().paused = false
	for obj in get_tree().get_nodes_in_group("Resetable"):
		if obj.has_method("reset"):
			obj.reset()
	Instances.player.reset()
	Instances.camera.enable()
	ProgressSingletone.ingame_devices = 0
	for device in GroupFinder.find_nodes_in_group(Instances.level, "Device"):
		if device.enabled == false:
			ProgressSingletone.ingame_devices += 1
	Instances.hud.update_all()
	Instances.hud.set_process(true)
	get_parent().add_child(load(GlobalConfig.UIOBJECTS_PATH + "FadeIn.res").instance())
	queue_free()
