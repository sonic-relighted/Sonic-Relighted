extends ColorRect

class_name FadeIn

export(float) var time : float = 1.0
export(Color) var custom_color : Color = Color.black

onready var opacity : float = 1.0

func _ready():
	color = Color(custom_color.r, custom_color.g, custom_color.b, 1.0)

func _process(delta):
	_update_opacity(delta)
	if opacity == .0:
		queue_free()
	color = Color(custom_color.r, custom_color.g, custom_color.b, opacity)

func _update_opacity(delta):
	if opacity > .0: opacity -= delta / time
	if opacity <= .0: opacity = .0
