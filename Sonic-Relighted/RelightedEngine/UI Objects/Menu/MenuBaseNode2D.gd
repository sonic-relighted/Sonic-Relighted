extends Node

class_name MenuBaseNode2D

onready var host : Node2D = get_parent()

func _ready():
	var rect_size = OS.get_screen_size()
	host.scale = Vector2.ONE / 3 / (1 + SettingsData.data.resolution)
	host.scale.x *= rect_size.x / 640
	host.scale.y *= rect_size.y / 360
	host.scale.x *= OS.window_size.x / rect_size.x
	host.scale.y *= OS.window_size.y / rect_size.y
	host.scale *= 3 + 0.01
#	scale = Vector2.ONE
