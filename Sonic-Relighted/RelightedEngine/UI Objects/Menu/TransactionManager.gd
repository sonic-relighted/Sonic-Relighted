extends Node

onready var canvas_layer = get_parent().get_parent().canvas_layer

func go_to(callback : String):
	var trns = load(GlobalConfig.UIOBJECTS_PATH + "TransactionScreen.res").instance()
	trns.connect("callback", get_parent(), "emit_signal", [callback])
	canvas_layer.add_child(trns)
