tool
extends Node2D

class_name Confirm

enum Message {
	EXIT,
	APPLY_AND_RESTART,
	DELETE,
	RETRY_LEVEL,
	EXIT_LEVEL
}

signal accept
signal cancel

export(Message) var message : int = Message.EXIT setget set_message, get_message

onready var accept_button = $Box/AcceptMenuButton
onready var cancel_button = $Box/CancelMenuButton

func set_message(new_message : int):
	message = new_message
	if $Box/Content:
		$Box/Content.region_rect.position.y = message * 50.0

func get_message() -> int: return message

func only_accept_button():
	accept_button.position = cancel_button.position
	accept_button.focused = true
	accept_button.near_right = ""
	cancel_button.queue_free()

func _on_AcceptMenuButton_clicked():
	_close()
	yield(get_tree(), "idle_frame")
	emit_signal("accept")

func _on_CancelMenuButton_clicked():
	_close()
	yield(get_tree(), "idle_frame")
	emit_signal("cancel")

func _close():
	$Box/AcceptMenuButton.enabled = false
	if is_instance_valid($Box/CancelMenuButton):
		$Box/CancelMenuButton.enabled = false
	$ConfirmFocusManager.queue_free()
	$VisibilityTween.interpolate_property(
			$Box, "scale",
			Vector2.ONE, Vector2.ZERO, .15,
			Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$VisibilityTween.start()
	yield($VisibilityTween, "tween_all_completed")
	queue_free()

func _apply_language(lang : String):
	for spr in GroupFinder.find_nodes_in_group(self, "MultiLang"):
		var path = GlobalConfig.SCENES_PATH + "Menu/Popup/Popup-" + lang + ".png"
		spr.texture = load(path)

func _ready():
	_apply_language(SettingsData.data.language)
	$VisibilityTween.interpolate_property(
			$Box, "scale",
			Vector2.ZERO, Vector2.ONE, .5,
			Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	$VisibilityTween.start()
