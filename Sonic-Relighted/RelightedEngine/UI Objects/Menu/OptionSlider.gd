extends Node2D

signal accept
signal cancel
signal change

class_name OptionSlider

export(int,0,9) var max_values : int = 9
export(int) var value : int = 0

onready var enabled : bool = false
onready var default_value : int = value
onready var items = [
		$Items/Item1, $Items/Item2, $Items/Item3, $Items/Item4,
		$Items/Item5, $Items/Item6, $Items/Item7, $Items/Item8,
		$Items/Item9, $Items/Item10]

func enable():
	show()
	_update_items()
	enabled = true

func disable():
	hide()
	enabled = false

func _ready():
	disable()
	for i in range(items.size()):
		var item = items[i]
		item.connect("clicked", self, "_on_click_item", [i])

func _input(event):
	if not enabled: return
	
	if event is InputEventKey \
	or event is InputEventJoypadButton:
		if event.is_action_released("ui_right"):
			value += 1
			if value > max_values: value = max_values
			emit_signal("change", value)
		elif event.is_action_released("ui_left"):
			value -= 1
			if value < 0: value = 0
			emit_signal("change", value)
		elif event.is_action_released("ui_accept"):
			items[value].set_process(true)
			items[value].modulate_position = items[value].hilight
		elif event.is_action_released("ui_cancel"):
			value = default_value
			emit_signal("cancel")
		_update_items()

func _update_items():
	for i in range(items.size()):
		items[i].modulate.a = 1 if i <= value else .2

	for i in range(max_values+1, items.size()):
		items[i].hide()

func _on_click_item(item_id):
	if not enabled: return
	value = item_id
	_update_items()
	emit_signal("change", value)
	emit_signal("accept", value)


func _on_OptionSlider_draw():
	$OpenManimationPlayer.play("open")
