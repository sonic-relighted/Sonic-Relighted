tool

extends Area2D

class_name ToggleMenuArea2DButton

signal clicked

export(NodePath) var near_up      : NodePath
export(NodePath) var near_left    : NodePath
export(NodePath) var near_right   : NodePath
export(NodePath) var near_bottom  : NodePath

export(bool)    var pressed       : bool  = false
export(bool)    var enabled       : bool  = true
export(bool)    var omnipresent   : bool  = false
export(bool)    var emit_instant  : bool  = true
export(float)   var hilight       : float = 3.0
export(float)   var hilight_speed : float = 2.0
export(String)  var value         : String = "play"
export(Vector2) var collider_size : Vector2 = Vector2(1, 1) setget set_collider_size,get_collider_size

onready var modulate_position : float = 0.0
onready var focused           : bool  = false setget set_focused,get_focused
onready var animator          : AnimationPlayer = $ClickAnimationPlayer

func set_collider_size(new_size):
	collider_size = new_size
	if Engine.editor_hint: $CollisionShape2D.scale = new_size

func get_collider_size():
	return collider_size

func set_focused(new_focused):
	focused = new_focused
	if focused:
		modulate = Color.yellow
	else:
		modulate = Color.white

func get_focused():
	return focused

func _ready():
	if not Engine.editor_hint:
		$CollisionShape2D.scale = collider_size
	set_process(false)
	_on_mouse_exited()

func _input_event(viewport, event, shape_idx):
	if Engine.editor_hint: return
	if not enabled: return
	if not omnipresent:
		_do_input(event)

func _input(event):
	if Engine.editor_hint: return
	if not enabled: return
	if omnipresent:
		_do_input(event)
	_do_key_accept(event)

func _process(delta : float):
	if Engine.editor_hint: return
	if modulate_position < 1.0:
		modulate_position = 1.0
		set_process(false)
		if not emit_instant: emit_signal("clicked", pressed)
	modulate_position -= delta * hilight_speed
	modulate = Color.white * modulate_position

func _do_input(event):
	if enabled and not event.is_pressed():
		if event is InputEventJoypadButton \
		or event is InputEventKey \
		or event is InputEventScreenTouch \
		or event is InputEventMouseButton:
			pressed = !pressed
			_on_mouse_entered()
			if emit_instant: emit_signal("clicked", pressed)
			animator.play("click")
			set_process(true)
			modulate_position = hilight

func _do_key_accept(event):
	if enabled and focused and not event.is_pressed():
		if event.is_action_released("ui_accept"):
			pressed = !pressed
			_on_mouse_entered()
			if emit_instant: emit_signal("clicked", pressed)
			animator.play("click")
			set_process(true)
			modulate_position = hilight

func _on_mouse_entered():
	if Engine.editor_hint: return
	if not enabled: return
	var img = tr("LANGUAGE.id") + "/" + value
	if pressed: img += "_ok"
	var res = GlobalConfig.UIOBJECTS_PATH + "Menu/" + img + "2.png"
	$Button.texture = load(res)

func _on_mouse_exited():
	if Engine.editor_hint: return
	var img = tr("LANGUAGE.id") + "/" + value
	if pressed: img += "_ok"
	var res = GlobalConfig.UIOBJECTS_PATH + "Menu/" + img + ".png"
	$Button.texture = load(res)
