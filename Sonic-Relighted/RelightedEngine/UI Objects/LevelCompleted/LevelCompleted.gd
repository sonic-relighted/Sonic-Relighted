extends CanvasLayer

var stage_music : OstPlayer

onready var timer    : Timer = $Timer
onready var animator : AnimationPlayer = $AnimationPlayer

func _ready():
#	ProgressSingletone.ingame_rings = 500
#	Instances.hud.current_time = 500.5
	var player = Instances.player
	player.input_manager.locked = true
	player.state_machine.current_state.exit(player)
	player.state_machine.current_state = player.state_machine.victory_state
	player.state_machine.current_state.enter(player)
	
	Instances.camera.level_completed_manager.activate()
	stage_music.stop()
	Instances.hud.set_process(false)

	animator.play("Show")
	$SlotDataManager.save_data(ProgressSingletone.game_slot_id, ProgressSingletone.game_slot_data)
	
	$HideHudTween.interpolate_property(
			Instances.hud.container,
			"modulate:a",
			1.0, .0, 1.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT
	)
	$HideHudTween.interpolate_property(
			Instances.hud.controller,
			"modulate:a",
			1.0, .0, 1.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT
	)
	$HideHudTween.start()
	var seconds = ProgressSingletone.ingame_seconds
	if seconds < 150: $Node2D/RankValue.rank = 0
	elif seconds < 180: $Node2D/RankValue.rank = 1
	elif seconds < 220: $Node2D/RankValue.rank = 2
	elif seconds < 290: $Node2D/RankValue.rank = 3
	elif seconds < 300: $Node2D/RankValue.rank = 4
	else: $Node2D/RankValue.rank = 5

func _physics_process(delta):
	if Input.is_action_just_pressed("menu"):
		_start_transition()

func _on_Timer_timeout():
	_start_transition()

func _start_transition():
	animator.play("end")

func _end_transition():
	timer.stop()
	set_physics_process(false)
	if GlobalConfig.DEMO:
		_demo()
		return
	LevelsCutscenesManager.close_level(ProgressSingletone.playing_level_id)

func _start_time_count():
	$CounterSfx.play()
	$CounterTween.interpolate_method(
			self,
			"_count_time",
			.0,
			Instances.hud.current_time,
			Instances.hud.current_time * .003,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$CounterTween.start()
	yield($CounterTween, "tween_all_completed")
	_start_rings_count()

func _start_rings_count():
	$CounterTween.interpolate_method(
			self,
			"_count_rings",
			.0,
			ProgressSingletone.ingame_rings,
			ProgressSingletone.ingame_rings * .01,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$CounterTween.start()
	yield($CounterTween, "tween_all_completed")
	$CounterSfx.stop()
	animator.play("Rank")
	timer.start()
	if $Node2D/RankValue.rank == $Node2D/RankValue.Ranks.E:
		yield(animator, "animation_finished")
		animator.play("RankBreak")

func _count_rings(number : int):
	$Node2D/Rings/Value.set_text(
			str(number))

func _count_time(number : float):
	$Node2D/Time/Value.set_text(
		Formatter.time_seconds_to_timestamp_with_decimals(
				number))

func _demo():
	get_tree().paused = false
	ProgressSingletone.playing_bonus_id = 1
	get_tree().change_scene(GlobalConfig.SCENES_PATH + "BonusStageIngame.tscn")
