tool
extends Sprite

enum Ranks {
	S, A, B,
	C, D, E
}
const POSITIONS = [
	Vector2(40, 18), Vector2(64, 18), Vector2(88, 18),
	Vector2(40, 42), Vector2(64, 42), Vector2(88, 42)
]

export(Ranks) var rank : int = Ranks.E setget set_rank, get_rank

func set_rank(new_rank : int):
	rank = new_rank
	region_rect.position = POSITIONS[rank]

func get_rank() -> int: return rank
