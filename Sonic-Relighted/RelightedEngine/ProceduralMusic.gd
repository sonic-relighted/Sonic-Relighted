extends Node

export(bool)  var auto_play     : bool  = true
export(int)   var current       : int   = 0
export(float) var tempo         : float = 120.0
export(float) var beats_per_bar : int = 4

onready var songs                 : Array = GroupFinder.find_nodes_by_type(self, AudioStreamPlayer)
onready var last = current
onready var next = null
onready var current_song_instance : AudioStreamPlayer = songs[current]
onready var last_song_instance    : AudioStreamPlayer = songs[last]
onready var seconds_per_beat      : float = tempo / (60 * 4)

onready var metronome = $Metronome

const MIN_VALUE : float = -80.0
const MAX_VALUE : float = 0.0

var speed

func _ready():
	for song in songs:
		song.volume_db = MIN_VALUE
		song.play()
		song.bus = "Music"
	set_process(false)
	if auto_play:
		change_to(current, 1)
	metronome.wait_time = seconds_per_beat
	metronome.start()
	_on_Metronome_timeout()

func change_to(song_id : int, fade_speed : float):
	next = {
		"song": song_id,
		"speed": fade_speed
	}

func _process(delta):
	var current_is_ok = false
	var last_is_ok = false
	var incr : float = delta * speed
	if current_song_instance.volume_db < MAX_VALUE:
		var db : float = db2linear(current_song_instance.volume_db) + incr
		if db > .99: current_song_instance.volume_db = MAX_VALUE
		else: current_song_instance.volume_db = linear2db(db)
	else:
		current_song_instance.volume_db = MAX_VALUE
		current_is_ok = true

	if last == current:
		last_is_ok = true
	else:
		if last_song_instance.volume_db > MIN_VALUE:
			var db : float = db2linear(last_song_instance.volume_db) - incr
			if db < .05: last_song_instance.volume_db = MIN_VALUE
			else: last_song_instance.volume_db = linear2db(db)
		else:
			last_song_instance.volume_db = MIN_VALUE
			last_is_ok = true

	if current_is_ok and last_is_ok:
		set_process(false)

func _on_Metronome_timeout():
	if is_processing(): return
	if next == null: return
	last = current
	current = next.song
	speed = next.speed
	current_song_instance = songs[current]
	last_song_instance = songs[last]
	next = null
	set_process(true)
