extends Spatial

onready var enableable_area : Resource = load(
	GlobalConfig.INGAME_ELEMENTS_PATH + "Interactuable/EnableableArea/EnableableArea.res")

func _ready():
	if enableable_area == null: print("ERROR, no existe EnableableArea")
	for node in GroupFinder.find_nodes_by_type(self, StaticBody):
		_inject(node)

func _inject(node : Node):
	_create_enableable(node)

func _create_enableable(node : Node):
	var enableable : EnableableArea = enableable_area.instance()
	enableable.manage_collision_layer = true
	enableable.manage_collision_mask = true
	enableable.manage_physics_process = true
	enableable.manage_process = true
	node.add_child(enableable)
 
