#
# Eso activa y desactiva segun la distancia con el jugador
# @deprecated
#

extends Spatial

class_name LiveObjects

export(float) var distance : float = 14.0
export(bool) var reset : bool = true # resetea posiciones y estados de los objetos

onready var is_enabled : bool = false

var player : Player

func _ready():
	set_process(false)
	set_physics_process(false)
	_disable_all()

func _process(delta):
	var length = (Instances.player.translation - translation).length()
	if is_enabled:
		if length > distance:
			_disable_all()
	else:
		if length < distance:
			_enable_all()

func _disable_all():
	is_enabled = false
	for child in get_children():
		child.set_process(false)
		child.set_physics_process(false)
		if reset \
				and child.is_in_group("Resetable") \
				and child.state_machine.current_state != child.state_machine.death_state:
			child.reset()

func _enable_all():
	is_enabled = true
	for child in get_children():
		child.set_process(true)
		child.set_physics_process(true)
