extends Spatial

signal prize
signal reset

signal enabled
signal disabled

export(Material) var material_led : Material

export(int)  var rings_needed : int = 20
export(int)  var rings_if_discard : int = 25
export(bool) var enabled      : bool = false setget set_enabled,get_enabled

onready var ruleta_click_sfx : AudioStreamPlayer = $RuletaClickSfx
onready var error_sfx        : AudioStreamPlayer = $ErrorSfx
onready var discard_sfx      : AudioStreamPlayer = $DiscardSfx
onready var no_prize_sfx     : AudioStreamPlayer = $NoPrizeSfx
onready var prize_sfx        : AudioStreamPlayer = $PrizeSfx
onready var ruleta           : RotatingPlatform  = $Ruleta
onready var sensor = $Sensor
onready var prize_mechanism = $PrizeMecanismo

onready var buttons = [ $RuletaButton, $RuletaButton2, $RuletaButton3, $RuletaButton4]
onready var arrows  = [ $Arrow, $Arrow2, $Arrow3, $Arrow4 ]

onready var current_active_button : int = 0

func set_enabled(new_enabled : bool):
	enabled = new_enabled
	if enabled:
		if ProgressSingletone.ingame_rings >= rings_needed:
			_on_PushButton_pressed()
			emit_signal("enabled")
			emit_signal("reset")
		else:
			_show_rings_needed()
			enabled = false
			emit_signal("disabled")
	else:
		emit_signal("disabled")

func get_enabled() -> bool: return enabled

func _on_Ruleta_stopped():
	set_enabled(false)
	_activate_button(randi() % 4)
	if sensor.current == null:
		no_prize_sfx.play()
	else:
		emit_signal("prize", sensor.current.item_id)
		prize_sfx.play()

func _show_rings_needed():
	Instances.hud.show_rings_needed(rings_needed)
	error_sfx.play()

func _on_PushButton_pressed():
	var time : float = rand_range(2.0, 6.0)
	var speed : float = rand_range(2.0, 5.0)
	var accel : float = rand_range(.5, 1.0)
	ruleta.program = '[' \
			+ '{"time": ' + str(time) + ','\
			+ ' "speed": ' + str(speed) + '}'\
			+ ']'
	ruleta.acceleration = accel
	ruleta.start()
	ProgressSingletone.ingame_rings -= rings_needed
	ProgressSingletone.ingame_rings_to_life += rings_needed
	Instances.hud.update_rings()
	_activate_button(-1)

func _activate_button(button : int):
	current_active_button = button
	for arrow in arrows:
		arrow.set_enabled(false)
	for button in buttons:
		button.enabled = false
	if current_active_button != -1:
		arrows[current_active_button].set_enabled(true)
		buttons[current_active_button].enabled = true
