extends Area

export(int) var item_id : int

func set_enabled(new_enabled : bool):
	$CollisionShape.disabled = !new_enabled

func _ready():
	add_to_group("RuletaItem")
	add_to_group("RuletaArea")
