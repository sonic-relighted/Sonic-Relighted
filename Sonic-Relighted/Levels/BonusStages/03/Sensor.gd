extends Area

export(Material) var neon_material : Material
export(float)    var energy_on     : float = 1.2
export(float)    var energy_off    : float = 0.2
export(Color)    var color_on      : Color = Color(.5, 1.0, .0)
export(Color)    var color_off     : Color = Color(.25, .5, .0)

onready var host  = get_parent()
onready var tween = $MaterialChangeTween
onready var current = null

func _on_Sensor_area_entered(area):
	current = area
	host.ruleta_click_sfx.play()

func _on_Sensor_area_exited(area):
	current = null

func _ready():
	add_to_group("RuletaSensor")
	add_to_group("RuletaArea")
