extends Spatial

export(bool) var enabled : bool setget set_enabled, get_enabled

onready var light_time : float = .0

var materials : Array

func set_enabled(new_enabled : bool):
	enabled = new_enabled
	for m in materials:
		m.emission_enabled = enabled
	set_physics_process(enabled)

func get_enabled() -> bool: return enabled

func _physics_process(delta : float):
	light_time += delta
	for m in materials:
		m.emission_energy = (sin(light_time * 4.0) + 1.0) * .5 + .1

func _ready():
	materials = []
	for mesh in GroupFinder.find_nodes_by_type(self, MeshInstance):
		var ins : MeshInstance = mesh
		var m = ins.mesh.surface_get_material(0).duplicate()
		ins.set_surface_material(0, m)
		materials.append(m)
	set_enabled(enabled)
