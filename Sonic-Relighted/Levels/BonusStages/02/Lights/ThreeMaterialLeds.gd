extends Node

export(Material) var leds_mat1 : Material
export(Material) var leds_mat2 : Material
export(Material) var leds_mat3 : Material
export(float) var emission_energy_low : float = .05
export(float) var emission_energy_mid : float = .1
export(float) var emission_energy_hi  : float = 1.0
export(float) var time                : float = 480 # 125bpm
export(int)   var sync_time           : float = 15

onready var initial_time : int = OS.get_ticks_msec()
onready var last_time    : float = .0
onready var mats = [leds_mat1, leds_mat2, leds_mat3]

func _physics_process(delta : float):
	var t = OS.get_ticks_msec() - initial_time + sync_time
	if t >= last_time:
		last_time += time
		_change()

func _change():
	mats[0].emission_energy = emission_energy_low
	mats[1].emission_energy = emission_energy_mid
	mats[2].emission_energy = emission_energy_hi

	if mats[0] == leds_mat1:
		mats = [leds_mat2, leds_mat3, leds_mat1]
	elif mats[0] == leds_mat2:
		mats = [leds_mat3, leds_mat1, leds_mat2]
	else:
		mats = [leds_mat1, leds_mat2, leds_mat3]
