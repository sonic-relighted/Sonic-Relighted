extends Area

signal prize

onready var collishape : CollisionShape = $CollisionShape
onready var disabled : bool = false

func _on_Target_body_entered(body):
	if not disabled and body.is_in_group("Player"):
		_hit(body)
		disabled = true
		yield(get_tree().create_timer(1.0), "timeout")
		disabled = false

func _hit(player : Player):
	var diff = global_transform.origin - player.global_transform.origin
	var prize = int((diff.length() * 6) / collishape.shape.radius + 1)
	emit_signal("prize", prize)
	player.velocity = Vector3.DOWN
	player.current_speed = .0
