extends StaticBody

signal enabled
signal disabled

export(bool)  var enabled : bool
export(float) var height  : float = .8

onready var tween      = $Tween
onready var collishape = $CollisionShape
onready var initial_y  = global_transform.origin.y

func set_enabled(new_enabled : bool):
	enabled = new_enabled
	if enabled:
		_move(initial_y - height, initial_y)
	else:
		_move(initial_y, initial_y - height)

func get_enabled() -> bool: return enabled

func _move(from : float, to : float):
	tween.interpolate_property(self, "global_transform:origin:y", from, to, 1.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()

func _on_Tween_tween_all_completed():
	if initial_y == global_transform.origin.y:
		emit_signal("enabled")
#		collishape.disabled = false
		collision_mask = 1
		collision_layer = 1
	else:
		emit_signal("disabled")
#		collishape.disabled = true
		collision_mask = 0
		collision_layer = 0

func _ready():
	enabled = true
	collision_mask = 1
	collision_layer = 1
