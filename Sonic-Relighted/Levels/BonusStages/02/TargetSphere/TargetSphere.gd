extends Spatial

signal countdown
signal moved

export(int)   var time_change : int   = 8.0
export(float) var radius      : float = 16.0
export(float) var min_angle   : float = AngleConstants.QUART_PI
export(float) var max_angle   : float = AngleConstants.QUART_PI

onready var fade_sfx  : AudioStreamPlayer = $FadeInOutSfx
onready var timer     : Timer = $Timer
onready var tween     : Tween = $Tween
onready var countdown : int   = time_change

const X = Vector3(1.0, .0, .0)
const Y = Vector3(.0, 1.0, .0)

func _random_position() -> Vector3:
	return Vector3.UP\
			.rotated(X, rand_range(min_angle, max_angle))\
			.rotated(Y, rand_range(-PI, PI))\
			.normalized() * radius

func _change():
	var center = global_transform.origin
	for child in get_children():
		if child is Area:
			var pos = _random_position() + center
			child.global_transform.origin = pos
			child.look_at(center, Vector3.UP)
	emit_signal("moved")

func _on_Timer_timeout():
	countdown -= 1
	emit_signal("countdown", countdown)
	if countdown < 1:
		_change()
		countdown = time_change
		_interpolate_alpha(0.0, 1.0)
	elif countdown == 1:
		_interpolate_alpha(1.0, 0.0)
		fade_sfx.play()

func _interpolate_alpha(from : float, to : float):
	for mesh in GroupFinder.find_nodes_by_type(self, CSGMesh):
		tween.interpolate_property(mesh.material, "albedo_color:a", from, to, .75, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()

func _ready():
	_change()
	timer.start()
