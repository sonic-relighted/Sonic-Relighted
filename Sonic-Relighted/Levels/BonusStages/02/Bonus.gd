extends AbstractLevel

onready var cannon        = $Scene/Cannon

onready var floor_hit_sfx = $FloorHitSfx

func _on_TargetSphere_countdown(countdown):
	if countdown < 6:
		print(countdown)

func _on_SpikesMovingPlatform_at_begin():
	Instances.camera.direction_hit_manager.hit(Vector3.DOWN * .5, 4.0)
	floor_hit_sfx.play()

func _on_player_throw_end():
	Instances.player.velocity = Vector3.DOWN
	Instances.player.current_speed = .0

func _ready():
	yield(get_tree(), "idle_frame")
	Instances.player.state_machine.cannon_throw_state.connect("end", self, "_on_player_throw_end")
