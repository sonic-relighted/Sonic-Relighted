extends Spatial

export(float) var radius : float = 2.0

onready var prize_sfx : AudioStreamPlayer = $PrizeSfx

onready var n : float = .0

func _on_Target_prize(prize : int):
	prize_sfx.play()
	var position = Vector3(sin(n) * radius, .0, cos(n) * radius)
	match prize:
		1: _build_monitor(position, 2) # life
		2: _build_monitor(position, 5) # electric
		3: _build_monitor(position, 4) # fire
		4: _build_monitor(position, 7) # ice
		5: _build_monitor(position, 0) # shield
		6: _build_monitor(position, 3) # rings
	n += PI - PI / 6.0

func _build_monitor(position : Vector3, type : int):
	add_child(Builder.build_monitor(position, type))
