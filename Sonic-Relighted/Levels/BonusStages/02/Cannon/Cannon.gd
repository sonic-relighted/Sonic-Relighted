extends Spatial

signal player_entered
signal player_exited
signal shoot

export(float) var force                          : float = 30.0

export(float) var limit_left  : float = -AngleConstants.QUART_PI * .5
export(float) var limit_right : float = AngleConstants.QUART_PI * .5
export(float) var limit_up    : float = .0
export(float) var limit_down  : float = -AngleConstants.QUART_PI

export(float) var change_camera_delay            : float = .5
export(bool)  var change_camera_wait_for_player  : bool  = false
export(bool)  var camera_follow_player           : bool  = false

onready var shoot_sfx = $ShootSfx
onready var player_output = $PlayerOutput
onready var cannon_output = $StaticBody/CannonOutput
onready var camera : Camera = $StaticBody/Camera
onready var body : StaticBody = $StaticBody
onready var particles : Particles = $StaticBody/Particles
onready var player_inside : bool = false
onready var player_thrown : bool = false
onready var player : Player = null

func _enter(body : Player):
	emit_signal("player_entered")
	player_inside = true
	camera.current = true
	player = body
	player.input_manager.locked = true
	player.visible = false
	player.velocity = Vector3.ZERO
	player.global_transform.origin = player_output.global_transform.origin
	camera.follow_player = false

func _exit():
	emit_signal("player_exited")
	camera.current = false
	player_inside = false
	if player and is_instance_valid(player):
		player.visible = true
		player.input_manager.locked = false
		player = null
	camera.follow_player = false
	Instances.camera.camera.make_current()

func _shoot():
	var direction = _calculate_direction()
	player_thrown = true
	emit_signal("shoot", direction, force)
	particles.shot()
	shoot_sfx.play()
	Instances.camera.direction_hit_manager.hit(Vector3.UP * .25, 4.0)
	get_tree().create_timer(change_camera_delay).connect("timeout", self, "_on_shoot_timeout")
	if player and is_instance_valid(player):
		player.visible = true
		player.current_angle = Instances.camera.rotation.y - _get_angle()
		player.global_transform.origin = cannon_output.global_transform.origin
		player.state_machine.current_state = player.state_machine.cannon_throw_state
		player.state_machine.current_state.enter(player)
		player.velocity = direction * force
		if camera_follow_player:
			camera.follow_player = true
		if change_camera_wait_for_player:
			yield(player.state_machine.current_state, "end")
			_shoot_end()

func _get_absolute_rotation() -> Vector3:
	return rotation + body.rotation

func _get_angle():
	var absrot = _get_absolute_rotation()
	return absrot.y - AngleConstants.SEMI_PI

func _get_updown():
	var absrot = _get_absolute_rotation()
	return absrot.z

func _calculate_direction() -> Vector3:
	var dir = _get_angle()
	var updown = _get_updown()
	return AngleConstants.convert_angles(1.0, dir, updown)

func _physics_process(delta : float):
	if player_inside and not player_thrown:
		# Esto en pantalla tactil no va funcionar...
		if Input.is_action_just_pressed("roll"):
			_exit()
		elif Input.is_action_just_pressed("jump"):
			_shoot()

func _shoot_end():
	player_thrown = false
	if player and is_instance_valid(player) and player.visible:
		camera.current = false
		Instances.camera.camera.make_current()
		player_inside = false
		player.input_manager.locked = false
		player = null
	camera.follow_player = false

func _on_shoot_timeout():
	_shoot_end()

func _on_Area_body_entered(body):
	if player_inside: return
	if body.is_in_group("Player"):
		_enter(body)
