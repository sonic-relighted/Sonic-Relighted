extends Camera

export(Vector3) var offset : Vector3 = Vector3.ZERO

onready var host = get_parent().get_parent()
onready var follow_player    : bool    = false
onready var default_rotation : Vector3 = rotation

func _physics_process(delta : float):
	translation.y = \
			Instances.camera.camera.translation.y + offset.y
	if follow_player and host.player_inside:
		if host.player:
			look_at(host.player.global_transform.origin, Vector3.UP)
	else:
		rotation.x = lerp_angle(rotation.x, default_rotation.x, delta)
		rotation.y = lerp_angle(rotation.y, default_rotation.y, delta)
		rotation.z = lerp_angle(rotation.z, default_rotation.z, delta)
