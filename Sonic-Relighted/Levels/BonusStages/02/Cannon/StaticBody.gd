extends StaticBody

onready var loop_sfx  : AudioStreamPlayer = $MechanismLoopSfx

onready var host                   = get_parent()
onready var rotation_speed : Vector2 = Vector2.ZERO

func set_physics_process(enabled : bool):
	.set_physics_process(enabled)
	host.set_physics_process(enabled)
	$Area.collision_layer = 1 if enabled else 0
	$Area.collision_mask = 1 if enabled else 0

func _left(delta : float):
	if rotation.y < host.limit_right:
		rotation_speed.x = min(5.0, rotation_speed.x + delta * 2.0)
	else:
		rotation_speed.x = lerp(rotation_speed.x, .0, delta * 8.0)

func _right(delta : float):
	if rotation.y > host.limit_left:
		rotation_speed.x = max(-5.0, rotation_speed.x - delta * 2.0)
	else:
		rotation_speed.x = lerp(rotation_speed.x, .0, delta * 8.0)

func _up(delta : float):
	if rotation.z >= host.limit_down:
		rotation_speed.y = max(-5.0, rotation_speed.y - delta * 2.0)
	else:
		rotation_speed.y = lerp(rotation_speed.y, .0, delta * 8.0)

func _down(delta : float):
	if rotation.z <= host.limit_up:
		rotation_speed.y = min(5.0, rotation_speed.y + delta * 2.0)
	else:
		rotation_speed.y = lerp(rotation_speed.y, .0, delta * 8.0)

func _move(delta : float):
	rotation.y += rotation_speed.x * delta
	rotation.z += rotation_speed.y * delta

func _reset(delta : float):
	var reset_pos : Vector3 = Vector3(
			(host.limit_left + host.limit_right) / 2.0,
			.0,
			(host.limit_up + host.limit_down) / 2.0
			)
	rotation = lerp(rotation, reset_pos, delta * 4.0)

func _brake(delta : float):
	if rotation_speed.x > .0:
		rotation_speed.x = max(.0, rotation_speed.x - 1.0 * delta)
	elif rotation_speed.x < -.0:
		rotation_speed.x = min(.0, rotation_speed.x + 1.0 * delta)
	else:
		rotation_speed.x = .0
	if rotation_speed.y > .0:
		rotation_speed.y = max(.0, rotation_speed.y - 1.0 * delta)
	elif rotation_speed.y < -.0:
		rotation_speed.y = min(.0, rotation_speed.y + 1.0 * delta)
	else:
		rotation_speed.y = .0

func _sfx():
	if loop_sfx.playing:
		if rotation_speed.length() < .05:
			loop_sfx.stop()
	else:
		if rotation_speed.length() > .05:
			loop_sfx.play()
	var pitch = .1 + rotation_speed.length() * .2
	loop_sfx.pitch_scale = pitch

func _physics_process(delta : float):
	if host.player_inside:
		if Input.is_action_pressed("left"):
			_left(delta)
		elif Input.is_action_pressed("right"):
			_right(delta)
		if Input.is_action_pressed("up"):
			_up(delta)
		elif Input.is_action_pressed("down"):
			_down(delta)
	else:
		_reset(delta)
	_move(delta)
	_brake(delta)
	_sfx()

func _on_StartSfx_finished():
	loop_sfx.play()
