extends PathFollow

export(float) var distance_low : float = 1.0
export(float) var distance_hi  : float = 2.0
export(float) var time         : float = 1.0
export(bool)  var enabled      : bool = true setget set_enabled, get_enabled

onready var tween : Tween = $Tween

func set_enabled(new_enabled : bool):
	enabled = new_enabled
	if enabled: _move()

func get_enabled() -> bool: return enabled

func _move():
	if not enabled: return
	var distance : float = rand_range(distance_low, distance_hi)
	tween.interpolate_property(self, "offset", offset, offset + distance, time, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	tween.interpolate_property(self, "rotation:y", rotation.y, rotation.y + .5, time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()

func _on_Tween_tween_all_completed():
	_move()

func _ready():
	_move()
