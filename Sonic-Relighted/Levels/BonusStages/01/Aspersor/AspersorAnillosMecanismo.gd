extends Spatial

export(float) var enabled_time : float = 4.0
export(bool) var enabled : bool = true setget set_enabled, get_enabled

onready var throw_rings_sfx : AudioStreamPlayer = $ThrowRingsSfx
onready var shake_sfx       : AudioStreamPlayer = $ShakeSfx

func set_enabled(new_enabled : bool):
	enabled = new_enabled
	if $Path/AspersorPathFollow:
		$Path/AspersorPathFollow.set_enabled(enabled)
		if not enabled:
			$ShakeButtonPath.toggle_position()
	if $Path/AspersorPathFollow/Aspersor:
		$Path/AspersorPathFollow/Aspersor.set_shake(!enabled)

func get_enabled() -> bool: return enabled

func _on_ShakeSfx_finished():
	throw_rings_sfx.play()
