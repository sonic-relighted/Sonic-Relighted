extends Path

export(float) var time : float = 4.0
onready var tween       : Tween = $Tween
onready var path_follow : PathFollow = $PathFollow

func toggle_position():
	if tween.is_active(): return
	if path_follow.unit_offset < .5:
		_move(0.0, 1.0)
	else:
		_move(1.0, 0.0)

func _move(from : float, to : float):
	tween.interpolate_property(path_follow, "unit_offset", from, to, time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.interpolate_property(path_follow, "rotation:y", PI * from, PI * to, time, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()
