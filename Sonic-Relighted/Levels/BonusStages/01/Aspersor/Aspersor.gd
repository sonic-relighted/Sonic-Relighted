extends Spatial

export(NodePath) var scene     : NodePath
export(NodePath) var mechanism : NodePath

export(float)   var time_shake : float = .25
export(float)   var time_low   : float = 2.0
export(float)   var time_hi    : float = 8.0
export(int)     var rings      : int   = 10
export(bool)    var shake      : bool = false setget set_shake, get_shake
export(Vector3) var direction  : Vector3 = Vector3.UP * 8.0

onready var tween = $Tween
onready var timer = $Timer
onready var scene_instance = get_node(scene)
onready var host = get_node(mechanism)

func set_shake(new_shake : bool):
	shake = new_shake
	if shake: host.shake_sfx.play()
	else: host.shake_sfx.stop()
	if $Timer:
		$Timer.stop()
		_throw()

func get_shake() -> bool: return shake

func _throw():
	if shake:
		timer.start(time_shake)
	else:
		timer.start(rand_range(time_low, time_hi))

func _emit_ring():
	Builder.build_bouncing_ring(
				scene_instance,
				global_transform.origin,
				true, direction)
	tween.interpolate_property(self, "scale", Vector3(1.4, 1.75, 1.4), Vector3.ONE, .5, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	tween.start()

func _on_Timer_timeout():
	if not shake: host.throw_rings_sfx.play()
	for i in range(rings):
		_emit_ring()
	_throw()
	
func _ready():
	_throw()

