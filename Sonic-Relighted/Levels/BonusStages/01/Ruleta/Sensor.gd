extends Area

export(Material) var neon_material : Material
export(float)    var energy_on     : float = 1.2
export(float)    var energy_off    : float = 0.2
export(Color)    var color_on      : Color = Color(.5, 1.0, .0)
export(Color)    var color_off     : Color = Color(.25, .5, .0)

onready var host  = get_parent()
onready var tween = $MaterialChangeTween
onready var current = null

func _on_Sensor_area_entered(area):
	current = area
	_neon_on()
	host.ruleta_click_sfx.play()

func _on_Sensor_area_exited(area):
	current = null
	_neon_off()

func _neon_on():
	if tween.is_active(): tween.stop_all()
	tween.interpolate_property(
			neon_material,
			"emission_energy",
			neon_material.emission_energy,
			energy_on,
			.25,
			Tween.TRANS_SINE,
			Tween.EASE_IN_OUT)
	tween.interpolate_property(
			neon_material,
			"emission",
			neon_material.emission,
			color_on,
			.25,
			Tween.TRANS_SINE,
			Tween.EASE_IN_OUT)
	tween.start()

func _neon_off():
	if tween.is_active(): tween.stop_all()
	tween.interpolate_property(
			neon_material,
			"emission_energy",
			neon_material.emission_energy,
			energy_off,
			.25,
			Tween.TRANS_SINE,
			Tween.EASE_IN_OUT)
	tween.interpolate_property(
			neon_material,
			"emission",
			neon_material.emission,
			color_off,
			.25,
			Tween.TRANS_SINE,
			Tween.EASE_IN_OUT)
	tween.start()

func _ready():
	add_to_group("RuletaSensor")
	add_to_group("RuletaArea")
