extends Spatial


onready var host = get_parent()
onready var moving_platform = $PrizeMovingPlatform
onready var prize_particles = $PrizeParticles
onready var reset_particles = $ResetParticles

onready var current_prize = null

func _activate():
	moving_platform.start()
	prize_particles.restart()
	prize_particles.emitting = true
	get_tree().create_timer(.5).connect("timeout", self, "_on_activation_timeout")

func _deactivate():
	moving_platform.start()

func _on_RuletaMecanismo_prize(id : int):
	_activate()
	var position = moving_platform.global_transform.origin
	match id:
		1: current_prize = Builder.build_monitor(position, 0) # shield
		2: current_prize = Builder.build_monitor(position, 2) # life
		3: current_prize = Builder.build_monitor(position, 4) # fire
		4: current_prize = Builder.build_monitor(position, 5) # electric
		5: current_prize = Builder.build_monitor(position, 6) # speed
		6: current_prize = Builder.build_monitor(position, 7) # ice
		7: current_prize = Builder.build_monitor(position, 8) # bubble
		8: current_prize = Builder.build_monitor(position, 10) # 50 rings
	if current_prize:
		add_child(current_prize)

func _physics_process(delta):
	if current_prize and is_instance_valid(current_prize):
		current_prize.global_transform.origin = moving_platform.global_transform.origin

func _on_RuletaMecanismo_reset():
	if moving_platform.current_position_status == MovingPlatform.PositionStatus.AT_END:
		_deactivate()

func _on_PrizeMovingPlatform_at_begin():
	if current_prize and is_instance_valid(current_prize):
		current_prize.queue_free()
		if not current_prize.is_destroyed:
			reset_particles.restart()
			reset_particles.emitting = true
			host.discard_sfx.play()
			Instances.player.recolections._rings(host.rings_if_discard)
			get_tree().create_timer(.5).connect("timeout", self, "_on_reset_timeout")

func _on_activation_timeout():
	prize_particles.emitting = false
	moving_platform.start()

func _on_reset_timeout():
	reset_particles.emitting = false
