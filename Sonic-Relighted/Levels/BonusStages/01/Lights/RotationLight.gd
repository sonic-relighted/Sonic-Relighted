extends SpotLight

export(float) var angle : float = PI * 2.0
export(float) var time  : float = 1.0
onready var tween : Tween = $Tween

onready var current_angle : float = angle

func _rotate():
	tween.interpolate_property(
			self,
			"rotation:y",
			rotation.y,
			rotation.y + current_angle,
			time,
			Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()
	current_angle *= -1

func _on_Tween_tween_all_completed():
	_rotate()

func _ready():
	_rotate()
