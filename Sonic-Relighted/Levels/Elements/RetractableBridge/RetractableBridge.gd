extends Spatial

signal enabled
signal disabled

export(bool)  var shake_camera   : bool = true
export(bool)  var enabled        : bool  = false setget set_enabled,get_enabled

export(float) var rotation_acceleration : float = 4.0

onready var start_sfx : AudioStreamPlayer3D = $StartSfx
onready var loop_sfx  : AudioStreamPlayer3D = $LoopSfx
onready var end_sfx   : AudioStreamPlayer3D = $EndSfx
onready var shake_sfx : AudioStreamPlayer3D = $ShakeSfx

onready var working : bool = false

func set_enabled(new_enabled : bool):
	if working:
		enabled = !new_enabled
		return
	enabled = new_enabled
	var mov = $MovingPlatform
	if mov:
		working = true
		if enabled and mov.current_position_status == MovingPlatform.PositionStatus.AT_BEGIN:
			mov.start()
			start_sfx.play()
		elif not enabled and mov.current_position_status == MovingPlatform.PositionStatus.AT_END:
			$MovingPlatform/Vertical/RotatingPlatform.start()
			start_sfx.play()

func get_enabled() -> bool: return enabled

func _on_MovingPlatform_at_end():
	var rot = $MovingPlatform/Vertical/RotatingPlatform
	if rot:
		rot.acceleration = rotation_acceleration
		rot.start()
		loop_sfx.stop()
		end_sfx.play()
		start_sfx.play()

func _on_RotatingPlatform_all_steps_done():
	$MovingPlatform.start()
	start_sfx.play()

func _on_MovingPlatform_at_begin():
	working = false
	emit_signal("disabled")
	loop_sfx.stop()
	end_sfx.play()

func _on_RotatingPlatform_step_done(step):
	if step == 0:
		emit_signal("enabled")
		if shake_camera:
			Instances.camera.direction_hit_manager.hit(Vector3.DOWN * .5, 4.0)
			shake_sfx.play()
	working = false
	if start_sfx.playing: start_sfx.stop()
	loop_sfx.stop()
	end_sfx.play()

func _on_StartSfx_finished():
	loop_sfx.play()
