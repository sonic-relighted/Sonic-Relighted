extends Spatial

class_name PushButton, "res://RelightedEngine/EditorIcons/Button.png"

signal pressed

export(float) var lock_time : float = 1.0
export(bool)  var enabled   : bool  = true

onready var on_sfx       = $PushOnSfx
onready var off_sfx      = $PushOffSfx
onready var platform     = $Plataforma
onready var unlock_timer = $UnlockTimer

func is_locked():
	return platform.locked

func unlock():
	platform.locked = false
	unlock_timer.stop()

func _on_Plataforma_pressed():
	if enabled:
		unlock_timer.start(lock_time)
		emit_signal("pressed")
	else:
		unlock()

func _ready():
	for body in GroupFinder.find_nodes_by_type($Boton, StaticBody):
		var a : StaticBody = body
		$Joint.set_node_b(a.get_path())
