extends PushButton

export(NodePath) var object_to_animate : NodePath
export(String)   var animation_name    : String = "Action"

onready var animation_players : Array = GroupFinder.find_nodes_by_type(get_node(object_to_animate), AnimationPlayer)

func _on_PushButtonForAnimate_pressed():
	if enabled:
		for anim in animation_players:
			anim.play(animation_name)

func _finished(anim_name : String):
	enabled = true
	unlock()

func _connect():
	for anim in animation_players:
		anim.connect("animation_finished", self, "_finished")

func _ready():
	._ready()
	_connect()


