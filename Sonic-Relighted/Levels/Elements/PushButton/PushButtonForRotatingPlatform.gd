extends PushButton

enum Type {
	ALL_STEPS,
	STEP_BY_STEP
}

export(NodePath) var rotating_platform : NodePath
export(Type)     var type              : int = Type.ALL_STEPS

onready var rotating_platform_instance : RotatingPlatform = get_node(rotating_platform)

func _on_PushButtonForRotatingPlatform_pressed():
	if enabled:
		rotating_platform_instance.auto_start = false
		rotating_platform_instance.auto_advance = type == Type.ALL_STEPS
		rotating_platform_instance.in_loop = false
		rotating_platform_instance.start()
		enabled = false

func _end_step(step : int):
	if type == Type.STEP_BY_STEP:
		enabled = true

func _end_all_steps():
	enabled = true

func _connect():
	rotating_platform_instance.connect("step_done", self, "_end_step")
	rotating_platform_instance.connect("all_steps_done", self, "_end_all_steps")

func _ready():
	._ready()
	_connect()
