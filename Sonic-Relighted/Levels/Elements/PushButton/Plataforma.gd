extends RigidBody

signal pressed

onready var host = get_parent()
onready var locked : bool = false

var platform_mat : Material

func unlock():
	locked = false

func push(pusher : Spatial, point : Vector3):
	if locked: return
	var origin = point - get_global_transform().origin
	var impulse = Vector3.FORWARD.rotated(Vector3(0, 1, 0), pusher.calculated_direction) / 8
	apply_impulse(origin, impulse)
	if host.enabled:
		if translation.z > .015:
			locked = true
			emit_signal("pressed")
			host.on_sfx.play()
			platform_mat.emission_energy = .05

func _physics_process(delta):
	if locked:
		translation.z = .1

func _on_UnlockTimer_timeout():
	locked = false
	host.off_sfx.play()
	# En boton de tipo enable/disable no se puede
	# poner esto en otro sitio para que se enchufe la luz
	# cuando de verdad se desbloquee:
	platform_mat.emission_energy = 1.0

func _ready():
	var mesh = $CollisionShape/CSGBox
	platform_mat = mesh.material.duplicate()
	platform_mat.emission_energy = 1.0
	mesh.material = platform_mat
