extends PushButton

export(NodePath) var moving_platform : NodePath
export(bool)     var back_to_begin   : bool = false

onready var moving_platform_instance : MovingPlatform = get_node(moving_platform)

var time

func _on_PushButtonForTwoStateMovingPlatform_pressed():
	if enabled:
		time = OS.get_system_time_msecs()
		moving_platform_instance.initial_delay = .0
		moving_platform_instance.toggle_delay = .0
		moving_platform_instance.start()
		enabled = false

func _at_end():
	if back_to_begin:
		var elapsed = (OS.get_system_time_msecs() - time)  / 1000.0
		yield(get_tree().create_timer(lock_time - elapsed), "timeout")
		moving_platform_instance.start()
		enabled = false
	else:
		enabled = true

func _at_begin():
	enabled = true

func _connect():
	moving_platform_instance.connect("at_end", self, "_at_end")
	moving_platform_instance.connect("at_begin", self, "_at_begin")

func _ready():
	._ready()
	_connect()

