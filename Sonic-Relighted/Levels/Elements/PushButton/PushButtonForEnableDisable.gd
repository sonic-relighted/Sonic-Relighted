extends PushButton

export(NodePath) var object_to_manage : NodePath
export(bool)     var auto_toggle      : bool = false
export(bool)     var wait_signals     : bool = false

onready var object_to_manage_instance = get_node(object_to_manage)

func _on_PushButtonForEnableDisable_pressed():
	if enabled:
		enabled = false
		_toggle()

func _on_UnlockTimer_timeout():
	if auto_toggle:
		_toggle()
	if not wait_signals:
		enabled = true

func _toggle():
	object_to_manage_instance.set_enabled(
		!object_to_manage_instance.get_enabled())

func _on_enabled():
	pass

func _on_disabled():
	enabled = true

func _connect():
	if object_to_manage_instance:
		object_to_manage_instance.connect("enabled", self, "_on_enabled")
		object_to_manage_instance.connect("disabled", self, "_on_disabled")

func _ready():
	._ready()
	if wait_signals:
		_connect()
