extends Camera

onready var default_position = global_transform.origin
onready var default_rotation = rotation

var return_to_real_camera


func activate(time : float):
	current = true
	$DisableTimer.start(time)
	
	global_transform.origin = Instances.camera.camera.global_transform.origin
	global_transform.basis = Instances.camera.camera.global_transform.basis
	
	return_to_real_camera = false
	set_process(true)

func _on_DisableTimer_timeout():
	return_to_real_camera = true

func _process(delta):
	if return_to_real_camera:
		var d = delta * 8.0
		global_transform.origin = lerp(global_transform.origin, Instances.camera.camera.global_transform.origin, d)
		rotation.y = lerp_angle(rotation.y, Instances.camera.rotation.y, d)

		var diff = global_transform.origin - Instances.camera.camera.global_transform.origin
		if diff.length() < .1:
			Instances.camera.camera.current = true
			set_process(false)

	else:
		var d = delta * .8
		global_transform.origin = lerp(global_transform.origin, default_position, d)
		rotation.y = lerp_angle(rotation.y, default_rotation.y, d)

	fov = Instances.camera.camera.fov

func _ready():
	set_process(false)
