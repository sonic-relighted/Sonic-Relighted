tool
extends Area

class_name PhysicsOverwriteArea, "res://RelightedEngine/EditorIcons/PhysicsOverwriteArea.png"

enum Type {
	WATER,
	ICE
}

export(Vector3) var size : Vector3 = Vector3.ONE setget set_size, get_size
export(Type)    var type : int     = 0

onready var default_collision_layer : int = collision_layer
onready var default_collision_mask  : int = collision_mask

func set_size(new_size : Vector3):
	size = new_size
	if $CollisionShape:
		$CollisionShape.scale = size

func get_size() -> Vector3: return size
