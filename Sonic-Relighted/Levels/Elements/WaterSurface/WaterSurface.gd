tool
extends StaticBody

class_name WaterSurface, "res://RelightedEngine/EditorIcons/WaterSurface.png"

export(Vector2) var size : Vector2 = Vector2.ONE setget set_size, get_size
export(int)   var frozen_msecs : int = 1000

onready var frozen : bool = false
onready var frozen_time : int = 0

func set_size(new_size : Vector2):
	size = new_size
	if $CollisionShape:
		$CollisionShape.scale.x = size.x
		$CollisionShape.scale.z = size.y
		if $CSGBox:
			$CSGBox.width = size.x * 2.0
			$CSGBox.depth = size.y * 2.0
		if $CSGBox2:
			$CSGBox2.width = size.x * 2.0
			$CSGBox2.depth = size.y * 2.0

func get_size() -> Vector2:
	return size

func freeze():
	frozen_time = OS.get_system_time_msecs()
	if frozen: return
	frozen = true
	$IceTween.stop_all()
	yield(get_tree(), "idle_frame")
	$IceTween.interpolate_method(self, "_disolve", .0, 1.0, .5, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$IceTween.start()
	$CSGBox2.show()
	yield($IceTween, "tween_all_completed")
	$CSGBox.hide()

func reset():
	if not frozen: return
	frozen = false
	$IceTween.interpolate_method(self, "_disolve", 1.0, .0, .5, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$IceTween.start()
	$CSGBox.show()
	yield($IceTween, "tween_all_completed")
	$CSGBox2.hide()

func _disolve(value : float):
	$CSGBox.material.albedo_color.a = 1.0 - value * .5 - .5
	$CSGBox2.material.set_shader_param("disolve", value)

func _physics_process(delta : float):
	if frozen_time + frozen_msecs < OS.get_system_time_msecs():
		reset()

func _ready():
	if Engine.editor_hint: set_physics_process(false)
	$CSGBox.material = $CSGBox.material.duplicate()
	$CSGBox2.material = $CSGBox2.material.duplicate()
	_disolve(.0)
