# Al exportar no se que pasa con LOS COLLISION POLYGON... no funcionan.
# he dejado el que es guay, desactivado, pero como no funciona he creado un
# shape provisional
extends Spatial

class_name Loop, "res://RelightedEngine/EditorIcons/Loop.png"

onready var audio_door    : AudioStreamPlayer = $AudioDoor
onready var camera        : Camera            = $Camera
onready var animator      : AnimationPlayer   = $AnimationPlayer
onready var indicator     : OmniLight         = $IndicatorLight
onready var lock          : StaticBody        = $Lock
onready var path          : Path              = $Path
onready var path_follow   : PathFollow        = $Path/PathFollow
onready var camera_target : Spatial           = null
onready var camera_target_position : Vector3  = Vector3.ZERO
onready var enter_point_1 : Spatial           = $EnterPoint1
#onready var enter_point_2 : Spatial           = $EnterPoint2
onready var exit_point    : Spatial           = $ExitPoint
onready var enter_point_1_translation : Vector3 = enter_point_1.get_global_transform().origin
#onready var enter_point_2_translation : Vector3 = enter_point_2.get_global_transform().origin

onready var lock_collision_layer = lock.collision_layer
onready var lock_collision_mask = lock.collision_mask

func _ready():
	# solo funciona en este angulo, hoy por hoy
	# y a demas no funciona el entrar por la salida...
#	rotation.y = -AngleConstants.SEMI_PI
	_enable_lock()
	set_process(false)
	set_physics_process(false)

func _physics_process(delta):
	if camera_target != null:
		camera_target.translation = path_follow.get_global_transform().origin
		_camera_follow(delta)

func _on_ActivatedArea_body_entered(body):
	if camera_target == null and body.is_in_group("Player"):
		camera.set_current(true)
		camera_target = body
		camera_target_position = camera_target.translation
		camera_target.is_on_loop = true
		camera_target.rail = path_follow
		_disable_lock()
		camera_target.current_speed = .0
		camera_target.current_updown = AngleConstants.SEMI_PI
		path_follow.offset = 0
		Instances.start_point.global_transform.origin = body.global_transform.origin
		set_physics_process(true)
		for device in GroupFinder.find_nodes_in_group(Instances.level, "Device"):
			device.persist()

func _on_ActivatedArea_body_exited(body):
	if body.is_in_group("Player"):
#		camera_target.velocity = Vector3.FORWARD \
#				.rotated(Vector3(1,0,0), rotation.x) \
#				.rotated(Vector3(0,1,0), rotation.y) \
#				.rotated(Vector3(0,0,1), rotation.z) * 30
		camera.set_current(false)
		path_follow.offset
		camera_target.translation = exit_point.get_global_transform().origin
		camera_target.is_on_loop = false
		camera_target.rail = null
		camera_target = null
		Instances.camera.camera.current = true
		_enable_lock()
		audio_door.play()
		path_follow.offset = 0
		set_physics_process(false)

func _disable_lock():
	animator.play("Open")
	lock.collision_layer = 0
	lock.collision_mask = 0
	if is_instance_valid(indicator):
		indicator.light_color = Color.green
#	$Armature/LoopDoor.get_surface_material(1).set_emission(Color.green)

func _enable_lock():
	animator.play("Close")
	lock.collision_layer = lock_collision_layer
	lock.collision_mask = lock_collision_mask
	if is_instance_valid(indicator):
		indicator.light_color = Color.red
#	$Armature/LoopDoor.get_surface_material(1).set_emission(Color.red)

# Esto esta pensado para que funcione en 2 direcciones, pero no funciona.
# no obstante, lo dejo por si lo corrijo algun dia...
# El problema esta en la conversion del floor_normal en current_updown
#func _get_near_point() -> Spatial:
#	return enter_point_1
#	var distance_with_1 = (enter_point_1_translation - camera_target_position).length()
#	var distance_with_2 = (enter_point_2_translation - camera_target_position).length()
#	if distance_with_1 < distance_with_2:
#		return enter_point_1
#	return enter_point_2

func _camera_follow(delta : float):
	camera_target_position = lerp(camera_target_position, camera_target.translation, .1)
	camera.look_at(camera_target_position, Vector3.UP)
