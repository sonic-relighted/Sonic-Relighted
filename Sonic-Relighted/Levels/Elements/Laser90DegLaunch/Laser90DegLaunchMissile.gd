extends KinematicBody

onready var body         : Spatial  = $Body
onready var velocity     : Vector3  = Vector3.ZERO
onready var lifetime     : float    = 6.0

func _physics_process(delta):
	lifetime -= delta
	if lifetime < 0:
		queue_free()
	var collision = move_and_collide(velocity * delta, true)
	body.rotation.y = atan2(velocity.x, velocity.z) + AngleConstants.SEMI_PI
	if collision != null:
		queue_free()
