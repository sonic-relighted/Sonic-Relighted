extends StaticBody

export(float) var missile_speed : float = 5.0

onready var animator       : AnimationPlayer = $Laser90DegLaunch/AnimationPlayer
onready var animator2      : AnimationPlayer = $AnimationPlayer
onready var aim            : Spatial         = $Aim
onready var missile_output : Spatial         = $Aim/MissileOutput

onready var bodies_inside : int = 0

func _ready():
	animator.play("Action")
	animator2.play("Action")

func _shoot():
	if bodies_inside > 0 : _create_laser()
	_update_angle()

func _create_laser():
	var angle : float = aim.rotation.y + rotation.y
	var direction = Vector3.RIGHT.rotated(Vector3(0,1,0), angle)
	Builder.build_laser90deg_missile(
			Instances.level,
			missile_output.global_transform.origin,
			direction * missile_speed)

func _update_angle():
	aim.rotation.y += AngleConstants.SEMI_PI
	while aim.rotation.y > AngleConstants.TWO_PI:
		aim.rotation.y -= AngleConstants.TWO_PI

func _on_AnimationPlayer_animation_finished(anim_name):
	animator.stop()
	animator.play(anim_name)
	animator2.play(anim_name)

func _on_Area_body_entered(body):
	if body.is_in_group("Player"):
		bodies_inside += 1

func _on_Area_body_exited(body):
	if body.is_in_group("Player"):
		bodies_inside -= 1
