extends Node

# Permite activar y desactivar, asi en plan general
# por ejemplo para subir y bajar plataformas

class_name SwitchManager

onready var enabled               : bool = false
onready var transaction_completed : bool = true
onready var host = get_parent()

func toggle():
	if enabled:
		disable()
	else:
		enable()

func enable():
	transaction_completed = false
	set_physics_process(true)

func disable():
	transaction_completed = false
	set_physics_process(true)

func _ready():
	set_physics_process(false)
	set_process(false)

func _physics_process(delta):
	if transaction_completed:
		set_physics_process(false)
		return

	if enabled:
		_disable(delta)
	else:
		_enable(delta)

func _enable(delta : float):
	if host.enabling(delta):
		transaction_completed = true
		enabled = true

func _disable(delta : float):
	if host.disabling(delta):
		transaction_completed = true
		enabled = false
