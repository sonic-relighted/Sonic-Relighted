extends Spatial

class_name AbstractLevel

const COLOR_WHITE = Color.white
const COLOR_YELLOW = Color(.8,.7,.3)
const COLOR_RED = Color.red

func get_min_position_y() -> float:
	return -.5

func _ready():
	_configure_collisions()
	_configure_floor()
	_initialize_live_objects()
	_update_ingame_data()
	_configure_lamps()
	_configure_lights()
	_configure_shadows()
#	yield(get_tree().create_timer(.5), "timeout")
#	for a in GroupFinder.find_nodes_by_type(self, Area):
#		if a.monitoring:
#			print(a.get_path())

func _configure_shadows():
	for obj in get_tree().get_nodes_in_group("Shadowable"):
		Builder.build_bottom_shadow(obj)

func _configure_lights():
	for light in GroupFinder.find_nodes_by_type(self, Light):
		light.shadow_enabled = false#SettingsData.data.shadows
		light.light_bake_mode = Light.BAKE_ALL

func _configure_collisions():
	var scene = $Scene
	for obj in GroupFinder.find_nodes_in_group(self, "InvWall"):
		#[x][ ][ ][ ][ ][ ][ ][ ][ ][ ]
		#[x][ ][ ][ ][ ][ ][ ][ ][ ][ ]
		obj.collision_mask = 1025
		obj.collision_layer = 1025
	for obj in GroupFinder.find_nodes_by_type(scene, StaticBody):
		#[x][x][ ][ ][ ][ ][ ][ ][ ][ ]
		#[x][x][ ][ ][ ][ ][ ][ ][ ][ ]
		obj.collision_mask = 3075
		obj.collision_layer = 3075
	# Surface water
	#[ ][ ][x][ ][ ][ ][ ][ ][ ][ ]
	#[ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
#	obj.collision_mask = 4
#	obj.collision_layer = 4

func _configure_lamps():
	var scene = $Scene
	for lamp in GroupFinder.find_nodes_by_name(scene, "Lamp"):
		var light : OmniLight = Builder.build_omni_light()
		if "Red" in lamp.get_name(): light.light_color = COLOR_RED
		elif "Yellow" in lamp.get_name(): light.light_color = COLOR_YELLOW
		else: light.light_color = COLOR_WHITE
		lamp.add_child(light)
		lamp.get_child(0).add_to_group("Lamp")
		lamp.get_child(0).add_to_group("NoFloor")
	
#	for lamp in GroupFinder.find_nodes_by_name(scene, "LampWithLamp"):
#		lamp.add_child(Builder.build_sprite_light())

func _configure_floor():
	var scene = $Scene
#	for mesh in GroupFinder.find_nodes_by_type(scene, StaticBody):
#		mesh.get_child(0).add_to_group("Floor")

func _initialize_live_objects():
	for elem in GroupFinder.find_nodes_by_type(self, LiveObjects):
		elem.set_process(true)

func _update_ingame_data():
	ProgressSingletone.ingame_seconds = 0
	ProgressSingletone.ingame_devices = 0
	ProgressSingletone.ingame_devices_total = _devices_count()

func _devices_count() -> int:
	return get_tree().get_nodes_in_group("Device").size()
