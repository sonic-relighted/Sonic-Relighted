extends Area

export(NodePath)       var host_nodepath    : NodePath
export(Array,NodePath) var physic_nodepaths : Array

onready var host : Player = get_node(host_nodepath)

onready var count : int = 0 # esto solo soluciona tener varias areas de fisicas, pero si son del mismo tipo

func _on_FoorPhysicsDetector_area_entered(area):
	if count == 0: host.emit_signal("physics_area_enter", area.type)
	count += 1

func _on_FoorPhysicsDetector_area_exited(area):
	count -= 1
	if count == 0: host.emit_signal("physics_area_exit", area.type)
