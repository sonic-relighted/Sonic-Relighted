extends Sprite3D

onready var host = get_parent().get_parent().get_parent()
onready var animator = $AnimationPlayer

var target

func start():
	set_physics_process(true)
	target = null

func stop():
	set_physics_process(false)
	hide()

func _physics_process(delta : float):
	target = host.homing_area.get_nearest()
	if target:
		if !visible:
			animator.play("show")
			_correct_offset(target)
	else:
		hide()
	if target: _positioning(target)

func _positioning(target):
	global_transform.origin = target.global_transform.origin

func _correct_offset(target):
	if target is MonitorItem:
		offset.y = 8.0
#	elif target is TurtleEnemy:
#		offset.y = 32
#	elif target is FishEnemy:
#		offset.y = 22
#	elif target is FrogEnemy:
#		offset.y = 128

func _ready():
	stop()
