extends Area

#
# Este es el area en el que aparecen los puntos de mira
#

onready var bodies = []

func get_nearest() -> Spatial:
	var r = null
	var distance : float = 99.0
	var my_position = global_transform.origin
	for body in bodies:
		if is_instance_valid(body) and body.is_destroyed == false:
			var diff = my_position - body.global_transform.origin
			if diff.length() < distance:
				distance = diff.length()
				r = body
	return r

func _on_EnemiesNearArea_body_entered(body):
	if body.is_in_group("Enemy") or body.is_in_group("Destructible") or body.is_in_group("Homingable"):
		bodies.append(body)

func _on_EnemiesNearArea_body_exited(body):
	if body.is_in_group("Enemy") or body.is_in_group("Destructible") or body.is_in_group("Homingable"):
		bodies.erase(body)
