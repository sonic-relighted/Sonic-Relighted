extends Node

export(NodePath) var ceil_raycast : NodePath
export(NodePath) var floor_raycast : NodePath

onready var host = get_parent()
onready var ceil_raycast_instance : RayCast = get_node(ceil_raycast)
onready var floor_raycast_instance : RayCast = get_node(floor_raycast)

func _physics_process(delta : float):
	if host.is_on_floor():
		if ceil_raycast_instance.is_colliding() \
		and floor_raycast_instance.is_colliding():
			if _chk_col(ceil_raycast_instance) \
			or _chk_col(floor_raycast_instance):
				host._death()

func _chk_col(raycast : RayCast) -> bool:
	return raycast.get_collider().is_in_group("MovingBody")
