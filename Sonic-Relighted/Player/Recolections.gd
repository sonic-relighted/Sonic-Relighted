extends Node

class_name PlayerRecolections

onready var host : Player = get_parent()

func _ready():
	set_process(false)
	set_physics_process(false)

func recolect(element : Spatial):
	if element is RingItem:
		_rings(1)
		_rings_to_lifes()
	elif element is StarItem:
		_star(element.star_id)
	elif element is MonitorItem and not element.is_destroyed:
		var type : int = element.get_type()
		match type:
			0: _shield(0) # Shield Normal
			1: _invencibility()
			2: _extra_life()
			3: _rings(10)
			4: _shield(1) # Shield Fuego
			5: _shield(2) # Shield Electrico
			6: _speed()
			7: _shield(3) # Shield Hielo
			8: _shield(4) # Shield Burbuja
			10: _rings(50)
		_rings_to_lifes()
	elif element is Device:
		ProgressSingletone.ingame_devices += 1
		Instances.hud.update_devices()
		if ProgressSingletone.ingame_devices >= ProgressSingletone.ingame_devices_total:
			host.emit_signal("complete_level")
	elif element is AirBubble:
		_air_bubble()

func _shield(type):
	host.sfx.play_shield()
	host.is_shielded = true
	host.vfx.disable_shield()
	host.vfx.enable_shield(type)
	host.shield_type = type
	ProgressSingletone.game_slot_data.shield = type

func _invencibility():
	host.invencibility_particles.emitting = true
	host.is_invencible = true
	var timer = Timer.new()
	add_child(timer)
	timer.wait_time = host.invencible_time
	timer.connect("timeout", self, "_end_invencibility")
	timer.start()

func _end_invencibility():
	host.invencibility_particles.emitting = false
	host.is_invencible = false

func _speed():
	host.is_hi_speed = true
	host.emit_signal("hi_speed", host.is_hi_speed)
	get_tree().create_timer(host.physics.HI_SPEED_TIME)\
			.connect("timeout", self, "_end_speed")

func _end_speed():
	host.is_hi_speed = false
	host.emit_signal("hi_speed", host.is_hi_speed)

func _rings(count : int):
	ProgressSingletone.ingame_rings += count
	ProgressSingletone.ingame_rings_to_life -= count
	Instances.hud.update_rings()
	host.sfx.play_item_ring()

func _star(star_id):
	ProgressSingletone.ingame_stars[star_id] = true

func _extra_life():
	if ProgressSingletone.game_slot_data.lifes < 99:
		ProgressSingletone.game_slot_data.lifes += 1
		Instances.hud.update_lifes()
		host.sfx.play_extra_life()

func _rings_to_lifes():
	if ProgressSingletone.ingame_rings_to_life < 1 and ProgressSingletone.ingame_rings < 400:
		_extra_life()
		ProgressSingletone.ingame_rings_to_life += 100

func _air_bubble():
	host.physics_container.reset_timer()
	host.sfx.play_underwater_air()
	host.state_machine.current_state.exit(host)
	host.state_machine.current_state = host.state_machine.underwater_breath_state
	host.state_machine.current_state.enter(host)
