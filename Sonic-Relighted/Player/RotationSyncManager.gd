extends Node

# Sincroniza la rotacion del personaje con el floor_normal

onready var host = get_parent()

func align_to_floor(delta : float):
	var normal = host.floor_normal.normalized()
	var x = atan2(normal.z, sqrt(pow(normal.y, 2) + pow(normal.x, 2)))
	var y = .0
	var z = -atan2(normal.x, sqrt(pow(normal.y, 2) + pow(normal.z, 2)))
	host.current_updown = _calc_updown(x, z)
	_rotate_character(delta, x, y, z)

func _calc_updown(x : float, z : float) -> float:
	var dir = host.calculated_direction
	var calc = \
			cos(dir - AngleConstants.SEMI_PI) * (-z) \
			+ cos(dir) * x
	return calc + AngleConstants.SEMI_PI

func _rotate_character(delta : float, x : float, y : float, z : float):
	var speed = delta * 8.0
	host.rotation.x = lerp_angle(host.rotation.x, x, speed)
	host.rotation.z = lerp_angle(host.rotation.z, z, speed)
	host.rotation.y = y
