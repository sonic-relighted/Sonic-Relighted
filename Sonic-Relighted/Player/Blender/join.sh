out="../../Sprites"

convert \
    \( idle.png +append \) \
    \( walk.png +append \) \
    \( jump.png +append \) \
    \( drift.png +append \) \
    -background none -append "tmp.png"

mv -fv tmp.png $out/Player.png
