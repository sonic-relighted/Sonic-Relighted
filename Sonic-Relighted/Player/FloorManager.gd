extends Node

class_name PlayerFloorManager, "res://RelightedEngine/EditorIcons/Floor.png"

onready var host = get_parent()

func ready():
	set_physics_process(false)
	set_process(false)

#
# Al acabar de caer/saltar, esto calcula el angulo y esas cosicas
#
func get_floor():
	host.is_grounded = host.floor_raycast.is_colliding()
	var new_normal = _get_floor_normal()
	if new_normal == null:
		# Esto ocasiona el out bounds...
		host.floor_normal = Vector3.UP
	else:
		host.floor_normal = new_normal

#
# Comprueba si estamos tocando suelo, y calcula el angulo
#
func check_ground(delta : float):
	if host.is_jumping: return
	host.is_grounded = host.floor_raycast.is_colliding()
	var new_normal = _get_floor_normal()
#	var limit = 20 * delta
	if new_normal == null:
		host.floor_normal = host.floor_normal
	elif host.floor_normal == Vector3.ZERO:
		host.floor_normal = new_normal
	elif host.left_raycast.is_colliding() or host.right_raycast.is_colliding():
		host.floor_normal = host.floor_normal
	else:#elif abs(new_normal.x - host.floor_normal.x) < limit and abs(new_normal.y - host.floor_normal.y) < limit and abs(new_normal.z - host.floor_normal.z) < limit:
		host.floor_normal = new_normal

#
# Calcula el angulo del suelo
#
# Esto hay que convertirlo a move_and_collide...
#
func _get_floor_normal():
	var count : int = 0
	var r : Vector3 = Vector3.ZERO
	var i : int = 0
	while i < host.get_slide_count():
		var col = host.get_slide_collision(i)
		var collider = col.collider
		if is_instance_valid(collider):
			var point_col = col.position - host.floor_detection_limit.global_transform.origin
			if point_col.length() < .12:
				r += col.normal
				count += 1
		i += 1
	if count == 0:
		return null
#	if r.x > .0:
#		return r
	return r.normalized()

