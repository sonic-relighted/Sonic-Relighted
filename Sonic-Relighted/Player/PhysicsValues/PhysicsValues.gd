extends Node

onready var host = get_parent()
onready var timer : Timer  = $Timer

onready var seconds : int = 0
onready var warns_to_left : int = 0
onready var warns_time : int = 0

func reset_timer():
	seconds = 0
	warns_time = host.physics.TIME_PHYSICS_WARN_TIME
	warns_to_left = host.physics.TIME_PHYSICS_WARNS_TO_DIE
	timer.stop()
	timer.start()
	Instances.hud.reset_countdown()
	host.sfx.stop_underwater()

func activate(type : int):
	host.physics = get_child(type)
	reset_timer()

func activate_default():
	host.physics = $Default
	reset_timer()
	host.sfx.stop_underwater()
	Instances.hud.reset_countdown()

func _on_Player_physics_area_enter(type : int):
	activate(type)
	if type == PhysicsOverwriteArea.Type.WATER:
		host.vfx.splash()
		host.sfx.play_splash_in()

func _on_Player_physics_area_exit(type : int):
	activate_default()
	if type == PhysicsOverwriteArea.Type.WATER:
		host.vfx.splash()
		host.sfx.play_splash_out()

func _on_Timer_timeout():
	if host.physics.TIMED_PHYSICS and host.physics.is_timed_physics_active():
		seconds += 1
		if seconds > host.physics.TIME_PHYSICS_WARN:
			_warn()

func _warn():
	if warns_time >= host.physics.TIME_PHYSICS_WARN_TIME:
		host.sfx.play_underwater()
		if warns_to_left < 0:
			_death()
		else:
			Instances.hud.countdown(warns_to_left)
		warns_time = 0
		warns_to_left -= 1
	warns_time += 1

func _death():
	host._death()
	activate_default()
