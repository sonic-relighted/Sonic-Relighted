extends Node

class_name PhysicsValues

export(float)   var MAX_SPEED       : float = 8.0
export(int)     var ROLL_MAX_SPEED  : int   = 10.0
export(float)   var LOOP_SPEED      : float = 10.0
export(int)     var AIR_MAX_SPEED   : int   = 8.0
export(float)   var SLIDE_IMPACT    : float = 0.0
export(float)   var HI_SPEED_VALUE  : float = 1.75
export(float)   var HI_SPEED_TIME   : float = 20.0

export(float)   var JUMP                 : float = 3.8 / 2.0
export(float)   var DOUBLE_JUMP          : float = 1.9
export(float)   var SPINDASH_TIME        : float = 2.0
export(float)   var BRAKE                : float = 15.0
export(Vector3) var STOMP_VEL            : Vector3 = Vector3.DOWN * 20.0
export(float)   var HOMING_LOCKTIME      : float = .15
export(float)   var BUBBLE_BOUNCE_FACTOR : float = 1.1

export(float)   var AIR_CONTROL             : float = .18
export(float)   var DOUBLE_JUMP_AIR_CONTROL : float = .18
export(float)   var FALL_AIR_CONTROL        : float = .18

export(float)   var GRAVITY : float = .98 / 2

export(bool)    var TIMED_PHYSICS             : bool # Si hay contador y muerte
export(int)     var TIME_PHYSICS_WARN         : int = -1 # Segundos hasta el primer warning
export(int)     var TIME_PHYSICS_WARN_TIME    : int = 3 # Segundos entre warnings
export(int)     var TIME_PHYSICS_WARNS_TO_DIE : int = 5 # Warnings hasta morir

onready var host = get_parent().get_parent()

# Esto habria que sobreescribir, de momento no es necesario
# porque solo existe un tipo de fisicas que matan, el agua
func is_timed_physics_active() -> bool:
	return not (host.is_shielded and host.shield_type == 4)
