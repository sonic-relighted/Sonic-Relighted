extends Node

onready var animator_player : AnimationPlayer = $"../SonicModel/AnimationPlayer"

var current_anim

func _ready():
	animator_player.playback_default_blend_time = .1
	animator_player.connect("animation_finished", get_parent().get_parent(), "_on_animation_finished")

func do(anim_name : String, speed : float, force : bool = false):
	animator_player.playback_speed = speed
	if !force and current_anim == anim_name:
		return
	current_anim = anim_name
	animator_player.play(anim_name, -1, speed)

func do_loop(anim_name : String, speed : float, force : bool = false):
	animator_player.playback_speed = speed
	if !force and current_anim == anim_name:
		return
	current_anim = anim_name
	animator_player.play(anim_name, 0, speed)
