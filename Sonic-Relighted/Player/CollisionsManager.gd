extends Node

onready var host = get_parent()

func open_item(body):
#	var direction : Vector3 = Vector3.UP
#	host.velocity = host.velocity.bounce(direction)
	host.recolect(body)
	body.recolect()

func destroy_destructible(body):
	var direction : Vector3 = Vector3.UP
	if body.destroy():
		host.velocity = host.velocity.bounce(direction)

func destroy_stompable(body):
	var direction : Vector3 = Vector3.UP
	if body.destroy():
		host.velocity = host.velocity.bounce(direction)

func kill_enemy(body):
	var direction : Vector3 = Vector3.UP
	host.velocity = host.velocity.bounce(direction)
	if body.has_method("damage") and body.damage():
		ProgressSingletone.game_slot_data.score += 100
	if host.velocity.y < 0:
		host.velocity *= -1

func touch_enemy(body):
	var force : Vector3 = body.get_damage_force()
	var other : Vector2 = Vector2(body.global_transform.origin.x, body.global_transform.origin.z)
	var me    : Vector2 = Vector2(host.global_transform.origin.x, host.global_transform.origin.z)
	var diff = me - other
	diff.x *= -1
	host.damage(force.rotated(Vector3.UP, diff.angle() - AngleConstants.SEMI_PI))

func touch_damage(area):
	if area.is_destructible() and area.check(host):
		area.destroy(host)
	else:
		var force : Vector3 = area.get_damage_force()
		var other : Vector2 = Vector2(area.global_transform.origin.x, area.global_transform.origin.z)
		var me    : Vector2 = Vector2(host.global_transform.origin.x, host.global_transform.origin.z)
		var diff = me - other
		diff.x *= -1
		host.damage(force.rotated(Vector3.UP, diff.angle() - AngleConstants.SEMI_PI))
