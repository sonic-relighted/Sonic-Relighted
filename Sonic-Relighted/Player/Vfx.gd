extends Spatial

onready var host = get_parent()

export(NodePath) var center_position : NodePath

onready var shield : Spatial = $Shield
onready var fire_shield : Spatial = $FireShield
onready var fire_shield_particles : Particles = $FireShield/Particles
onready var electric_shield : Spatial = $ElectricShield
onready var electric_shield_particles : Particles = $ElectricShield/Particles
onready var ice_shield : Spatial = $IceShield
onready var bubble_shield : Spatial = $BubbleShield
onready var stomp_vfx : Sprite3D = $StompVfx
onready var stomp_particles : Particles = $StompParticles
onready var splash_particles : Particles = $SplashParticles

onready var center_position_instance = get_node(center_position)

func _on_Player_physics_area_enter(type : int):
	if type == PhysicsOverwriteArea.Type.WATER:
		if fire_shield.visible or electric_shield.visible:
			disable_shield()
			host.shield_type = -1
			host.is_shielded = false
			print("SFX")

func enable_shield(type : int):
	host.set_collision_layer_bit(2, false)
	host.floor_raycast.set_collision_mask_bit(2, false)
	match type:
		0: shield.show()
		1: fire_shield.show()
		2: electric_shield.show()
		3:
			ice_shield.show()
			host.set_collision_layer_bit(2, true)
			host.floor_raycast.set_collision_mask_bit(2, true)
		4: bubble_shield.show()

func disable_shield():
	if host.is_shielded:
		host.emit_signal("lost_shield", host.shield_type)
		action_bubble_stop()
	shield.hide()
	fire_shield.hide()
	electric_shield.hide()
	ice_shield.hide()
	bubble_shield.hide()
	host.set_collision_layer_bit(2, false)
	host.floor_raycast.set_collision_mask_bit(2, false)

func action_fire():
	fire_shield_particles.emitting = true

func action_electric():
	electric_shield_particles.emitting = true

func action_ice():
	true

func action_bubble():
	bubble_shield.animator.play("squash")

func action_bubble_stop():
	bubble_shield.animator.play("shield")

func start_stomp():
	if SettingsData.data.transparencies:
		stomp_vfx.show()

func stop_stomp():
	stomp_vfx.hide()
	stomp_particles.emitting = true
	if get_tree():
		yield(get_tree().create_timer(.12), "timeout")
	stomp_particles.emitting = false

func stop_fire():
	fire_shield_particles.emitting = false

func splash():
	splash_particles.emitting = true
	if get_tree():
		yield(get_tree().create_timer(.12), "timeout")
	splash_particles.emitting = false

func _physics_process(delta : float):
	rotation.y = Instances.camera.rotation.y
	global_transform.origin = lerp(
			global_transform.origin,
			center_position_instance.global_transform.origin,
			delta * 10)
