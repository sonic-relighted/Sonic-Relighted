extends Node

onready var host = get_parent()

const JUMP_KEY = "jump"
const ROLL_KEY = "roll"
const MENU_KEY = "menu"
const CAML_KEY = "camL"
const CAMR_KEY = "camR"

func get_analog_joystick_value() -> Vector2:
	var hz_amount = -Input.get_action_strength("left") + Input.get_action_strength("right")
	var vt_amount = _get_vt() 
	var r = Vector2(hz_amount, vt_amount)
	if r.length() < .05:
		return Vector2.ZERO
	return r

func _get_vt() -> float:
	if host.is_25D: return .0
	return -Input.get_action_strength("up") + Input.get_action_strength("down")

func is_just_pressed_jump() -> bool:
	return Input.is_action_just_pressed(JUMP_KEY)

func is_pressed_jump() -> bool:
	return Input.is_action_pressed(JUMP_KEY)

func is_just_released_jump() -> bool:
	return Input.is_action_just_released(JUMP_KEY)

func is_just_pressed_roll() -> bool:
	return Input.is_action_just_pressed(ROLL_KEY)
	
func is_just_released_roll() -> bool:
	return Input.is_action_just_released(ROLL_KEY)

func is_just_pressed_menu() -> bool:
	return Input.is_action_just_pressed(MENU_KEY)

func is_just_pressed_camL() -> bool:
	return Input.is_action_just_pressed(CAML_KEY)

func is_just_pressed_camR() -> bool:
	return Input.is_action_just_pressed(CAMR_KEY)
