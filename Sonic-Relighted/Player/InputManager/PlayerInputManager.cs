using Godot;
using System;

public class PlayerInputManager : Node
{
	private MySystem sys;
	
	private bool pad_player1_connected;
	private Node keys;
	private bool is_25D;
	private bool locked;
	
	const String UP_KEY    = "up";
	const String LEFT_KEY  = "left";
	const String RIGHT_KEY = "right";
	const String DOWN_KEY  = "down";
	const String WALK_KEY  = "walk";
	
	public bool is_pressing_up() {
		if (locked) { return false; }
		return Input.IsActionPressed(UP_KEY);
	}
	public bool is_pressing_left() {
		if (locked) { return false; }
		return Input.IsActionPressed(LEFT_KEY);
	}
	public bool is_pressing_right() {
		if (locked) { return false; }
		return Input.IsActionPressed(RIGHT_KEY);
	}
	public bool is_pressing_down() {
		if (locked) { return false; }
		return Input.IsActionPressed(DOWN_KEY);
	}
	
	public bool is_pressing_walk() {
		if (locked) { return false; }
		return Input.IsActionPressed(WALK_KEY);
	}
	
	public bool is_just_pressed_up() {
		if (locked) { return false; }
		return Input.IsActionJustPressed(UP_KEY);
	}
	public bool is_just_pressed_left() {
		if (locked) { return false; }
		return Input.IsActionJustPressed(LEFT_KEY);
	}
	public bool is_just_pressed_right() {
		if (locked) { return false; }
		return Input.IsActionJustPressed(RIGHT_KEY);
	}
	public bool is_just_pressed_down() {
		if (locked) { return false; }
		return Input.IsActionJustPressed(DOWN_KEY);
	}
	
	public bool is_just_pressed_roll() {
		if (locked) { return false; }
		return ((InputManagerPhysicalKeys)keys).is_just_pressed_roll();
	}
	public bool is_just_pressed_jump() {
		if (locked) { return false; }
		return ((InputManagerPhysicalKeys)keys).is_just_pressed_jump();
	}
	public bool is_pressed_jump() {
		if (locked) { return false; }
		return ((InputManagerPhysicalKeys)keys).is_pressed_jump();
	}
	
	public bool is_just_released_roll() {
		if (locked) { return false; }
		return ((InputManagerPhysicalKeys)keys).is_just_released_roll();
	}
	public bool is_just_released_jump() {
		if (locked) { return false; }
		return ((InputManagerPhysicalKeys)keys).is_just_released_jump();
	}
	
	public bool is_just_pressed_menu() {
		if (locked) { return false; }
		return ((InputManagerPhysicalKeys)keys).is_just_pressed_menu();
	}
	
	public bool is_just_pressed_camL() {
		if (locked) { return false; }
		return ((InputManagerPhysicalKeys)keys).is_just_pressed_camL();
	}
	
	public bool is_just_pressed_camR() {
		if (locked) { return false; }
		return ((InputManagerPhysicalKeys)keys).is_just_pressed_camR();
	}
	
	public Vector2 get_analog_joystick_value() {
		if (locked) { return new Vector2(0, 0); }
		return ((InputManagerPhysicalKeys)keys).get_analog_joystick_value();
	}
		
	public override void _Ready() {
		sys = GetNode<MySystem>("/root/MySystem");
		GD.Print("En C# No existe Input.Connect, debes crear un nodo Initializer con su correspondiente .gd");
		on_joy_connection_changed(0, Input.IsJoyKnown(0));
		SetPhysicsProcess(false);
		SetProcess(false);
		pad_player1_connected = false;
		if (sys.is_mobile()) {
			keys = GetNode("VirtualKeys");
		} else {
			keys = GetNode("PhysicalKeys");
		}
		is_25D = false;
		locked = false;
	}

	public void on_joy_connection_changed(int device_id, bool connected) {
		if (device_id == 0) {
			pad_player1_connected = connected;
			if (sys.is_mobile()) {
				if (connected) {
					keys = GetNode("PhysicalKeys");
				} else {
					keys = GetNode("VirtualKeys");
				}
			}
		}
	}
}
