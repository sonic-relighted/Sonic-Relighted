using Godot;
using System;

public class CSharpInputManagerPhysicalKeys : Node
{
	const String JUMP_KEY = "jump";
	const String ROLL_KEY = "roll";
	const String MENU_KEY = "menu";
	const String CAML_KEY = "camL";
	const String CAMR_KEY = "camR";
	
	private MySystem sys;
	private Node host;
	
	public override void _Ready() {
		sys = GetNode<MySystem>("/root/MySystem");
		host = GetParent();
		GD.Print(sys.is_mobile());
	}

	public Vector2 get_analog_joystick_value() {
		float hz_amount = -Input.GetActionStrength("left") + Input.GetActionStrength("right");
		float vt_amount = _get_vt();
		var r = new Vector2(hz_amount, vt_amount);
		if (r.Length() < .05) {
			return new Vector2(0, 0);
		}
		return r;
	}

	public float _get_vt() {
//		if (host.is_25D) { return .0; }
		return -Input.GetActionStrength("up") + Input.GetActionStrength("down");
	}
	
	public bool is_just_pressed_jump() {
		return Input.IsActionJustPressed(JUMP_KEY);
	}
	
	public bool is_pressed_jump() {
		return Input.IsActionPressed(JUMP_KEY);
	}
	
	public bool is_just_released_jump() {
		return Input.IsActionJustReleased(JUMP_KEY);
	}
	
	public bool is_just_pressed_roll() {
		return Input.IsActionJustPressed(ROLL_KEY);
	}
		
	public bool is_just_released_roll() {
		return Input.IsActionJustReleased(ROLL_KEY);
	}
	
	public bool is_just_pressed_menu() {
		return Input.IsActionJustPressed(MENU_KEY);
	}
	
	public bool is_just_pressed_camL() {
		return Input.IsActionJustPressed(CAML_KEY);
	}
	
	public bool is_just_pressed_camR() {
		return Input.IsActionJustPressed(CAMR_KEY);
	}

}
