extends Node

class_name PlayerInputManager, "res://RelightedEngine/EditorIcons/Input.png"

onready var pad_player1_connected : bool = false
onready var keys = $VirtualKeys if MySystem.is_mobile() else $PhysicalKeys

onready var is_25D : bool = false

onready var locked : bool = false

const UP_KEY    = "up"
const LEFT_KEY  = "left"
const RIGHT_KEY = "right"
const DOWN_KEY  = "down"

const WALK_KEY = "walk"

func is_pressing_up():
	if locked: return false
	return Input.is_action_pressed(UP_KEY)
func is_pressing_left():
	if locked: return false
	return Input.is_action_pressed(LEFT_KEY)
func is_pressing_right():
	if locked: return false
	return Input.is_action_pressed(RIGHT_KEY)
func is_pressing_down():
	if locked: return false
	return Input.is_action_pressed(DOWN_KEY)

func is_pressing_walk():
	if locked: return false
	return Input.is_action_pressed(WALK_KEY)

func is_just_pressed_up():
	if locked: return false
	return Input.is_action_just_pressed(UP_KEY)
func is_just_pressed_left():
	if locked: return false
	return Input.is_action_just_pressed(LEFT_KEY)
func is_just_pressed_right():
	if locked: return false
	return Input.is_action_just_pressed(RIGHT_KEY)
func is_just_pressed_down():
	if locked: return false
	return Input.is_action_just_pressed(DOWN_KEY)

func is_just_pressed_roll():
	if locked: return false
	return keys.is_just_pressed_roll()
func is_just_pressed_jump():
	if locked: return false
	return keys.is_just_pressed_jump()
func is_pressed_jump():
	if locked: return false
	return keys.is_pressed_jump()

func is_just_released_roll():
	if locked: return false
	return keys.is_just_released_roll()
func is_just_released_jump():
	if locked: return false
	return keys.is_just_released_jump()

func is_just_pressed_menu():
	if locked: return false
	return keys.is_just_pressed_menu()

func is_just_pressed_camL():
	if locked: return false
	return keys.is_just_pressed_camL()

func is_just_pressed_camR():
	if locked: return false
	return keys.is_just_pressed_camR()

func get_analog_joystick_value() -> Vector2:
	if locked: return Vector2.ZERO
	return keys.get_analog_joystick_value()

func _ready():
	Input.connect("joy_connection_changed", self, "_on_joy_connection_changed")
	_on_joy_connection_changed(0, Input.is_joy_known(0))
	set_physics_process(false)
	set_process(false)

func _on_joy_connection_changed(device_id, connected):
	if device_id == 0:
		pad_player1_connected = connected
		if MySystem.is_mobile():
			if connected:
				keys = $PhysicalKeys
			else:
				keys = $VirtualKeys
	
