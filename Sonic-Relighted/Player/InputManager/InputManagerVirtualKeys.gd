extends Node

onready var host = get_parent()

func get_analog_joystick_value() -> Vector2:
	var r = Instances.virtual_controller.get_pad_value()
	if host.is_25D:
		r.y = .0
	return r

func is_just_pressed_jump():
	return Instances.virtual_controller.is_button1_just_pressed()

func is_just_released_jump():
	return Instances.virtual_controller.is_button1_just_released()

func is_pressed_jump():
	return Instances.virtual_controller.is_button1_pressed()

func is_just_pressed_roll() -> bool:
	return Instances.virtual_controller.is_button2_just_pressed()

func is_just_released_roll() -> bool:
	return Instances.virtual_controller.is_button2_just_released()

func is_just_pressed_menu() -> bool:
	return Instances.virtual_controller.is_menu_just_pressed()

func is_just_pressed_camL() -> bool:
	return Instances.virtual_controller.is_button3_just_pressed()

func is_just_pressed_camR() -> bool:
	return Instances.virtual_controller.is_button4_just_pressed()
