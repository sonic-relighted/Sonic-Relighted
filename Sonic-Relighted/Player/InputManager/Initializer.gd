extends Node
#
# En C# no existe Input.connect, entonces habria que cargar este script
#

func _ready():
	Input.connect(
		"joy_connection_changed",
		get_parent(),
		"on_joy_connection_changed")
