extends Area

onready var host = get_parent().get_parent().get_parent()

func _on_EnableableArea_area_entered(area):
	if area.is_in_group("ElectricAtraction"):
		if !area.host.is_destroyed \
		and host.is_shielded \
		and host.shield_type == 2:
			area.enable(host)
	elif area.is_in_group("OrnamentArea"):
		area.enable()
	elif area.is_in_group("AttackArea"):
		area.enable(host)
	else:
		EnableableManager.add(EnableableManager.Action.ENABLE, area)

func _on_EnableableArea_area_exited(area):
	if area.is_in_group("AttackArea"):
		area.disable(host)
	elif area.is_in_group("OrnamentArea"):
		area.disable()
	elif area.is_in_group("ElectricAtraction"):
		pass
	else:
		EnableableManager.add(EnableableManager.Action.DISABLE, area)
