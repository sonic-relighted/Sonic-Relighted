extends Node

class_name PlayerResetable

onready var host : Player = get_parent()

func _ready():
	set_process(false)
	set_physics_process(false)

func reset():
	var start_point = Instances.start_point
	host.input_manager.locked = false
	host.global_transform.origin = start_point.global_transform.origin
	host.current_speed = .0
	host.velocity = Vector3.ZERO
	host.protected_time = host.damage_protect_time
	host.is_protected = true
	host.rail = null
	host.accelerator = null
	host.is_jumping = false
	host.is_pushing = false
	#host.is_grounded = true
	host.is_on_loop = false
	host.is_wall_walking = false
	if host.state_machine.current_state != host.state_machine.preinitialized_state:
		host.state_machine.current_state = host.state_machine.grounded_state
	if ProgressSingletone.game_slot_data.shield != -1:
		ProgressSingletone.active_shield = ProgressSingletone.game_slot_data.shield
	host.vfx.disable_shield()
	if ProgressSingletone.active_shield > -1:
		host.vfx.enable_shield(ProgressSingletone.active_shield)
		host.is_shielded = true
		host.shield_type = ProgressSingletone.active_shield
	else:
		host.is_shielded = false
		host.shield_type = 0
	host.set_process(true)
	host.set_physics_process(true)
#	Instances.hud.set_process(true)
	ProgressSingletone.ingame_rings = 0
	ProgressSingletone.ingame_rings_to_life = ProgressSingletone.RINGS_TO_LIFE
#	ProgressSingletone.ingame_stars = [false, false, false, false, false]
	Instances.hud.update_all()
	var angle : float = Instances.start_point.rotation.y
	host.current_angle = angle
	host.model.rotation.y = angle
	Instances.camera.initialize(host, angle + Instances.start_point.direction * PI)

