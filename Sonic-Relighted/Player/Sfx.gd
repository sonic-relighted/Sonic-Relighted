extends Node

onready var loss_rings       : AudioStreamPlayer = $LossRings
onready var jump             : AudioStreamPlayer = $Jump
onready var electric_jump    : AudioStreamPlayer = $ElectricJump
onready var fire_jump        : AudioStreamPlayer = $FireJump
onready var bubble_stomp     : AudioStreamPlayer = $BubbleStomp
onready var underwater       : AudioStreamPlayer = $Underwater
onready var underwater_air   : AudioStreamPlayer = $UnderwaterAir
onready var spindash_begin   : AudioStreamPlayer = $SpindashBegin
onready var spindash_release : AudioStreamPlayer = $SpindashRelease
onready var item_ring_l      : AudioStreamPlayer = $ItemRingLeft
onready var item_ring_r      : AudioStreamPlayer = $ItemRingRight
onready var extra_life       : AudioStreamPlayer = $ExtraLife
onready var shield           : AudioStreamPlayer = $Shield
onready var shield_lost      : AudioStreamPlayer = $ShieldLost
onready var splash_in        : AudioStreamPlayer = $SplashIn
onready var splash_out       : AudioStreamPlayer = $SplashOut

onready var last_item_ring : AudioStreamPlayer = item_ring_r

func _ready():
	set_process(false)
	set_physics_process(false)

func play_loss_rings():
	if !loss_rings.playing: loss_rings.play()

func play_jump():
	if jump.playing: jump.stop()
	jump.play()

func play_electric_jump():
	if electric_jump.playing: electric_jump.stop()
	electric_jump.play()

func play_fire_jump():
	if fire_jump.playing: fire_jump.stop()
	fire_jump.play()

func play_bubble_stomp():
	if bubble_stomp.playing: bubble_stomp.stop()
	bubble_stomp.play()

func play_underwater_air():
	if underwater_air.playing: underwater_air.stop()
	underwater_air.play()

func play_spindash_begin():
	spindash_begin.play()

func stop_spindash_begin():
	if spindash_begin.playing: spindash_begin.stop()

func play_spindash_release():
	if spindash_release.playing: spindash_release.stop()
	spindash_release.play()

func play_item_ring():
	if last_item_ring == item_ring_l:
		last_item_ring = item_ring_r
	else:
		last_item_ring = item_ring_l
	last_item_ring.play()

func play_extra_life():
	if extra_life.playing: extra_life.stop()
	Instances.music.pause()
	extra_life.play()

func play_underwater():
	if not underwater.playing:
		Instances.music.pause()
		underwater.play()

func stop_underwater():
	if underwater.playing:
		underwater.stop()

func play_shield():
	shield.play()

func play_shield_lost():
	shield_lost.play()

func play_splash_in():
	if splash_in.playing: splash_in.stop()
	splash_in.play()

func play_splash_out():
	if splash_out.playing: splash_out.stop()
	splash_out.play()

func _on_ExtraLife_finished():
	Instances.music.resume_with_fade_in()

func _on_Underwater_finished():
	Instances.music.resume_with_fade_in()
