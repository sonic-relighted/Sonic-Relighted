extends Area

export(NodePath) var parent_path : NodePath

onready var host : Player = get_node(parent_path)
onready var enemies : Array = []
onready var damages : Array = []

func _on_DamageArea_body_entered(body):
	if body.is_in_group("Enemy"):
		enemies.append(body)
		_touch_enemy(body)
		set_physics_process(true)
	elif body.is_in_group("Item"): _touch_item(body)
	elif body.is_in_group("Destructible"): _touch_destructible(body)
	elif body.is_in_group("EnemyMissile"): _touch_enemy_missile(body)

func _on_DamageArea_body_exited(body):
	if body.is_in_group("Enemy"):
		enemies.erase(body)
		if enemies.size() < 1 and damages.size() < 1: set_physics_process(false)
	elif body.is_in_group("Item"): pass
	elif body.is_in_group("Destructible"): pass
	elif body.is_in_group("EnemyMissile"): pass

func _on_DamageArea_area_entered(area):
	if area.is_in_group("DamageArea"):
		damages.append(area)
		_touch_damage(area)
		set_physics_process(true)

func _on_DamageArea_area_exited(area):
	if area.is_in_group("DamageArea"):
		damages.erase(area)
		if enemies.size() < 1 and damages.size() < 1: set_physics_process(false)

func _physics_process(delta : float):
	for body in enemies: _touch_enemy(body)

func _touch_enemy(body):
	if body.is_destroyed:
		enemies.erase(body)
		return
	if host.state_machine.is_attack():
		host.collisions_manager.kill_enemy(body)
	else:
		host.collisions_manager.touch_enemy(body)

func _touch_damage(area):
	host.collisions_manager.touch_damage(area)

func _touch_item(body):
	if host.state_machine.is_attack():
		host.collisions_manager.open_item(body)

func _touch_destructible(body):
	if host.state_machine.is_attack():
		host.collisions_manager.destroy_destructible(body)

func _touch_enemy_missile(body):
	host.collisions_manager.touch_enemy(body)
	body.queue_free()

func _ready():
	set_physics_process(false)
