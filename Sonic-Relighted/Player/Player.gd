extends KinematicBody

class_name Player, "res://RelightedEngine/EditorIcons/Player.png"

signal death
signal complete_level

signal physics_area_enter
signal physics_area_exit

signal lost_shield
signal hi_speed(active)

export(float) var max_time_slowly_walking_on_wall : float = .2
export(float) var damage_protect_time : float             = 2.5
export(float) var invencible_time : float                 = 10

onready var physics         : PhysicsValues = $PhysicsValues/Default
onready var default_physics : PhysicsValues = $PhysicsValues/Default
onready var physics_container = $PhysicsValues

onready var animator = $Character/AnimationModel

# Core
onready var input_manager = $InputManager
onready var floor_manager = $FloorManager
onready var collisions_manager    = $CollisionsManager
onready var rotation_sync_manager = $RotationSyncManager
onready var floor_detection_limit = $Character/SonicModel/FloorDetectionLimit
onready var state_machine = $StateMachine
onready var sfx           = $Sfx
onready var vfx           = $Vfx
onready var resetable     = $Resetable
onready var recolections  = $Recolections
onready var jump_button_msec_pressed : float = OS.get_ticks_msec() + 2000 # Para entrar en on_ground_state estando pulsando intro

# Cast
onready var floor_raycast : RayCast = $Character/FloorRayCast
onready var push_raycast  : RayCast = $Character/SonicModel/PushRayCast
onready var left_raycast  : RayCast = $Character/SonicModel/LeftRayCast  #para no subir por las paredes lateralmente
onready var right_raycast : RayCast = $Character/SonicModel/RightRayCast #para no subir por las paredes lateralmente

# Character
onready var character                    = $Character
#onready var collider                     = $CollisionShape
onready var model                   : Spatial = $Character/SonicModel
onready var spindash_smoke          : Particles = $Character/SonicModel/SpindashSmoke
onready var spindash_smoke_timer    : Timer = $Character/SonicModel/SpindashSmoke/Timer
onready var stomp_collider          : CollisionShape = $Character/SonicModel/StompArea/CollisionShape
onready var default_collider        : CollisionShape = $Character/SonicModel/DamageArea/Default
onready var rolling_collider        : CollisionShape = $Character/SonicModel/DamageArea/Rolling
onready var homing_area             : Area      = $Character/SonicModel/HomingArea
onready var homing_aim              : Sprite3D  = $Character/SonicModel/HomingAim
onready var invencibility_particles : Particles = $Vfx/InvencibilityParticles

# State
onready var slide_velocity       : Vector3 = Vector3.ZERO
onready var velocity             : Vector3 = Vector3.ZERO
onready var floor_normal         : Vector3 = Vector3.ZERO
#onready var wind_force           : Vector3 = Vector3.ZERO
onready var current_speed        : float = .0
onready var current_angle        : float = PI
onready var current_updown       : float = 1.0
onready var calculated_direction : float = .0
onready var time_slowly_walking_on_wall : float = .0
onready var deceleracion_subida  : float = 1.0
onready var is_jumping           : bool = false
onready var is_pushing           : bool = false
onready var is_grounded          : bool = true
onready var is_on_loop           : bool = false
onready var is_on_fan            : bool = false
onready var is_bouncing          : bool = false #bumper
onready var is_wall_walking      : bool = false
onready var wall_walking_timer   : float = .0
onready var end_fall             : bool = false # Esto vale true justo despues de caer. Se calcula si hay una para brusca de velocidad vertical
onready var rail                 : PathFollow = null # Podemos estar sobre un rail
onready var accelerator          : Accelerator = null # Podemos estar en un acelerador
onready var collision            : KinematicCollision = null # Colision con otros cuerpos
onready var is_colliding         : bool = false # Colision con otros cuerpos
onready var is_protected         : bool = false # Al perder anillos estamos un rato invencibles
onready var protected_time       : float = damage_protect_time
onready var is_shielded          : bool = false # Tenemos puesto un escudo
onready var shield_type          : int = 0 # normal, homing...
onready var is_hi_speed          : bool = false # Monotor de velocidad
onready var is_invencible        : bool = false # Monitor de inmunibilidad

func _ready():
	push_raycast.add_exception(character)
	push_raycast.add_exception(model)
	push_raycast.add_exception(self)

	floor_raycast.add_exception(character)
	floor_raycast.add_exception(model)
	floor_raycast.add_exception(self)
	
	for spring in get_tree().get_nodes_in_group("Spring"):
		# El children 2 es el static body
		floor_raycast.add_exception(spring)

	for destr in get_tree().get_nodes_in_group("Destructible"):
		floor_raycast.add_exception(destr)
	
	for enemy in get_tree().get_nodes_in_group("Enemy"):
		push_raycast.add_exception(enemy)
	
#	# buffffff...
#	var m : SpatialMaterial = load("res://Player/Blender/Sonic DavidKBD.material")
#	m.flags_transparent = false
#	yield(get_tree(), "idle_frame")
#	m.flags_transparent = true

func _process(delta):
	state_machine.step_animation(self, delta)
	if is_protected: _protected_animation(delta)
	if translation.y < Instances.level.get_min_position_y():
		_death()

func _physics_process(delta):
	if input_manager.is_just_pressed_menu():
		Instances.hud.show_pause_menu()
	if input_manager.is_just_pressed_camL():
		Instances.camera.rotation_manager.move_l()
	if input_manager.is_just_pressed_camR():
		Instances.camera.rotation_manager.move_r()

	_check_pushing()
	floor_manager.check_ground(delta)
	state_machine.step(self, delta)
	if is_grounded and is_shielded:
		var collider = floor_raycast.get_collider()
		if shield_type == 1 and collider.is_in_group("Fireable"):
			collider.destroy()
		elif shield_type == 3 and collider.is_in_group("Frozeable"):
			collider.freeze()

func _check_pushing():
	is_pushing =  push_raycast.is_colliding()

func _on_animation_finished(anim_name : String):
	state_machine.on_animation_finished(anim_name)

func _on_Timer_timeout():
	spindash_smoke.emitting = false

func _protected_animation(delta : float):
	if int(protected_time * 100) % 2 == 0:
		if character.is_visible_in_tree():
			character.hide()
		else:
			character.show()
	protected_time -= delta
	if protected_time <= 0:
		is_protected = false
		character.show()

func reset():
	resetable.reset()

func recolect(element : Spatial):
	recolections.recolect(element)

func damage(dir : Vector3) -> bool:
	if is_protected or is_invencible: return false
	if is_shielded:
		_loss_shield()
		_protect()
		velocity = dir
		state_machine._change_state(self, state_machine.damage_state)
	elif ProgressSingletone.ingame_rings > 0:
		_loss_rings()
		_protect()
		velocity = dir
		state_machine._change_state(self, state_machine.damage_state)
	else:
		_protect()
		_death()
		velocity = dir
		state_machine._change_state(self, state_machine.damage_state)
	return true

func _death():
	ProgressSingletone.game_slot_data.lifes -= 1
	ProgressSingletone.active_shield = -1
	ProgressSingletone.game_slot_data.shield = -1
	# la camara se vuelve loca, creo que por poner en pausa...
	velocity = Vector3.ZERO
	emit_signal("death")
	set_physics_process(false)
	set_process(false)
	animator.do("Death", 1, true)
	model.rotation.y = Instances.camera.rotation.y
	vfx.disable_shield()
	is_shielded = false
	shield_type = 0
	homing_aim.stop()
	is_hi_speed = false
	is_invencible = false
	vfx.stop_stomp()

func _loss_rings():
	sfx.play_loss_rings()
	var i = 0
	var count = min(32.0, ProgressSingletone.ingame_rings)
	ProgressSingletone.ingame_rings = 0
	ProgressSingletone.ingame_rings_to_life = ProgressSingletone.RINGS_TO_LIFE
	Instances.hud.update_rings()
	var position = global_transform.origin
	while i < count:
		var angle = i * (0.19635 + AngleConstants.SEMI_PI)
		var direction = Vector3(sin(angle), 1.5, cos(angle))
		var new_position = position + direction * .25
		Builder.build_bouncing_ring(
				Instances.level,
				new_position,
				false,
				direction * 2.0)
		i += 1
		if (i % 2) == 0:
			yield(get_tree(), "idle_frame")

func _loss_shield():
	sfx.play_shield_lost()
	vfx.disable_shield()
	is_shielded = false
	ProgressSingletone.game_slot_data.shield = -1

func _protect():
	is_protected = true
	protected_time = damage_protect_time

func _on_VisibilityNotifier_camera_exited(camera):
	if camera == Instances.camera.camera:
		Instances.camera.rotation_manager.notify(self)
