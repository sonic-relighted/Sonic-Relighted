extends Area

export(NodePath) var parent_path : NodePath

onready var host : Player = get_node(parent_path)

func _on_StompArea_body_entered(body):
	if body.is_in_group("Enemy"):
		host.collisions_manager.kill_enemy(body)
		return
	if body.is_in_group("Item"):
		host.collisions_manager.open_item(body)
	elif body.is_in_group("Destructible"):
		host.collisions_manager.destroy_destructible(body)
	elif body.is_in_group("Stompable"):
		host.collisions_manager.destroy_stompable(body)
