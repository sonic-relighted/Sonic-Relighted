extends PlayerState

onready var state_machine = get_parent()

var impulsed : bool
var end      : bool

func is_attack() -> bool: return false

func enter(host : Player):
	end = false
	impulsed = host.velocity != Vector3.ZERO 
	host.rotation = Vector3.ZERO
	host.default_collider.disabled = false
	host.rolling_collider.disabled = true

func exit(host : Player):
	host.rotation = Vector3.ZERO

func on_animation_finished(anim_name : String):
	end = true

func step_animation(host : Player, delta : float):
	host.animator.do("Damage", 1, true)

func step(host : Player, delta : float):
	if end: return state_machine.grounded_state
	host.velocity.y -= host.physics.GRAVITY
	if host.is_on_loop: return host.state_machine.loop_state
	return self

func step_move(host : Player, delta : float):
	var fall_vel = host.velocity.y
	host.velocity = host.move_and_slide(host.velocity, host.floor_normal, false, 4, PI/4, false)
	host.end_fall = fall_vel < -.9 and fall_vel - host.velocity.y < 0
