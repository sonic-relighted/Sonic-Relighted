extends PlayerState

onready var state_machine = get_parent()

var acting
var jump
var roll

func is_attack() -> bool: return false

func enter(host : Player):
	acting = false
	jump = false
	roll = false
	host.default_collider.disabled = false
	host.rolling_collider.disabled = true

func exit(host : Player):
	pass

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	host.animator.do("Push", 1, true)

func step(host : Player, delta : float):
#	var point : Vector2 = Vector2(host.push_raycast.get_collision_point().x, host.push_raycast.get_collision_point().z)
#	var me : Vector2 = Vector2(host.translation.x, host.translation.z)
#	host.current_angle = (point - me).angle()
	if jump: return state_machine.jump_state
	if roll: return state_machine.spindash_state
	if !host.is_pushing: return state_machine.grounded_state

	_control(host, delta)
	_angle(host, delta)
	host.current_speed = .0

	var obj = host.push_raycast.get_collider()
	if acting and obj.is_in_group("Push"):
		obj.push(host, host.push_raycast.get_collision_point())
		var point : Vector3 = host.push_raycast.get_collision_point()
		var diff : Vector2 = Vector2(
				point.x - host.translation.x,
				point.z - host.translation.z)
		host.velocity.x = diff.x * 10
		host.velocity.z = diff.y * 10
	else:
		host.velocity.x = .0
		host.velocity.z = .0
	if host.is_shielded:
		if host.shield_type == 1 and obj.is_in_group("Fireable"):
			obj.destroy()

	host.velocity += Vector3.DOWN.rotated(Vector3(1,0,0), host.rotation.x).rotated(Vector3(0,0,1), host.rotation.z)

	return self

func _angle(host : Player, delta : float):
	host.model.rotation.y  = lerp_angle(
			host.model.rotation.y,
			Instances.camera.rotation.y - host.current_angle + PI,
			delta * 10.0)

func _control(host : Player, delta : float):
	var previous_angle = host.current_angle
	var inp : PlayerInputManager = host.input_manager
	
	var analog = host.input_manager.get_analog_joystick_value()
	var angle = analog.angle() + AngleConstants.SEMI_PI
	var length = analog.length()
	acting = false
	if length > .1:
		host.current_angle = angle
		acting = true
	if inp.is_just_pressed_jump():
		jump = true
	elif inp.is_just_pressed_roll():
		roll = true

func step_move(host : Player, delta : float):
	var fall_vel = host.velocity.y
	host.velocity = host.move_and_slide(host.velocity, host.floor_normal, false, 4, PI/4, false)
	host.end_fall = fall_vel < -.9 and fall_vel - host.velocity.y < 0
