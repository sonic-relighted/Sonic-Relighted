extends Node

class_name PlayerState

func is_attack() -> bool:
	return false

func enter(host):
	pass

func exit(host):
	pass

func step_animation(host, delta):
	pass

func step(host, delta):
	pass

func step_move(host, delta):
	pass

func on_animation_finished(anim_name : String):
	pass
