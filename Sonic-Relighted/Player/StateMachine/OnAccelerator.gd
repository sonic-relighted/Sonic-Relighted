extends PlayerState

const SECONDS_SOUND   : float = .4

onready var state_machine = get_parent()
onready var inp = $"../../InputManager"

var is_in_position       : bool
var started_acceleration : bool
var started_spindash     : bool
var propulsion           : bool
var dest_position        : Vector3
var global_timer         : float
var spindash_timer       : float
var spindash_count       : int

func is_attack() -> bool: return false

func enter(host : Player):
	is_in_position = false
	started_acceleration = false
	started_spindash = false
	propulsion = false
	dest_position = host.accelerator.get_player_position()
	global_timer = .0
	spindash_timer = .0
	spindash_count = 0

	# esto es porque grounded es el estado por defecto
	# y aun no se ha cargado el player cuando se ejecuta esto
	# pero nada mas empezar entra en juego el estado fall
	# asi que nos da igual...
	if host.default_collider != null:
		host.default_collider.disabled = false
		host.rolling_collider.disabled = true

func exit(host : Player):
	host.current_speed = float(host.physics.ROLL_MAX_SPEED)# / 1.5
	host.spindash_smoke.emitting = false
	host.accelerator = null
	host.sfx.play_spindash_release()
	
	var updown = ((sin(host.calculated_direction) * host.rotation.z) + (sin(host.calculated_direction - AngleConstants.SEMI_PI) * host.rotation.x)) / PI
	var speed = -host.current_speed
	var movement = AngleConstants.convert_angles(speed, host.calculated_direction, updown)
	host.velocity = lerp(host.velocity, movement, .5)
	host.velocity -= host.floor_normal
	print("Exit   ", host.current_angle, "    ", host.current_angle)

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	if is_in_position and started_spindash:
		host.spindash_smoke.emitting = true
		host.animator.do_loop("Jump", 1.5, true)
#	elif is_in_position and not started_spindash:
#		host.animator.do("PrepareAccelerator", 1, true) #do_loop("PrepareAcceleration", 1, true)
	else:
		var speed = 1 + (host.current_speed * 1.2) / host.physics.MAX_SPEED
		if speed > 2:
			host.animator.do("Run", speed, true)
		elif speed > 1:
			host.animator.do("Walk", speed, true)
		else:
			host.animator.do("Idle", speed, true)

func step(host : Player, delta : float):
	_control_walk(host, delta)
	if host.accelerator == null: return state_machine.grounded_state
	if is_in_position and not started_acceleration:
		host.accelerator.started_acceleration()
	if is_in_position:
		_control_spindash(host, delta)
		started_acceleration = true
	if propulsion: return state_machine.rollinginf_state
	
	global_timer += delta

	_angle(host, delta)
#	_update_speed(host, delta)
	var movement = AngleConstants.convert_angles(
			-host.current_speed,
			host.calculated_direction,
			host.current_updown)
	host.velocity = lerp(host.velocity, movement, .2)
	host.velocity += Vector3.DOWN.rotated(
			Vector3(1,0,0),
			host.rotation.x).rotated(Vector3(0,0,1),
			host.rotation.z)
	
	var abs_rot_x = abs(host.rotation.x)
	var abs_rot_z = abs(host.rotation.z)
	if host.current_updown <= 2.0:
		host.deceleracion_subida = 1.0
	else:
		host.deceleracion_subida += delta * 10
	if (abs_rot_x < 1 and abs_rot_z < 1) or host.current_speed > 2.0:
		host.time_slowly_walking_on_wall = .0
	elif host.time_slowly_walking_on_wall < host.max_time_slowly_walking_on_wall:
		host.time_slowly_walking_on_wall += delta
	else:
		host.velocity += Vector3.UP \
				.rotated(Vector3.RIGHT, host.rotation.x) \
				.rotated(Vector3.BACK, host.rotation.z)
		return host.state_machine.fall_state

	if !host.is_grounded: return host.state_machine.fall_state
	
	host.rotation_sync_manager.align_to_floor(delta)
	
	return self

func _control_walk(host : Player, delta : float):
	var diff : Vector3 = host.translation - dest_position
	var camera_angle = Instances.camera.rotation.y - AngleConstants.SEMI_PI

	if diff.length() < 0.1:
		if not is_in_position:
			host.global_transform.origin = host.accelerator.player_position.global_transform.origin
		is_in_position = true
	if is_in_position:
		host.current_angle = host.accelerator.rotation.y + PI + camera_angle
		host.current_speed = .0
		host.velocity = Vector3.ZERO
	else:
		host.current_angle = atan2(diff.z, diff.x) + camera_angle
		host.current_speed = diff.length() * 10

func _control_spindash(host : Player, delta : float):
	if spindash_count < 3:
		if started_spindash:
			spindash_timer -= delta
			if spindash_timer <= .0:
				spindash_count += 1
				spindash_timer = SECONDS_SOUND
				host.sfx.play_spindash_begin()
	else:
		host.spindash_smoke.emitting = false

func _angle(host : Player, delta : float):
	host.model.rotation.y  = lerp_angle(
			host.model.rotation.y,
			Instances.camera.rotation.y - host.current_angle + PI,
			delta * 10.0)

func step_move(host : Player, delta : float):
	var fall_vel = host.velocity.y
	host.velocity = host.move_and_slide(host.velocity, host.floor_normal, false, 4, PI/4, false)
	host.end_fall = fall_vel < -.9 and fall_vel - host.velocity.y < 0

####################### Signals emitidas por el acelerador #######################

func do_begin():
	pass

func do_spindash():
	started_spindash = true
	print("AAAAAAAAAAAAaa")

func do_exit():
	propulsion = true

func do_end():
	pass
