extends PlayerState

onready var state_machine = get_parent()

func is_attack() -> bool: return false

func enter(host : Player):
	host.rotation = Vector3.ZERO
	host.default_collider.disabled = false
	host.rolling_collider.disabled = true
	host.animator.do("WinF", 1, true)

func exit(host : Player):
	host.rotation = Vector3.ZERO

func step(host : Player, delta : float):
	host.velocity = Vector3.ZERO
	host.current_speed = .0
	return self

