extends PlayerState

onready var state_machine = get_parent()

var camera_rotation_y
var previous_state

func is_attack() -> bool: return false

func enter(host : Player):
	if previous_state == state_machine.rolling_state:
		previous_state = state_machine.rolling_state
	else:
		previous_state = state_machine.grounded_state
		
	camera_rotation_y = Instances.camera.rotation.y
	host.velocity = Vector3.ZERO
	host.current_speed = .0

func exit(host : Player):
#	if previous_state == host.state_machine.rolling_state: 
#		host.current_speed = host.physics.ROLL_MAX_SPEED
#	else:
#		host.current_speed = host.physics.MAX_SPEED
#
#	host.velocity = AngleConstants.convert_angles(
#			-host.current_speed,
#			host.calculated_direction,
#			host.current_updown)
#	host.velocity += Vector3.DOWN \
#			.rotated(Vector3.RIGHT, host.rotation.x) \
#			.rotated(Vector3.BACK, host.rotation.z)
	host.velocity = Vector3.ZERO
	host.current_speed = .0

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	var anim_speed : float = 1 + (host.physics.LOOP_SPEED * 1.2) / host.physics.MAX_SPEED
	if previous_state == host.state_machine.rolling_state:
		host.animator.do_loop("Jump", anim_speed, true)
	else:
		host.animator.do("Run", anim_speed, true)

func step(host : Player, delta : float):
	if host.rail != null:
		var speed = host.physics.LOOP_SPEED
		var new_offset = host.rail.offset + speed * delta
		host.rail.offset += speed * delta
		host.current_updown = host.rail.unit_offset * AngleConstants.TWO_PI + AngleConstants.SEMI_PI
		host.rotation.z = -host.current_updown + AngleConstants.SEMI_PI

	host.current_angle = camera_rotation_y - AngleConstants.RIGHT
	_angle(host, delta)

	if !host.is_on_loop:
		return previous_state
	return self

func _angle(host : Player, delta : float):
	host.model.rotation.y  = lerp_angle(
			host.model.rotation.y,
			camera_rotation_y - host.current_angle + PI,
			delta * 10.0)

func step_move(host : Player, delta : float):
	pass
