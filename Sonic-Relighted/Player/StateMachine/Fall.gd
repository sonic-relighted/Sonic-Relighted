extends PlayerState

onready var state_machine = get_parent()

var time_falling : float
var pressing_timer : float # Tiempo presionando direccion
var previous_state
var can_homing : bool

func is_attack() -> bool: return previous_state.is_attack()

func enter(host : Player):
	time_falling = .0
	pressing_timer = .0
	previous_state = state_machine.previous_state
	if is_attack():
		host.default_collider.disabled = true
		host.rolling_collider.disabled = false
	else:
		host.default_collider.disabled = false
		host.rolling_collider.disabled = true

	can_homing = false
	if host.is_shielded and host.shield_type == 1:
		host.homing_aim.start()
	host.is_grounded = false

func exit(host : Player):
	host.rotation = Vector3.ZERO # Seria mejor obtener la el floor normal para evitar out bounds y tal...
	host.floor_manager.get_floor()
	host.homing_aim.stop()

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	if is_attack():
		host.animator.do_loop("Jump", 2, true)
	else:
		host.animator.do("Fall", 1, true)

func step(host : Player, delta : float):
	can_homing = time_falling > host.physics.HOMING_LOCKTIME
	if time_falling < .2: host.is_grounded = false
	time_falling += delta
	if host.is_on_loop: return host.state_machine.loop_state
	if host.is_grounded:
		host.rotation = Vector3.ZERO
		return state_machine.grounded_state
	
#	host.rotation = lerp(host.rotation, Vector3.ZERO, .2)
	_reset_rotation(host, delta)
	
	_process_jump(host, delta)
	return _control_jump(host, delta)

func _reset_rotation(host : Player, delta : float):
	var speed = delta * 2
	host.rotation.x = lerp_angle(host.rotation.x, .0, speed)
	host.rotation.y = lerp_angle(host.rotation.y, .0, speed)
	host.rotation.z = lerp_angle(host.rotation.z, .0, speed)

func _control_jump(host : Player, delta : float):
	var inp = host.input_manager
	if inp.is_just_pressed_jump(): host.jump_button_msec_pressed = OS.get_ticks_msec()
	
	var joy = inp.get_analog_joystick_value()
	if joy.length() > .1:
		pressing_timer += delta
	else:
		pressing_timer = .0
	var movement = joy \
			.rotated(-Instances.camera.rotation.y) * _calc_air_control(host)
	var new_x = host.velocity.x + movement.x
	var new_z = host.velocity.z + movement.y
	var length = sqrt(pow(new_x, 2) + pow(new_z, 2))
	if length < host.physics.AIR_MAX_SPEED:
		host.velocity.x = new_x
		host.velocity.z = new_z
	if inp.is_just_pressed_jump():
		if host.is_shielded:
#			if host.shield_type == 0:
#				return XXXX
			if can_homing and host.shield_type == 1:
				var nearest = host.homing_aim.target
				if nearest == null:
					print("Play SFX de que no hay enemigo cerca")
					return self
				host.state_machine.homing_attack_state.target = nearest
				return host.state_machine.homing_attack_state
			if host.shield_type == 2:
				return host.state_machine.double_jump_state
	return self

func _calc_air_control(host : Player):
	var r = host.physics.FALL_AIR_CONTROL
	var factor = 3.0 - 4.0 * pressing_timer
	if pressing_timer < .25:
		factor *= pressing_timer * 4.0
	if factor > 1.0:
		r *= factor
	return r

func _process_jump(host : Player, delta : float):
	if not host.is_on_fan:
		host.velocity.y -= host.physics.GRAVITY

func step_move(host : Player, delta : float):
	var fall_vel = host.velocity.y
	host.velocity = host.move_and_slide(host.velocity, host.floor_normal, false, 4, PI/4, false)
	host.end_fall = fall_vel < -.9 and fall_vel - host.velocity.y < 0
