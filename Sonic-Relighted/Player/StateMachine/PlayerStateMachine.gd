extends Node

class_name PlayerStateMachine, "res://RelightedEngine/EditorIcons/State.png"

onready var preinitialized_state = $PreInitialized
onready var grounded_state       = $Grounded
onready var jump_state           = $Jump
onready var stomp_state          = $Stomp
onready var homing_attack_state  = $HomingAttack
onready var double_jump_state    = $DoubleJump
onready var bubble_stomp_state   = $BubbleStomp
onready var fall_state           = $Fall
onready var underwater_breath_state = $UnderwaterBreath
onready var spindash_state       = $Spindash
onready var rolling_state        = $Rolling
onready var rollinginf_state     = $RollingInf # Al salir disparado de un acelerador, por ejemplo
onready var loop_state           = $Loop
onready var onaccelerator_state  = $OnAccelerator
onready var pushing_state        = $Pushing
onready var victory_state        = $Victory
onready var damage_state         = $Damage
onready var cannon_throw_state   = $CannonThrow

#####
#####onready var debug                = $Debug
#####

onready var current_state = preinitialized_state
onready var previous_state = current_state

func _ready():
	current_state.enter(get_parent())
	set_process(false)
	set_physics_process(false)

func on_animation_finished(anim_name : String):
	current_state.on_animation_finished(anim_name)

func is_attack():
	return current_state.is_attack()

func step_animation(host : Player, delta : float):
	current_state.step_animation(host, delta)

func step(host : Player, delta : float):
	# Calcular esto aqui me da un poco de fuego en los ojos
	host.calculated_direction = Instances.camera.rotation.y - host.current_angle
#	current_state = debug
	var next_state = current_state.step(host, delta)
	current_state.step_move(host, delta)
	_change_state(host, next_state)

func _change_state(host : Player, next_state : PlayerState):
	if next_state == current_state: return
	current_state.exit(host)
	previous_state = current_state
	current_state = next_state
	current_state.enter(host)
