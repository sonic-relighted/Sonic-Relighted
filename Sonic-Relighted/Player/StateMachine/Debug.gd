extends PlayerState

onready var state_machine = get_parent()

var low_speed : bool
var is_walking : bool

func is_attack() -> bool: return true

func enter(host : Player):
	low_speed = false
	is_walking = false

func exit(host : Player):
	pass

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	pass

func step(host : Player, delta : float):

	_control_walk(host, delta)
	_angle(host, delta)
	_update_speed(host, delta)
	
	var direction = Instances.camera.rotation.y - host.current_angle
	var updown = host.current_updown
	var speed = -host.current_speed
	var movement = AngleConstants.convert_angles(speed, direction, updown)
	host.velocity = movement
	
#	print(updown)
	
	return self

func _angle(host : Player, delta : float):
	host.model.rotation.y  = lerp_angle(
			host.model.rotation.y,
			Instances.camera.rotation.y - host.current_angle + PI,
			delta * 10.0)

func _control_walk(host : Player, delta : float):
	var hz_amount = -Input.get_action_strength("left") + Input.get_action_strength("right")
	var vt_amount = -Input.get_action_strength("up") + Input.get_action_strength("down")
	var analog = Vector2(hz_amount, vt_amount)
	var angle = analog.angle() + AngleConstants.SEMI_PI
	var length = analog.length()
	host.current_angle = angle
	is_walking = false
	if length > .09:
		is_walking = true
		low_speed = length < .9

	if Input.is_action_pressed("ui_end"):
		host.current_updown = 0
	if Input.is_action_pressed("ui_page_up"):
		host.current_updown += .1
	if Input.is_action_pressed("ui_page_down"):
		host.current_updown -= .1

	return self

func _update_speed(host : Player, delta : float):
	if is_walking:
		var max_speed : float = host.physics.MAX_SPEED
		if low_speed: max_speed /= 3
		if host.current_speed < max_speed:
			host.current_speed += delta * (20 - host.current_speed)
		elif low_speed:
			host.current_speed *= .3
	else:
		if host.current_speed > 1:
			host.current_speed *= .7
		else:
			host.current_speed = 0

func step_move(host : Player, delta : float):
	var fall_vel = host.velocity.y
	host.velocity = host.move_and_slide(host.velocity, host.floor_normal, false, 4, PI/4, false)
	host.end_fall = fall_vel < -.9 and fall_vel - host.velocity.y < 0
