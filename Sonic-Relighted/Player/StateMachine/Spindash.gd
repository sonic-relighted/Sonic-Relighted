extends PlayerState

onready var state_machine = get_parent()

var is_done : bool
var timer : float

func is_attack() -> bool: return true

func enter(host : Player):
	host.is_grounded = true
	_freeze(host)
	is_done = false
	timer = 0
	host.spindash_smoke.emitting = true
	host.sfx.play_spindash_begin()
	host.default_collider.disabled = true
	host.rolling_collider.disabled = false

func exit(host : Player):
	if timer > 0:
		host.current_speed = _calculate_force(host)
		var updown = ((sin(host.calculated_direction) * host.rotation.z) + (sin(host.calculated_direction - AngleConstants.SEMI_PI) * host.rotation.x)) / PI
		var speed = -host.current_speed
		var movement = AngleConstants.convert_angles(speed, host.calculated_direction, updown)
		host.velocity = lerp(host.velocity, movement, .5)
		host.velocity -= host.floor_normal
		host.sfx.play_spindash_release()
	host.spindash_smoke_timer.start()
	host.sfx.stop_spindash_begin()

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	host.animator.do_loop("Jump", 1.5, true)

func step(host : Player, delta : float):
	timer += delta
	_freeze(host)
	_control(host, delta)
	_angle(host, delta)
	if timer > host.physics.SPINDASH_TIME:
		timer = 0
		is_done = true
	if is_done:
		return state_machine.rolling_state
	return self

func _freeze(host : Player):
	host.current_speed = 0
	host.velocity = Vector3.ZERO

func _control(host : Player, delta : float):
	var inp : PlayerInputManager = host.input_manager
	if inp.is_just_released_roll():
		is_done = true
	
	var analog = inp.get_analog_joystick_value()
	if analog.length() > .05:
		host.current_angle  = analog.angle() + AngleConstants.SEMI_PI
		
func _angle(host : Player, delta : float):
	host.model.rotation.y  = lerp_angle(
		host.model.rotation.y,
		Instances.camera.rotation.y - host.current_angle + PI,
		delta * 10.0)

func _calculate_force(host : Player) -> float:
	return timer * host.physics.ROLL_MAX_SPEED

func step_move(host : Player, delta : float):
	var fall_vel = host.velocity.y
	host.velocity = host.move_and_slide(host.velocity, host.floor_normal, false, 4, AngleConstants.QUART_PI, false)
	host.end_fall = fall_vel < -.9 and fall_vel - host.velocity.y < 0
