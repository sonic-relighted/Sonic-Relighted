extends PlayerState

onready var state_machine = get_parent()

func is_attack() -> bool: return true

func enter(host : Player):
	host.default_collider.disabled = true
	host.rolling_collider.disabled = false

func exit(host : Player):
	pass

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	var speed = 0.6 + (host.velocity.length() * 1.2) / host.physics.MAX_SPEED
	host.animator.do_loop("Jump", speed, true)

func step(host : Player, delta : float):
	if host.is_bouncing: return state_machine.fall_state
	if host.input_manager.is_just_pressed_jump(): return state_machine.jump_state
	if host.is_colliding:
		var new_normal = host.collision.normal
		var limit = 40 * delta
		if host.floor_normal == Vector3.ZERO:
			host.floor_normal = new_normal
		elif abs(new_normal.x - host.floor_normal.x) < limit and abs(new_normal.y - host.floor_normal.y) < limit and abs(new_normal.z - host.floor_normal.z) < limit:
			host.floor_normal = new_normal
		else:
			print("!")
			var velocity = host.velocity.bounce(new_normal)
			host.current_angle = Vector2(velocity.x, velocity.z).angle() + Instances.camera.rotation.y + AngleConstants.SEMI_PI

	_angle(host, delta)
	host.rotation_sync_manager.align_to_floor(delta)
	
	host.velocity.y = 0
	host.velocity = AngleConstants.convert_angles(-host.current_speed, host.calculated_direction, host.current_updown)
	
	if host.is_grounded and host.get_slide_count():
		_collide(host)

	if !host.is_grounded: return state_machine.fall_state

#	if Vector2(host.velocity.x, host.velocity.z).length() < 3:
	if host.is_on_loop: return host.state_machine.loop_state
	if host.accelerator != null: return host.state_machine.onaccelerator_state
	if host.current_speed < 1.0: return state_machine.grounded_state

	return self

func _angle(host : Player, delta : float):
	host.model.rotation.y = lerp_angle(
			host.model.rotation.y,
			Instances.camera.rotation.y - host.current_angle + PI,
			delta * 10.0)

func step_move(host : Player, delta : float):
	var slide_impact = host.default_physics.SLIDE_IMPACT if host.is_shielded and host.shield_type == 3 else host.physics.SLIDE_IMPACT
	host.slide_velocity = lerp(host.velocity, host.slide_velocity, slide_impact * delta)
	var c = host.move_and_collide(
			(host.velocity + host.slide_velocity) * .5 * delta,
			false)
	if c == null:
		host.is_colliding = false
		host.collision = null
		return

	if is_instance_valid(c.collider):
		var collider = c.collider
		if !collider.is_in_group("Item") and !collider.is_in_group("Enemy"):
			host.collision = c
			host.is_colliding = true

func _collide(host : Player):
	for i in range(host.get_slide_count()):
		var col = host.get_slide_collision(i)
		var point_col = col.position - host.floor_detection_limit.global_transform.origin
		if point_col.length() < .1:
			host.velocity += Vector3.DOWN.rotated(Vector3.RIGHT, host.rotation.x).rotated(Vector3.BACK, host.rotation.z)
