extends PlayerState

onready var state_machine = get_parent()

var is_accelerating : bool
var is_braking      : bool

func is_attack() -> bool: return true

func enter(host : Player):
	is_accelerating = false
	is_braking      = false
	host.default_collider.disabled = true
	host.rolling_collider.disabled = false

func exit(host : Player):
	pass

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	var speed = 0.6 + (host.velocity.length() * 1.2) / host.physics.ROLL_MAX_SPEED
	host.animator.do_loop("Jump", speed, true)

func step(host : Player, delta : float):
	if host.is_bouncing: return state_machine.fall_state
	var next_state = _control(host, delta)
	if host.is_colliding: _update_floor_normal(host, delta)

	_update_speed(host, delta)
	_angle(host, delta)
	host.rotation_sync_manager.align_to_floor(delta)
	
	host.velocity.y = 0
	host.velocity = AngleConstants.convert_angles(-host.current_speed, host.calculated_direction, host.current_updown)
	
	if host.is_grounded and host.get_slide_count(): _collide(host)
	
	var abs_rot_x = abs(host.rotation.x)
	var abs_rot_z = abs(host.rotation.z)
	if host.current_updown <= 2.0:
		host.deceleracion_subida = 1.0
	else:
		host.deceleracion_subida += delta * 10
	if (abs_rot_x < 1 and abs_rot_z < 1) or host.current_speed > 2.0:
		host.time_slowly_walking_on_wall = .0
	elif host.time_slowly_walking_on_wall < host.max_time_slowly_walking_on_wall:
		var time_factor = 3 - host.current_speed
		host.time_slowly_walking_on_wall += delta * time_factor
	else:
		host.velocity += Vector3.UP \
				.rotated(Vector3.RIGHT, host.rotation.x) \
				.rotated(Vector3.BACK, host.rotation.z)
		return host.state_machine.fall_state

	if !host.is_grounded: return state_machine.fall_state

#	if Vector2(host.velocity.x, host.velocity.z).length() < 3:
	if host.is_on_loop: return host.state_machine.loop_state
	if host.accelerator != null: return host.state_machine.onaccelerator_state
	if host.current_speed < 1.0: return state_machine.grounded_state

	return next_state

func _update_floor_normal(host : Player, delta : float):
	var limit = 40.0 * delta
	if host.floor_normal == Vector3.ZERO:
		host.floor_normal = host.collision.normal
	elif abs(host.collision.normal.x - host.floor_normal.x) < limit \
	and abs(host.collision.normal.y - host.floor_normal.y) < limit \
	and abs(host.collision.normal.z - host.floor_normal.z) < limit:
		host.floor_normal = host.collision.normal
	else:
		var velocity = host.velocity.bounce(host.collision.normal)
		host.current_angle = Vector2(velocity.x, velocity.z).angle() \
				+ Instances.camera.rotation.y \
				+ AngleConstants.SEMI_PI

func _update_speed(host : Player, delta : float):
	var slide_impact = host.default_physics.SLIDE_IMPACT if host.is_shielded and host.shield_type == 3 else host.physics.SLIDE_IMPACT
	if is_accelerating and host.current_speed > 1.0 and host.current_speed < host.physics.ROLL_MAX_SPEED:
		var esfuerzo_subida = 1 + cos(host.current_updown) / 10
		host.current_speed += delta * ((10 - host.current_speed) / host.deceleracion_subida)
		host.current_speed *= esfuerzo_subida
	elif is_braking and host.current_speed > .1:
		host.current_speed -= delta * host.physics.BRAKE
	elif host.current_speed > 1.0:
		host.current_speed = lerp(host.current_speed, .0, delta)
	else:
		host.current_speed = .0

func _angle(host : Player, delta : float):
	host.model.rotation.y = lerp_angle(
			host.model.rotation.y,
			Instances.camera.rotation.y - host.current_angle + PI,
			delta * 10.0)

func _control(host : Player, delta : float):
	var previous_angle : float = host.current_angle
	var new_angle      : float = previous_angle
	var is_pressing    : float = false
	
	var inp = host.input_manager
	var analog = inp.get_analog_joystick_value()
	if analog.length() > .1:
		new_angle = analog.angle() + AngleConstants.SEMI_PI
		is_pressing = true
		host.current_angle = lerp_angle(host.current_angle, new_angle, delta)
	
	var diff = abs(new_angle - previous_angle)
	is_accelerating = is_pressing and diff < AngleConstants.SEMI_PI
	is_braking      = is_pressing and diff > AngleConstants.SEMI_PI
	
	if inp.is_just_pressed_jump(): return state_machine.jump_state
	
	if !is_pressing: return self

	return self

func step_move(host : Player, delta : float):
	var slide_impact = host.default_physics.SLIDE_IMPACT if host.is_shielded and host.shield_type == 3 else host.physics.SLIDE_IMPACT
	host.slide_velocity = lerp(host.velocity, host.slide_velocity, slide_impact * delta)
	var c = host.move_and_collide(
		(host.velocity + host.slide_velocity) * .5 * delta,
		false)
	if c == null:
		host.is_colliding = false
		host.collision = null
		return

	if is_instance_valid(c.collider):
		var collider = c.collider
		if !collider.is_in_group("Item") and !collider.is_in_group("Enemy"):
			host.collision = c
			host.is_colliding = true

func _collide(host : Player):
	for i in range(host.get_slide_count()):
		var col = host.get_slide_collision(i)
		var point_col = col.position - host.floor_detection_limit.global_transform.origin
		if point_col.length() < .1:
			host.velocity += Vector3.DOWN.rotated(Vector3.RIGHT, host.rotation.x).rotated(Vector3.BACK, host.rotation.z)
