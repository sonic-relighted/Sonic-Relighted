extends PlayerState

onready var state_machine = get_parent()

var timer : float
var pressing_timer : float # Tiempo presionando direccion
var jump_state : float
var long_jump : bool
var up_direction : bool
var floor_normal : Vector3

func is_attack() -> bool: return true

func enter(host : Player):
	host.is_grounded = false
	host.is_jumping = true
	host.rotation = Vector3.ZERO
	long_jump = true
	timer = .0
	pressing_timer = .0
	jump_state = 1.0
	floor_normal = host.floor_normal.normalized()
	up_direction = true
	host.sfx.play_electric_jump()
	host.default_collider.disabled = true
	host.rolling_collider.disabled = false
	host.vfx.action_electric()
	host.velocity.y = 1.0

func exit(host : Player):
	host.is_jumping = false
	host.floor_manager.get_floor()

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	host.animator.do_loop("Jump", 1.2, true)

func step(host : Player, delta : float):
	var inp : PlayerInputManager = host.input_manager
	_process_jump(inp, host, delta)
	_control_jump(inp, host, delta)

	if inp.is_just_pressed_roll():
		return host.state_machine.stomp_state
	timer += delta
	jump_state += delta * 2
	if up_direction: return self
	if host.is_on_loop: return host.state_machine.loop_state
	if host.is_grounded and timer > .01 : return state_machine.grounded_state
	return self

func _control_jump(inp : PlayerInputManager, host : Player, delta : float):
	var joy = inp.get_analog_joystick_value()
	if joy.length() > .1:
		pressing_timer += delta
	else:
		pressing_timer = .0
	var movement = joy \
			.rotated(-Instances.camera.rotation.y) * _calc_air_control(host)
	var new_x = host.velocity.x + movement.x
	var new_z = host.velocity.z + movement.y
	var length = sqrt(pow(new_x, 2) + pow(new_z, 2))
	if length < host.physics.AIR_MAX_SPEED:
		host.velocity.x = new_x
		host.velocity.z = new_z

func _calc_air_control(host : Player):
	var r = host.physics.DOUBLE_JUMP_AIR_CONTROL
	var factor = 3.0 - 4.0 * pressing_timer
	if pressing_timer < .25:
		factor *= pressing_timer * 4.0
	if factor > 1.0:
		r *= factor
	return r

func _process_jump(inp : PlayerInputManager, host : Player, delta : float):
	if up_direction:
		var jump_force = host.physics.DOUBLE_JUMP
		var current_jump_force = jump_force if long_jump or !up_direction else jump_force * .6
		current_jump_force /= jump_state
		if timer > .2: long_jump = false
		if long_jump:
			# Convendria que no fuese tan brusco
			# El valor maximo de salto se llega demasiado pronto
			if inp.is_just_released_jump():
				long_jump = false
		else:
			jump_state += delta
		
		var incr = current_jump_force / (jump_state * 30)
		if incr > .048:
			host.velocity += floor_normal * current_jump_force
		else:
			up_direction = false
			floor_normal = floor_normal.bounce(Vector3.UP)
	else:
		host.is_jumping = false
		host.velocity.y -= host.physics.GRAVITY

func step_move(host : Player, delta : float):
	var fall_vel = host.velocity.y
	host.velocity = host.move_and_slide(host.velocity, host.floor_normal, false, 4, PI/4, false)
	host.end_fall = fall_vel < -.9 and fall_vel - host.velocity.y < 0
