extends PlayerState

signal end

onready var state_machine = get_parent()

var floor_normal : Vector3
var timer : float

func is_attack() -> bool: return true

func enter(host : Player):
	timer = .0
	floor_normal = host.floor_normal.normalized()
	host.default_collider.disabled = true
	host.rolling_collider.disabled = false
	if host.is_shielded and host.shield_type == 1:
		host.homing_aim.start()

func exit(host : Player):
	host.is_jumping = false
	host.floor_manager.get_floor()
	host.homing_aim.stop()
	host.current_speed = host.velocity.length()
	emit_signal("end")

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	host.animator.do_loop("Jump", 1.2, true)

func step(host : Player, delta : float):
	var inp : PlayerInputManager = host.input_manager
	_process_jump(inp, host, delta)
	var next_state = _control_jump(inp, host, delta)

	timer += delta
	if host.is_on_loop: return host.state_machine.loop_state
	if host.is_grounded and timer > .1: return state_machine.rolling_state
	return next_state

func _control_jump(inp : PlayerInputManager, host : Player, delta : float):
	if inp.is_just_pressed_jump():
		if host.is_shielded:
#			if host.shield_type == 0:
#				return XXXX
			if host.shield_type == 1:
				var nearest = host.homing_aim.target
				if nearest == null:
					print("Play SFX de que no hay enemigo cerca")
					return self
				host.state_machine.homing_attack_state.target = nearest
				return host.state_machine.homing_attack_state
#			if host.shield_type == 2:
#				return host.state_machine.double_jump_state
	return self

func _process_jump(inp : PlayerInputManager, host : Player, delta : float):
#	host.is_jumping = false
	if not host.is_on_fan:
		host.velocity.y -= host.physics.GRAVITY

func step_move(host : Player, delta : float):
	var fall_vel = host.velocity.y
	host.velocity = host.move_and_slide(host.velocity, host.floor_normal, false, 4, PI/4, false)
	host.end_fall = fall_vel < -.9 and fall_vel - host.velocity.y < 0
