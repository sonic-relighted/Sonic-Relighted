extends PlayerState

onready var state_machine = get_parent()
onready var inp = $"../../InputManager"

var entered_jumping : bool
var low_speed       : bool
var is_walking      : bool

func is_attack() -> bool: return false

func enter(host : Player):
	low_speed = false
	# esto es porque grounded es el estado por defecto
	# y aun no se ha cargado el player cuando se ejecuta esto
	# pero nada mas empezar entra en juego el estado fall
	# asi que nos da igual...
	if host.default_collider != null:
		host.default_collider.disabled = false
		host.rolling_collider.disabled = true

	host.slide_velocity = host.velocity
	host.is_bouncing = false
	if host.input_manager.is_pressed_jump():
		var diff = OS.get_ticks_msec() - host.jump_button_msec_pressed
		entered_jumping = diff < 75
		host.animator.do_loop("Walk", 1.0, false)
	else:
		entered_jumping = false
	
func exit(host : Player):
	host.is_bouncing = false

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	var speed = 1 + (host.current_speed * .9) / host.physics.MAX_SPEED
	if speed > 1:
		if low_speed or speed < 1.5:
			host.animator.do("Slow", speed * 1.4, true)
		elif low_speed or speed < 2.0:
			host.animator.do("Walk", speed, true)
		else:
			host.animator.do("Run", speed, true)
	else:
		host.animator.do("Idle", speed, true)

func step(host : Player, delta : float):
	var max_speed = host.physics.MAX_SPEED
	if host.is_hi_speed: max_speed *= host.physics.HI_SPEED_VALUE
	
	var state = _control_walk(host, delta)
	_angle(host, delta)
	_update_speed(host, delta)

	var movement = AngleConstants.convert_angles(
			-host.current_speed,
			host.calculated_direction,
			host.current_updown)
	host.velocity = lerp(host.velocity, movement, .2)
	_snap_to_floor(host)

	var abs_rot_x = abs(host.rotation.x)
	var abs_rot_z = abs(host.rotation.z)
	if host.current_updown <= 2.0:
		host.deceleracion_subida = 1.0
	else:
		host.deceleracion_subida += delta * 10
	if (abs_rot_x < 1 and abs_rot_z < 1) or host.current_speed > 2.0:
		host.time_slowly_walking_on_wall = .0
	elif host.time_slowly_walking_on_wall < host.max_time_slowly_walking_on_wall:
		var time_factor = 3 - host.current_speed
		host.time_slowly_walking_on_wall += delta * time_factor
	else:
		host.velocity += Vector3.UP \
				.rotated(Vector3.RIGHT, host.rotation.x) \
				.rotated(Vector3.BACK, host.rotation.z)
		return host.state_machine.fall_state

	if host.is_on_loop: return host.state_machine.loop_state
	if host.accelerator != null: return host.state_machine.onaccelerator_state
	if !host.is_grounded: return host.state_machine.fall_state
	if host.is_pushing: return host.state_machine.pushing_state

	host.rotation_sync_manager.align_to_floor(delta)

	return state

func _snap_to_floor(host : Player):
	if not host.is_on_fan and not host.is_bouncing:
		host.velocity -= host.global_transform.basis.y

func _angle(host : Player, delta : float):
	if is_walking:
		host.model.rotation.y = lerp_angle(
				host.model.rotation.y,
				Instances.camera.rotation.y - host.current_angle + PI,
				delta * 10.0)

func _control_walk(host : Player, delta : float):
	is_walking = false
	low_speed = false
	var previous_angle = host.current_angle
	_control_walk_direction(host)
	if  host.velocity.length() > 1:
		if inp.is_pressing_walk():
			low_speed = true
		elif inp.is_just_pressed_roll():
			return state_machine.rolling_state
		if host.current_speed > 3.0 and abs(host.current_angle - previous_angle) == PI:
			host.current_angle = previous_angle
	else:
		if inp.is_just_pressed_roll():
			return state_machine.spindash_state
	
	if entered_jumping or inp.is_just_pressed_jump():
		return state_machine.jump_state
	return self

func _control_walk_direction(host : Player):
	var analog = host.input_manager.get_analog_joystick_value()
	var angle = analog.angle() + AngleConstants.SEMI_PI
	var length = analog.length()
#	is_walking = false
	if length > .1:
		host.current_angle = angle
		is_walking = true
		low_speed = length < .9

func _update_speed(host : Player, delta : float):
	if is_walking:
		var max_speed = host.physics.MAX_SPEED
		if host.is_hi_speed:
			max_speed *= host.physics.HI_SPEED_VALUE
		if low_speed: max_speed /= 3
		if host.current_speed < max_speed:
			var esfuerzo_subida = 1 + cos(host.current_updown) / 10
			host.current_speed += delta * ((20 - host.current_speed) / host.deceleracion_subida)
			host.current_speed *= esfuerzo_subida
		elif low_speed:
			host.current_speed *= .3
	else:
		if host.current_speed > 1.0:
#			host.current_speed = .7
			host.current_speed = lerp(host.current_speed, .0, delta * host.physics.BRAKE)
		else:
			host.current_speed = .0
	
func step_move(host : Player, delta : float):
	var fall_vel = host.velocity.y
	var slide_impact = host.default_physics.SLIDE_IMPACT if host.is_shielded and host.shield_type == 3 else host.physics.SLIDE_IMPACT
	host.slide_velocity = lerp(host.velocity, host.slide_velocity, slide_impact * delta)
	host.velocity = host.move_and_slide(#_with_snap(
			(host.velocity + host.slide_velocity) * .5,
#			Vector3.DOWN,
			host.floor_normal,
			false, 4, PI/4, false)
	host.end_fall = fall_vel < -.9 and fall_vel - host.velocity.y < 0
