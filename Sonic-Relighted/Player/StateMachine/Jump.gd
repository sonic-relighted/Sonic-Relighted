extends PlayerState

onready var state_machine = get_parent()

var timer : float
var pressing_timer : float # Tiempo presionando direccion
var jump_state : float
var long_jump : bool
var up_direction : bool
var floor_normal : Vector3
var can_shield_attack : bool
var is_post_bubble_bounce : bool

func is_attack() -> bool: return true

func enter(host : Player):
	host.is_grounded = false
	host.is_jumping = true
	long_jump = true
	timer = .0
	pressing_timer = .0
	jump_state = 1.0
	floor_normal = host.floor_normal.normalized()
	up_direction = true
	is_post_bubble_bounce = host.state_machine.previous_state == host.state_machine.bubble_stomp_state
	if is_post_bubble_bounce:
		host.sfx.play_bubble_stomp()
	else:
		host.sfx.play_jump()
	host.default_collider.disabled = true
	host.rolling_collider.disabled = false
	can_shield_attack = false
	if host.is_shielded and host.shield_type == 1:
		host.homing_aim.start()

func exit(host : Player):
	host.is_jumping = false
	host.floor_manager.get_floor()
	host.homing_aim.stop()

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	host.animator.do_loop("Jump", 1.2, true)

func step(host : Player, delta : float):
	can_shield_attack = timer > host.physics.HOMING_LOCKTIME
	var inp : PlayerInputManager = host.input_manager
	_process_jump(inp, host, delta)
	var next_state = _control_jump(inp, host, delta)

	var speed = delta * 2
	host.rotation.x = lerp_angle(host.rotation.x, .0, speed)
	host.rotation.y = lerp_angle(host.rotation.y, .0, speed)
	host.rotation.z = lerp_angle(host.rotation.z, .0, speed)
	
	timer += delta
	jump_state += speed
	if up_direction: return self
	if host.is_on_loop: return host.state_machine.loop_state
	if host.is_grounded and timer > .01: return state_machine.grounded_state
	return next_state

func _control_jump(inp : PlayerInputManager, host : Player, delta : float):
	if inp.is_just_pressed_jump(): host.jump_button_msec_pressed = OS.get_ticks_msec()
	var joy = inp.get_analog_joystick_value()
	if joy.length() > .1:
		pressing_timer += delta
	else:
		pressing_timer = .0
	var movement = joy \
			.rotated(-Instances.camera.rotation.y) * _calc_air_control(host)
	var new_x = host.velocity.x + movement.x
	var new_z = host.velocity.z + movement.y
	var length = sqrt(pow(new_x, 2) + pow(new_z, 2))
	if length < host.physics.AIR_MAX_SPEED:
		host.velocity.x = new_x
		host.velocity.z = new_z
	if inp.is_just_pressed_roll():
		return host.state_machine.stomp_state
	if inp.is_just_pressed_jump() and can_shield_attack and host.is_shielded:
		match host.shield_type:
			1: return _fireshield_attack(host)
			2: return _electricshield_attack(host)
			3: #Hielo
				pass
			4: return _bubbleshield_attack(host)
	return self

func _calc_air_control(host : Player):
	var r = host.physics.AIR_CONTROL
	var factor = 3.0 - 4.0 * pressing_timer
	if pressing_timer < .25:
		factor *= pressing_timer * 4.0
	if factor > 1.0:
		r *= factor
	return r

func _fireshield_attack(host : Player):
	var nearest = host.homing_aim.target
	if nearest == null:
		print("Play SFX de que no hay enemigo cerca")
		return self
	host.state_machine.homing_attack_state.target = nearest
	return host.state_machine.homing_attack_state

func _electricshield_attack(host : Player):
	return host.state_machine.double_jump_state

func _bubbleshield_attack(host : Player):
	return host.state_machine.bubble_stomp_state

func _process_jump(inp : PlayerInputManager, host : Player, delta : float):
	if up_direction:
		var jump_force = host.physics.JUMP
		var current_jump_force = jump_force if long_jump or !up_direction else jump_force * .6
		if is_post_bubble_bounce:
			current_jump_force *= host.physics.BUBBLE_BOUNCE_FACTOR
		current_jump_force /= jump_state
		if timer > .2: long_jump = false
		if long_jump:
			# Convendria que no fuese tan brusco
			# El valor maximo de salto se llega demasiado pronto
			if inp.is_just_released_jump():
				long_jump = false
		else:
			jump_state += delta
		
		var incr = current_jump_force / (jump_state * 30)
		if incr > .048:
			host.velocity += floor_normal * current_jump_force
		else:
			up_direction = false
			floor_normal = floor_normal.bounce(Vector3.UP)
	else:
		host.is_jumping = false
		if not host.is_on_fan:
			host.velocity.y -= host.physics.GRAVITY

func step_move(host : Player, delta : float):
	var fall_vel = host.velocity.y
	host.velocity = host.move_and_slide(host.velocity, host.floor_normal, false, 4, PI/4, false)
	host.end_fall = fall_vel < -.9 and fall_vel - host.velocity.y < 0
