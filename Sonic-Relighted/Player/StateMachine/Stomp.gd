extends PlayerState

onready var state_machine = get_parent()

var fall : bool

func is_attack() -> bool: return true

func enter(host : Player):
	host.velocity = Vector3.ZERO
	host.current_speed = .0
	# Lanzar animacion y esperar
	host.animator.do("Jump", 1.0)
	fall = false
	yield(get_tree().create_timer(.12), "timeout")
	host.animator.do("Fall", 1.0)
	host.velocity = host.physics.STOMP_VEL
	host.current_speed = .0
	host.vfx.start_stomp()
	fall = true

func exit(host : Player):
	host.stomp_collider.disabled = false
	host.vfx.stop_stomp()
	yield(get_tree().create_timer(.12), "timeout")
	host.stomp_collider.disabled = true

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
#	host.animator.do_loop("Stomp", 1.2, true)
	pass

func step(host : Player, delta : float):
	if fall:
		host.velocity.y -= host.physics.GRAVITY
	if host.is_on_loop: return host.state_machine.loop_state
	if host.is_grounded:
		_hitbox(host)
		return state_machine.grounded_state
	if host.velocity.y > .0: return state_machine.fall_state
	return self

func step_move(host : Player, delta : float):
	var fall_vel = host.velocity.y
	host.velocity = host.move_and_slide(host.velocity, host.floor_normal, false, 4, PI/4, false)
	host.end_fall = fall_vel < -.9 and fall_vel - host.velocity.y < 0

func _hitbox(host : Player):
	print("OSTION")
