extends PlayerState

onready var state_machine = get_parent()

var timer : float
var target : Spatial

func is_attack() -> bool: return true

func enter(host : Player):
	timer = 0
	host.sfx.play_fire_jump()
	host.default_collider.disabled = true
	host.rolling_collider.disabled = false
	host.vfx.action_fire()

func exit(host : Player):
	if not target or not is_instance_valid(target) or not target.is_in_group("Spring"):
		host.velocity = Vector3.UP * 10
	host.vfx.stop_fire()

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	host.animator.do_loop("Jump", 1.2, true)

func step(host : Player, delta : float):
	var diff : Vector3 = \
			(target.global_transform.origin
			- host.global_transform.origin
			+ Vector3.UP * .25)
	var direction = diff.normalized()
	
	if not target.is_destroyed and diff.length() > .5:
		host.velocity = direction * 15
		host.current_angle = atan2(direction.z, direction.x)
		host.model.rotation.y = host.current_angle
	else:
		if target.is_in_group("Item"):
			host.collisions_manager.open_item(target)
		elif target.is_in_group("Enemy"):
			host.collisions_manager.kill_enemy(target)
		else:
			host.collisions_manager.destroy_destructible(target)
#		if host.get_slide_count() > 0:
		return state_machine.fall_state
		
	timer += delta
	if timer >= 1.0: return host.state_machine.fall_state
	if host.is_on_loop: return host.state_machine.loop_state
	if host.is_grounded and timer > .01 : return state_machine.grounded_state
	return self

func _control_jump(inp : PlayerInputManager, host : Player, delta : float):
	if inp.is_just_pressed_jump(): host.jump_button_msec_pressed = OS.get_ticks_msec()
	var movement = inp.get_analog_joystick_value() \
			.rotated(-Instances.camera.rotation.y) * host.physics.JUMP
	var new_x = host.velocity.x + movement.x
	var new_z = host.velocity.z + movement.y
	var length = sqrt(pow(new_x, 2) + pow(new_z, 2))
	if length < host.physics.AIR_MAX_SPEED:
		host.velocity.x = new_x
		host.velocity.z = new_z
	if inp.is_just_pressed_jump():
		if host.is_shielded:
			if host.shield_type == 1:
				return 

func step_move(host : Player, delta : float):
	host.move_and_slide(host.velocity, host.floor_normal, false, 4, PI/4, false)
