extends PlayerState

onready var state_machine = get_parent()

func is_attack() -> bool: return false

func enter(host : Player):
	pass

func exit(host : Player):
	pass

func on_animation_finished(anim_name : String):
	pass

func step_animation(host : Player, delta : float):
	pass

func step(host : Player, delta : float):
	return self

func step_move(host : Player, delta : float):
	pass
