extends Area

export(NodePath) var host_nodepath : NodePath
onready var host = get_node(host_nodepath)
onready var particles = $Particles

func _on_RingCollectorArea_area_entered(area):
	if area.is_in_group("Ring"):
		if area.is_destroyed: return
		host.recolect(area)
		area.recolect()
		_particles_on()
	elif area.is_in_group("Star"):
		if area.is_destroyed: return
		host.recolect(area)
		area.recolect()
		_particles_on()
	elif area.is_in_group("AirBubble"):
		if area.is_destroyed: return
		host.recolect(area)
		area.recolect()

func _particles_on():
	particles.emitting = true
	get_tree().create_timer(.5).connect("timeout", self, "_particles_off")
	
func _particles_off():
	particles.emitting = false

func _ready():
	particles.emitting = false
