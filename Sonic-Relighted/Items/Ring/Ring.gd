extends Area

class_name RingItem, "res://RelightedEngine/EditorIcons/Ring.png"

signal destroyed
signal physics_processs_changed(enabled)

onready var model     : Spatial           = $"Scene Root"
onready var collider  : CollisionShape    = $CollisionShape
onready var animator  : AnimationPlayer   = $"Scene Root/AnimationPlayer"
onready var particles : Particles         = $Particles
onready var anillo    : Spatial           = $"Scene Root/Torus"
onready var electric_atraction = $ElectricShieldAtractionArea

onready var is_destroyed          : bool  = false

func set_physics_process(enabled : bool):
	.set_physics_process(enabled)
	emit_signal("physics_processs_changed", enabled)
	_enable_electric(enabled)

func _enable_electric(enabled : bool):
	if enabled:
		electric_atraction.collision_layer = 131072
	else:
		electric_atraction.collision_layer = 0

func configure_shadow(shadow : Spatial):
	shadow.scale *= .5

func _start_animation():
	var anim = animator.get_animation("Idle")
	anim.set_loop(true)
	animator.play("Idle", -1)

func recolect():
	if is_destroyed: return
	model.hide()
	particles.emitting = true
	is_destroyed = true
	get_tree().create_timer(.5).connect("timeout", self, "_on_Timer_timeout")
	electric_atraction.reset()

func reset():
	$Resetable.reset()

func _on_Timer_timeout():
	emit_signal("destroyed")
	model.hide()
	hide()

func _ready():
	_start_animation()
	particles.emitting = false
	anillo.add_to_group("Shadowable")
	var scr = load(GlobalConfig.ITEMS_PATH + "Ring/RingShadowable.gd")
	anillo.set_script(scr)
	var enableable : EnableableArea = load(GlobalConfig.INGAME_ELEMENTS_PATH + "Interactuable/EnableableArea/EnableableArea.res").instance()
	enableable.manage_process = true
	enableable.manage_physics_process = true
	enableable.manage_collision_layer = true
	enableable.manage_collision_mask = true
	anillo.add_child(enableable)
