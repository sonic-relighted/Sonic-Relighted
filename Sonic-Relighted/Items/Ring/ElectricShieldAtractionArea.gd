extends Area

class_name ElectricShieldAtractionAarea

onready var host = get_parent()

onready var speed  : float  = .0
onready var target : Player = null

const height = Vector3.UP * .15

func reset():
	speed = .0
	target = null
	set_physics_process(false)

func enable(body):
	target = body
	set_physics_process(true)

func _ready():
	set_physics_process(false)
	set_process(false)

func _physics_process(delta):
	_update_speed(delta)
	host.global_transform.origin += _get_velocity()

func _update_speed(delta : float):
	if speed > 10.0: speed = 10.0
	else: speed += delta * .5

func _get_velocity() -> Vector3:
	var target_pos = target.global_transform.origin + height
	var diff = target_pos - host.global_transform.origin
	return diff.normalized() * speed

func _on_Ring_hide(enabled : bool):
	print(enabled)
	set_collision_layer_bit(0, enabled)
