extends KinematicBody

class_name BouncingRing, "res://RelightedEngine/EditorIcons/Ring.png"

export(float)    var life_time      : float = 5.5
export(float)    var notice_time    : float = 2.0
export(Material) var blink_material : Material
export(int)      var bounces        : int = 2

onready var ring  : Spatial      = $Ring
onready var torus : MeshInstance = ring.anillo

onready var velocity : Vector3 = Vector3.ZERO

onready var bounce_count : int = 0
onready var frame_count  : int = 0

func recolect():
	ring.recolect()

func apply_central_impulse(impulse : Vector3):
	velocity += impulse

func _on_Ring_destroyed():
	queue_free()

func _physics_process(delta : float):
	frame_count += 1
#	if (frame_count % 2) == 0: return
	velocity.y -= .8
	var col : KinematicCollision = move_and_collide(velocity * delta)
	if col:
		if not col.collider.is_in_group("Item"):
			bounce_count += 1
		if bounce_count >= bounces:
			set_physics_process(false)
		else:
			velocity = velocity.bounce(col.normal) * .8
#		var bouncing = .8

func _timeout_blink():
	torus.set_surface_material(0, blink_material)

func _timeout_destroy():
	_on_Ring_destroyed()

func _timeout_enable():
	ring.collider.disabled = false

func _ready():
	ring.collider.disabled = true
	var enable_time = .25
	get_tree().create_timer(notice_time).connect("timeout", self, "_timeout_blink")
	get_tree().create_timer(life_time).connect("timeout", self, "_timeout_destroy")
	get_tree().create_timer(enable_time).connect("timeout", self, "_timeout_enable")
