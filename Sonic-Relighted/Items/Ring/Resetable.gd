extends Node

onready var host           : Spatial = get_parent()
onready var default_origin : Vector3 = host.global_transform.origin

func reset():
	host.electric_atraction.reset()
	host.is_destroyed = false
	host._start_animation()
	host.particles.emitting = false
	host.global_transform.origin = default_origin
	host.show()
	host.model.show()
