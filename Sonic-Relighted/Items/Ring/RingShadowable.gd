extends Node

signal physics_processs_changed(enabled)

func configure_shadow(shadow : Spatial):
	shadow.scale *= .5

func set_physics_process(enabled : bool):
	.set_physics_process(enabled)
	var ring = get_parent().get_parent()
	ring.set_physics_process(enabled)
	emit_signal("physics_processs_changed", enabled)
