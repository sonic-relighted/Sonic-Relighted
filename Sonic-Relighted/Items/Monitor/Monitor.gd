tool

extends Spatial

class_name MonitorItem, "res://RelightedEngine/EditorIcons/Monitor.png"

export(int, "Shield", "Invencible", "Life", "Rings", "ShieldFire", "ShieldElectric", "Speed", "ShieldIce", "ShieldBubble", "Shield?", "Rings50", "Bonus") var type : int = 0 setget set_type,get_type

onready var audio : AudioStreamPlayer = $Audio
onready var animator_icon = $AnimationPlayer
onready var particles = $Particles
onready var monitor = $Model
onready var box_collider = $BoxCollider
onready var icon = $Sprite3D
onready var is_destroyed : bool = false
onready var destroyed_delay_timer : float = 3.0

func set_type(value):
	type = value
	if Engine.editor_hint:
		$Sprite3D.frame = type

func get_type():
	return type

func _ready():
	if Engine.editor_hint: return
	icon.frame = type
	_start_animation()
	set_process(false)

func _process(delta):
	if Engine.editor_hint: return
	destroyed_delay_timer -= delta
	
	if destroyed_delay_timer <= 0:
		hide()
		set_process(false)

func _start_animation():
	animator_icon.play("Idle")

func recolect():
	if is_destroyed: return
	audio.play()
	animator_icon.play("Action")
	particles.emitting = true
	box_collider.disabled = true
	monitor.hide()
	is_destroyed = true
	set_process(true)

func reset():
	$Resetable.reset()

func _on_AnimationPlayer_animation_finished(anim_name):
	icon.hide()
