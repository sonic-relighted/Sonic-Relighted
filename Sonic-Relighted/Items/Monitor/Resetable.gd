extends Node

onready var host : Spatial = get_parent()

func _ready():
	set_process(false)
	set_physics_process(false)

func reset():
	host._start_animation()
	host.set_process(false)
	host.is_destroyed= false
	host.destroyed_delay_timer = 3.0
	host.particles.emitting = false
	host.box_collider.disabled = false
	host.show()
	host.monitor.show()
	host.icon.show()
