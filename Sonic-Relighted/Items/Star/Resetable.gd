extends Node

onready var host : Spatial = get_parent()

func _ready():
	set_process(false)
	set_physics_process(false)

func reset():
	host.is_destroyed = false
	host._start_animation()
	host.particles.emitting = false
	host.show()
	host.model.show()
