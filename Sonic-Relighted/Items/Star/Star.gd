extends Spatial

class_name StarItem, "res://RelightedEngine/EditorIcons/Ring.png"

signal destroyed

export(int) var star_id : int = 0

onready var model     : Spatial           = $star
onready var animator  : AnimationPlayer   = $star/AnimationPlayer
onready var timer     : Timer             = $Timer
onready var particles : Particles         = $Particles

onready var is_destroyed          : bool  = false

func configure_shadow(shadow : Spatial):
	shadow.scale *= .5

func _start_animation():
	var anim = animator.get_animation("Idle")
	anim.set_loop(true)
	animator.play("Idle", -1)

func recolect():
	if is_destroyed: return
	particles.emitting = true
	is_destroyed = true
	timer.start()

func reset():
	$Resetable.reset()

func _on_Timer_timeout():
	emit_signal("destroyed")
	model.hide()
	hide()

func _ready():
	_start_animation()
	particles.emitting = false
	model.add_to_group("Shadowable")	
	var scr = load(GlobalConfig.ITEMS_PATH + "Ring/RingShadowable.gd")
	model.set_script(scr)
