extends Area

func _on_SurfaceSensor_body_entered(body):
	if body.is_in_group("WaterSurface"):
		get_parent().destroy()
