extends Spatial

class_name AirBubbleEmitter, "res://RelightedEngine/EditorIcons/AirBubbleEmitter.png"

export(float) var time : float = 3.0

onready var timer : Timer = $Timer

func _ready():
	timer.wait_time = time
	_on_Timer_timeout() # Para inicializacion

func _on_Timer_timeout():
	Builder.build_air_bubble(self, Vector3.ZERO)

func set_physics_process(enabled : bool):
	.set_physics_process(enabled)
	if enabled:
		timer.start()
	else:
		timer.stop()
