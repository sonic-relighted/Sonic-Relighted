extends Area

class_name AirBubble

signal destroyed

export(Vector3) var velocity : Vector3 = Vector3.UP

onready var life_tween : Tween = $LifeTween

onready var is_destroyed : bool  = false

func _start_animation():
	pass

func destroy():
	$CollisionShape/Sprite3D/AnimationPlayer.play("Drop")

func recolect():
	if is_destroyed: return
	is_destroyed = true
	destroy()

func _physics_process(delta : float):
	global_transform.origin += velocity * delta * .5

func _ready():
	_start_animation()
	set_process(false)
	var time = .5
	life_tween.interpolate_property(self, "scale", Vector3.ZERO, scale, time, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	life_tween.interpolate_property(self, "translation:y", translation.y, translation.y + .5, time, Tween.TRANS_SINE, Tween.EASE_OUT)
	life_tween.start()

func _on_AnimationPlayer_animation_finished(anim_name):
	emit_signal("destroyed")
	queue_free()
