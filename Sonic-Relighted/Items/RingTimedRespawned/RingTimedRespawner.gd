extends Spatial

export(Resource) var ring_resource : Resource
export(float)    var time : float = 10.0

onready var timer = $Timer
onready var particles = $Particles

func _create():
	var instance : RingItem = ring_resource.instance()
	instance.connect("destroyed", self, "_on_destroy")
	add_child(instance)

func _on_destroy():
	get_tree().create_timer(time).connect("timeout", self, "_on_timeout")

func _on_timeout():
	particles.restart()
	particles.emitting = true
	_create()

func _ready():
	_create()
